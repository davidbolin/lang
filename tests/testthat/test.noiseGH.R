library(testthat)
context("jonas best test noise")
graphics.off()
library(LANG)
draw_data <- function(n, theta, B, transformed_sigma = 0, transformed_lambda = 0, GAL = 1)
{
  spatialObj<- list(latent=list(n=n))
  spatialObj <- init.noise.gal(spatialObj, B)
  noise <- spatialObj$latent$noise
  
  if(transformed_lambda==0)
    lambda_vec = noise$B.lambda%*%theta$lambda
  if(transformed_lambda==1)
    lambda_vec = exp(noise$B.lambda%*%theta$lambda)
  if(transformed_lambda==2)
    lambda_vec = noise$B.lambda%*%exp(theta$lambda)
  if(GAL ==1)
    V <-  rgamma(n, shape= lambda_vec)
  else
    V <-  rgig_sample(rep(-1/2.,n),
                      rep(2.,n),
                      lambda_vec)
  noise$GAL = GAL
  if(transformed_sigma==0)
    sigma_vec <- (noise$B.sigma%*%theta$sigma)
  if(transformed_sigma==1)
    sigma_vec <- exp(noise$B.sigma%*%theta$sigma)
  
  X <- noise$B.delta%*%theta$delta + (noise$B.mu%*%theta$mu)*V + sigma_vec*sqrt(V)*rnorm(n)
  return(list(noise = noise, X = X, V = V, theta= theta))
}

GIG_moment <- function(p, aGIG, bGIG, pGIG)
{
  sab <- sqrt(aGIG*bGIG)
  b1 <- besselK(sab,pGIG + p)
  b2 <- besselK(sab,pGIG )
  c1 <- (bGIG/aGIG)^(p/2)
  return(c1 * b1/b2)
}

n <- 1000
B <- rep(1,n)
delta_true  <- -2.3
mu_true     <-  2.2
sigma_true  <- 1.5
lambda_true <- 2
theta_true <- c(delta_true,mu_true,sigma_true,lambda_true)

#set.seed(123456)
test_that("Noise, GAL noise delta est", {
#simple gradient test if we can find true parameter

data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(0,data_$theta$mu  , data_$theta$sigma, data_$theta$lambda)
data_$noise$estimate <- c(1,0,0,0)
for(i in 1:20)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
}
expect_equal(theta, theta_true,tolerance=5e-1)
})

test_that("Noise, GAL test a,b,p GIG parameters",{
  
  n <- 1000
  B <- rep(1,n)
  theta_true <- c(delta_true,mu_true,sigma_true,lambda_true)
  data_<- draw_data(n, list(delta= delta_true, mu=mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta, data_$theta$mu  , data_$theta$sigma, data_$theta$lambda)
  data_$noise$estimate <- c(0,0,0,0)
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  pGIG_ <- data_$noise$B.lambda%*%data_$noise$lambda -1/2.
  aGIG_ <- ((data_$noise$B.mu%*%data_$noise$mu)/ (data_$noise$B.sigma%*%data_$noise$sigma))^2 +2
  bGIG_ <- ((data_$X - data_$noise$B.delta%*%data_$noise$delta)/ (data_$noise$B.sigma%*%data_$noise$sigma))^2
  expect_equal(as.vector(pGIG_),as.vector(data_$noise$pGIG),tolerance=1e-5)
  expect_equal(as.vector(aGIG_),as.vector(data_$noise$aGIG),tolerance=1e-5)
  expect_equal(as.vector(bGIG_),as.vector(data_$noise$bGIG),tolerance=1e-5)
  
})

test_that("Noise, GAL noise mu est",{
  #simple gradient test if we can find true parameter
data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(data_$theta$delta,0  , data_$theta$sigma, data_$theta$lambda)
data_$noise$estimate <- c(0,1,0,0)
for(i in 1:20)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
}
expect_equal(theta, theta_true,tolerance=5e-1)
})


test_that("Noise, GAL noise sigma est", {
data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(data_$theta$delta, data_$theta$mu  , 1, data_$theta$lambda)
data_$noise$estimate <- c(0,0,1,0)
for(i in 1:20)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
   theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
}
expect_equal(theta, theta_true,tolerance=5e-1)
})

test_that("Noise, GAL noise sigma est expontial transform", {
  data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta, data_$theta$mu  , 3, data_$theta$lambda)
  data_$noiseçestimate <- c(0,0,1,0)
  data_$noise$sigma_transformed <- 1
  for(i in 1:20)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
  }
  expect_equal(exp(theta[3]), theta_true[3],tolerance=5e-1)
})
test_that("Noise, GAL noise lambda est", {

data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(data_$theta$delta, data_$theta$mu, data_$theta$sigma,1)
data_$noise$estimate <- c(0,0,0,1)

for(i in 1:100)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
}
expect_equal(theta, theta_true,tolerance=2e-1)
})
test_that("Noise, GAL noise lambda est transformed", {
  theta_true <- c(delta_true,mu_true,sigma_true,lambda_true)
  data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta, data_$theta$mu, data_$theta$sigma, 3)
  estimate <- c(0,0,0,1)
  data_$noise$lambda_transformed <- 1
  for(i in 1:100)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    #print(data_$noise$df)
    theta <- theta - (1/(0.01*i+1)) * (1/n) * estimate * data_$noise$df
    
  }
  expect_equal(exp(theta[4]), theta_true[4],tolerance=2e-1)
})

test_that("Noise, GAL noise lambda est transformed version 2", {
theta_true <- c(delta_true,mu_true,sigma_true,lambda_true)
data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(data_$theta$delta, data_$theta$mu, data_$theta$sigma, 3)
estimate <- c(0,0,0,1)
data_$noise$lambda_transformed <- 2
for(i in 1:100)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  #print(data_$noise$df)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * estimate * data_$noise$df
  
}
expect_equal(exp(theta[4]), theta_true[4],tolerance=5e-1)
})

test_that("Noise, GAL noise all est", {
data_<- draw_data(n, list(delta= -2.3, mu=2.2, sigma = 1.5, lambda = 2), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(1, 1, 1,1)
data_$noise$estimate <- c(1,1,1,1)
for(i in 1:100)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
  
}

expect_equal(theta, theta_true,tolerance=5e-1)
})

n <- 1000
B <- cbind(rep(1,n),rep(1,n))
B[1:(n/2),1] <- 0
B[(n/2 + 2):n,2] <- 0
delta_true  <- c(-2.3,1)
mu_true     <-  c(2.2,1)
sigma_true  <- c(1.5,0.5)
lambda_true <- c(1.5,2)
theta_true <-  c(delta_true ,mu_true  , sigma_true, lambda_true)
test_that("Noise, GAL noise delta est multi param", {
  #simple gradient test if we can find true parameter

  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(0,0,data_$theta$mu  , data_$theta$sigma, data_$theta$lambda)
  estimate <- c(1,1,rep(0,2*3))
  for(i in 1:120)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+0.5)) * (0.1/n) * estimate* data_$noise$df
  }
  expect_equal(theta, theta_true,tolerance=5e-1)
})

test_that("Noise, GAL noise mu est multi", {
  #simple gradient test if we can find true parameter

  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta,0,0  , data_$theta$sigma, data_$theta$lambda)
  estimate <- c(0,0,1,1,rep(0,2*2))
  for(i in 1:20)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (0.1/n) * estimate * data_$noise$df
    
  }
  expect_equal(theta, theta_true,tolerance=5e-1)
})
test_that("Noise, GAL noise sigma est multi", {
  #simple gradient test if we can find true parameter

  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta, data_$theta$mu , 1,1, data_$theta$lambda)
  estimate <- c(0,0,0,0,1,1,rep(0,1*2))
  for(i in 1:50)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (1/n) * estimate * data_$noise$df
  }
  expect_equal(theta, theta_true,tolerance=2e-1)
})
test_that("Noise, GAL noise lambda est multi", {
  #simple gradient test if we can find true parameter

  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
  theta <- c(data_$theta$delta, data_$theta$mu ,data_$theta$sigma, 1,1)
  data_$noise$estimate <- c(0,0,0,0,0,0,1,1)
  for(i in 1:20)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (1/n) * data_$noise$df
  }
  expect_equal(theta, theta_true,tolerance=5e-1)
})
test_that("Noise, GAL noise lambda est multi transformed", {
  #simple gradient test if we can find true parameter
  lambda_true <-c(-1,0.5)
  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B)
                    ,transformed_lambda=1)
  theta <- c(data_$theta$delta, data_$theta$mu ,data_$theta$sigma, 1,1)
  estimate <- c(0,0,0,0,0,0,1,1)
  data_$noise$lambda_transformed <- 1
  for(i in 1:20)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (1/n) * estimate* data_$noise$df
  }
  expect_equal(theta[7:8], lambda_true,tolerance=5e-1)
})

test_that("Noise, GAL noise lambda est multi transformed 2", {
#simple gradient test if we can find true parameter

data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B)
                  ,transformed_lambda=0)
theta <- c(data_$theta$delta, data_$theta$mu ,data_$theta$sigma, 1,1)
estimate <- c(0,0,0,0,0,0,1,1)
data_$noise$lambda_transformed <- 2
for(i in 1:100)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.01*i+1)) * (1/n) * estimate* data_$noise$df
}
expect_equal(exp(theta[7:8]), lambda_true,tolerance=5e-1)
})
test_that("Noise, NIG noise lambda est multi transformed 2", {
data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B)
                  ,transformed_lambda=0,GAL=0)
theta <- c(data_$theta$delta, data_$theta$mu ,data_$theta$sigma, 1,1)
estimate <- c(0,0,0,0,0,0,1,1)
data_$noise$lambda_transformed <- 2
for(i in 1:20)
{
  data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
  theta <- theta - (1/(0.02*i+1)) * (1/n) * estimate* data_$noise$df
}

expect_equal(exp(theta[7:8]), lambda_true,tolerance=2e-1)
})

test_that("Noise, GAL nH", {
n <- 5
B <- cbind(rep(1,n),runif(n))
delta_true  <- c(-2.3,1)
mu_true     <-  c(2.2,1)
sigma_true  <- c(1.5,0.5)
lambda_true <- c(1.5,0.5)
theta_true <-  c(delta_true ,mu_true  , sigma_true, lambda_true)
data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
theta <- c(data_$theta$lambda ,data_$theta$mu, data_$theta$sigma, data_$theta$lambda)
data_$noise$estimate <- c(1,1,1,1,rep(1,2*3))
noise<-noiseGalgrad(theta, data_$noise, data_$X, data_$V)
H <- t(noise$B.delta)%*%diag(as.vector( (noise$B.sigma%*%noise$sigma)^(-2)/noise$V))%*%noise$B.delta
H_delta_mu <-  t(noise$B.delta)%*%diag(as.vector( (noise$B.sigma%*%noise$sigma)^(-2)))%*%noise$B.mu
H_mu <- t(noise$B.mu)%*%diag(as.vector( (noise$B.sigma%*%noise$sigma)^(-2)*noise$V))%*%noise$B.mu
H <- rbind(cbind(H , H_delta_mu),cbind(t(H_delta_mu),H_mu))
resid <- -(data_$X - noise$B.delta%*%noise$delta - data_$V*(noise$B.mu%*%noise$mu))/(data_$V * (noise$B.sigma%*%noise$sigma)^2)
H_delta_sigma <- 2*(t(noise$B.delta)%*%(diag(as.vector(resid / (noise$B.sigma%*%noise$sigma))) %*%noise$B.sigma))
H_mu_sigma <- 2*(t(noise$B.mu)%*%(diag(as.vector(data_$V * resid / ( noise$B.sigma%*%noise$sigma))) %*%noise$B.sigma))
H_sigma <- 3*(t(noise$B.sigma)%*%(diag(as.vector(data_$V * resid^2 )) %*%noise$B.sigma))
H_sigma <- H_sigma - t(noise$B.sigma)%*%diag(as.vector(noise$B.sigma%*%noise$sigma)^(-2))%*%noise$B.sigma
H <- rbind(cbind( H , rbind(H_delta_sigma, H_mu_sigma)), cbind(t(H_delta_sigma), t(H_mu_sigma), H_sigma))
H_lambda <- t(noise$B.lambda)%*%diag(as.vector(trigamma(noise$B.lambda%*%noise$lambda)))%*%noise$B.lambda
H <- rbind(cbind(H,matrix(0,nrow=ncol(H),ncol=nrow(H_lambda))), cbind(matrix(0,ncol=ncol(H),nrow=nrow(H_lambda)),H_lambda))
expect_equal(noise$H,-H,tolerance=1e-7)
})
test_that("Noise, GAL moments GIG", {
n <- 100
B <- rep(1,n)
delta_true  <- -2.3
mu_true     <-  2.2
sigma_true  <- 1.5
lambda_true <- 1.9
theta_true <- c(delta_true,mu_true,sigma_true,lambda_true)
data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B))
data_$noise <- noiseSimV(theta_true, data_$noise, data_$X, 5000)

EG1 <- GIG_moment(1,data_$noise[["aGIG"]],data_$noise[["bGIG"]],data_$noise[["pGIG"]])
EG2 <- GIG_moment(2,data_$noise[["aGIG"]],data_$noise[["bGIG"]],data_$noise[["pGIG"]])
expect_equal(data_$noise[["V"]], EG1,tolerance=1e-0)
expect_equal(data_$noise[["V2"]], EG2,tolerance=1e-0)
})


test_that("Noise, GAL noise sigma est multi transformed sigma", {
  #simple gradient test if we can find true parameter
n <- 1000
B <- cbind(rep(1,n),runif(n))
delta_true  <- c(-2.3,1)
mu_true     <-  c(2.2,1)
sigma_true  <- c(-1,0.4)
lambda_true <- c(1.5,0.5)
theta_true <-  c(delta_true ,mu_true  , sigma_true, lambda_true)
  data_<- draw_data(n, list(delta= delta_true, mu= mu_true, sigma = sigma_true, lambda = lambda_true), list(B.delta=B, B.mu = B, B.sigma = B, B.lambda = B)
                    ,transformed_sigma = 1)
  theta <- c(data_$theta$delta, data_$theta$mu , 1,1, data_$theta$lambda)
  estimate <- c(0,0,0,0,1,1,rep(0,1*2))
  data_$noise$sigma_transformed <- 1
  for(i in 1:200)
  {
    data_$noise <- noiseGalgrad(theta, data_$noise, data_$X, data_$V)
    theta <- theta - (1/(0.01*i+1)) * (1/n) *  (estimate  * data_$noise$df)
    
  }
  expect_equal(theta[5:6], sigma_true,tolerance=5e-1)
})

