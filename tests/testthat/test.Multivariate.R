######
# Testing that multivariate matern produces correct results and works!
# author: JW
######
rm(list=ls())
graphics.off()


#install.packages("~/hg/Rpackages/LANG_0.1-20150522.tgz",repos = NULL, type="source")
library(LANG)
library(testthat)
require(INLA)



context("Multivariate")
###
# Setting mesh
#
###
n.lattice = 2
x=seq(from=0,to=10,length.out=n.lattice)
lattice=inla.mesh.lattice(x=x,y=x)
mesh=inla.mesh.create(lattice=lattice, extend=FALSE, refine=FALSE)
S = matrix(c(1,1,0,1),2,2)
spde <- spde.model(mesh,d=2,temporal.model="Multivariate Matern",
                   mv.structure=S)


test_that("Covariance, General test", {
Cbig <- kronecker(Diagonal(2),spde$C)
Kbig <- spde.operator(spde,list(kappa=S,phi=S))
Qbig <- t(Kbig)%*%Cbig%*%Kbig
in_list = list(QQQ = Qbig, CCC = as(Cbig,"dgCMatrix"), KKK = as(Kbig,"dgCMatrix"), S = S, G = spde$G, C = spde$C, Ci = as(as(spde$Ci,"CsparseMatrix"),"dgCMatrix"), kappa = 1.27, B.kappa= 0.34*spde$B.kappa, B.phi = spde$B.phi*0.123, phi = 0.59, use.chol = 1)
out_list = multivariate_matern_test(in_list)
})

kappa <- 1.26
phi <- 0.57
  in_list = list( S = S, G = spde$G, C = spde$C, Ci = as(as(spde$Ci,"CsparseMatrix"),"dgCMatrix"), kappa = kappa, B.kappa= spde$B.kappa, B.phi = spde$B.phi, phi = phi, use.chol = 1)
  eps <- 10^-6
  param <-list(kappa=S*kappa,phi=S*phi)
  Kbig <- spde.operator(spde,param)
  Ci <- kronecker(Diagonal(2),spde$Ci)
  Q <- t(Kbig)%*%Ci%*%Kbig
count <- 0
param_eps <- param
  for(i in 1:length(S)){
    if(S[i] > 0){
      param_eps <- param
      param_eps$kappa[i] <-param_eps$kappa[i] + eps
      Kbig_eps  <- spde.operator(spde,param_eps)
      Q_eps <- t(Kbig_eps)%*%Ci%*%Kbig_eps
      df_i = multivariate_matern_gradient_test(in_list, count)
      #print(df_i$df)
      #print(( Q_eps-Q)/eps)
      expect_equal(as.matrix(df_i$df) ,as.matrix(( Q_eps-Q)/eps ),tolerance = 1e-3 )
      count <- count +1
    }
    
  }

