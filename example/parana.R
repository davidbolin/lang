library(gridExtra)
library(ggplot2)
library(lattice)
library(INLA)
library(splancs)
library(LANG)

data(PRprec)
data(PRborder)
use_INLA = TRUE
transform.data = FALSE
month = 10
max_ = TRUE
do.crossval = FALSE
save.plots = TRUE

if(month < 10) {
  month_str <- paste("d0",month,sep="")
} else {
  month_str <- paste("d",month,sep="")
}
index = substr(dimnames(PRprec)[[2]],1,3)%in%month_str

if(max_) {
  Y <- sapply(1:dim(PRprec[,index])[1], function(i) max(PRprec[,index][i,]))
} else {
  Y <- rowMeans(PRprec[,index], na.rm=TRUE)
}

Y_ <- PRprec[,index]
Y <- rowMeans(Y_, na.rm=TRUE)
for(i in 1:nrow(Y_))
{
  if(mean(is.na(Y_[i,]))==1)
  {
    Y[i] = NA
  }else{
    if(max_ == 1){
      Y[i] = max(Y_[i,],na.rm=T)
    }else{
      Y[i] = rowMeans(Y_[i,],na.rm=T)
    }
  }
}

ind <- !is.na(Y)
Y <- Y[ind]
if(transform.data){
  Y <- sqrt(Y)
}

c.map = col.regions = rev(topo.colors(100))

n.obs = length(Y)
coords <- as.matrix(PRprec[ind,1:2])

ggplot() + geom_point(aes(x=coords[,1],y=coords[,2],colour=Y), size=2, alpha=1) + scale_colour_gradientn(colours=c.map) + geom_path(aes(x=PRborder[,1],y=PRborder[,2]))

prdomain <- inla.nonconvex.hull(coords, -0.03, -0.05, resolution=c(100,100))
prmesh <- inla.mesh.2d(boundary=prdomain, max.edge=c(.2,1), cutoff=0.05)

plot(prmesh, asp=1, main=""); lines(PRborder, col=3)
points(coords[,1], coords[,2], pch=19, cex=.5, col="red")

A <- inla.spde.make.A(prmesh, loc=coords)
spde <- spde.model(prmesh)

B = matrix(cBind(rep(1,dim(coords)[1]),coords),dim(coords)[1],3)
Bm = c(mean(B[,2]), mean(B[,3]))
Bsd = c(sd(B[,2]), sd(B[,3]))
B[,2] = (B[,2] - Bm[1])/Bsd[1]
B[,3] = (B[,3] - Bm[2])/Bsd[2]

res = spde.est(data = list(Y=Y, B=B, A=A),latent = spde,Optim=list(use.chol=0, trace.iter=100),verbose=1)



if(save.plots){
  nxy <- c(150,100)
  projgrid <- inla.mesh.projector(prmesh, xlim=range(PRborder[,1]),
                                  ylim=range(PRborder[,2]), dims=nxy)

  xy.in <- inout(projgrid$lattice$loc, cbind(PRborder[,1], PRborder[,2]))
  A.prd <- projgrid$proj$A[xy.in, ]
  coord.prd = projgrid$lattice$loc[xy.in,]
  n.prd <- dim(coord.prd)[1]
  B.prd = matrix(cBind(rep(1,n.prd),coord.prd),n.prd,3)
  B.prd[,2] = (B.prd[,2] - Bm[1])/Bsd[1]
  B.prd[,3] = (B.prd[,3] - Bm[2])/Bsd[2]

  prd <- spde.predict(res,coord.prd,B.prd,calc.mean=TRUE,
                    calc.improved.variances=TRUE)

  m.prd <- sd.prd <- matrix(NA, nxy[1], nxy[2])
  m.prd[xy.in] <- as.vector(prd$mean)
  sd.prd[xy.in] <- as.vector(sqrt(prd$vars.improved))

  c.map = tim.colors(100)
  grid.arrange(levelplot(m.prd, col.regions=c.map, xlab="",
                         ylab="", scales=list(draw=FALSE),main="mean"),
               levelplot(sd.prd, col.regions=c.map, xlab="",
                         ylab="", scales=list(draw=FALSE),
                         main="standard deviation"))

  #sample posterior
  s1 = 199
  s2 = 258
  coords.samp = coords[c(s1,s2),]
  B.samp = matrix(cBind(rep(1,2),coords.samp),2,3)
  B.samp[,2] = (B.samp[,2] - Bm[1])/Bsd[1]
  B.samp[,3] = (B.samp[,3] - Bm[2])/Bsd[2]

  samp <- spde.predict(res,coords[c(s1,s2),],B.samp,n.samples=10000)

  y <- samp$Y.samples
  if(transform.data){
    y = y^2
  }

  par(mfrow=c(2,2))
  plot(density(samp$X.samples[1,]),main = "X(s_1)")
  plot(density(samp$X.samples[2,]),main = "X(s_2)")
  plot(density(y[1,]),main = "X(s_1) + e")
  plot(density(y[2,]),main = "X(s_2) + e")
}
if(use_INLA)
{
#INLA
spde2 <- inla.spde2.matern(prmesh)
mesh.index <- inla.spde.make.index(name="field",n.spde=spde2$n.spde)

stk.dat <- inla.stack(data=list(y=Y), A=list(A,1), tag="est",
        effects=list(c(mesh.index,list(Intercept=1)),
                     list(long=B[,2],lat=B[,3])))

f <- y ~ -1 + Intercept + long + lat + f(field, model=spde2)
#f <- y ~ -1 + Intercept +  f(field, model=spde)

cat('Estimate parameters using INLA\n')
r.inla <- inla(f, family="Gaussian", data=inla.stack.data(stk.dat),
          control.predictor=list(A=inla.stack.A(stk.dat),compute=TRUE,link = 1),
          control.fixed=list(prec=1e-10))


res.inla = list(mu = r.inla$summary.fixed[,1],
                sigma2 = 1/r.inla$summary.hyperpar[1,1],
                kappa2 = exp(2*r.inla$summary.hyperpar[3,1]),
                phi2 = exp(2*r.inla$summary.hyperpar[2,1]))

results = data.frame(
      mean = t(cbind(res$likelihood.parameters$beta, as.vector(res.inla$mu))),
      sigma = c(sqrt(res$likelihood.parameters$sigma2),sqrt(res.inla$sigma2)),
      kappa = c(sqrt(res$latent.parameters$kappa2), sqrt(res.inla$kappa2)),
            phi = c(1/res$latent.parameters$phi,1/sqrt(res.inla$phi2)),
            row.names = c("gauss.est","INLA"))
cat("Estiation results for ")
if(max_){
  cat('max data:\n')
}else{
  cat('mean data:\n')
}
print(results)

if(do.crossval){
  s2 = result$likelihood.parameters$sigma2
  beta = result$likelihood.parameters$beta
  Q = spde.precision(spde,par.kappa2 = result$latent.parameters$kappa,
                     par.phi = result$latent.parameters$phi)
  n.groups = 10
  cat('Performing crossvalidation with', n.groups, 'groups.\n')
  sim = 1000
  inds = sample(1:n.groups,n.obs,replace=TRUE)

  for(i in 1:n.groups){
    cat(i, ' ')
    index = inds == i
    nk = sum(index)
    Y.k = Y[index]
    A.k = A[index,]
    B.k = B[index,]

    Y.p = Y[!index]
    A.p = A[!index,]
    B.p = B[!index,]

    Q.post = Q + t(A.p)%*%A.p/s2
    X.post = solve(Q.post,(t(A.p)%*%(Y.p-B.p%*%beta)/s2))

    X.hat = B.k%*%beta + A.k%*%X.post

    Sigma.k = A.k%*%solve(Q.post,t(A.k)) + diag(rep(s2,nk))
    R.k = chol(Sigma.k)

    x1 = inla.qsample(n=sim, Q=Q.post)
    x2 = inla.qsample(n=sim, Q=Q.post)
    y1 = B.k%*%beta + A.k%*%x1+matrix(rnorm(nk*sim)*sqrt(s2),nk,sim)
    y2 = B.k%*%beta + A.k%*%x1+matrix(rnorm(nk*sim)*sqrt(s2),nk,sim)

    if(transform.data){
      XY = Y.k^2 - y1^2
      XX = y1^2 - y2^2
    } else {
      XY = Y.k - y1
      XX = y1 - y2
    }
    ds.k = diag(Sigma.k)
    if(i==1){
      if(transform.data){
        err.k = X.hat^2 + ds.k - Y.k^2
        Krig.err.k = 4*X.hat^2*ds.k + 2*ds.k^2
      } else {
        err.k = X.hat-Y.k
        Krig.err.k = ds.k
      }
      XY.vec =  XY
      XX.vec = XX
    } else {
      if(transform.data){
        err.k = rBind(err.k,X.hat^2 + ds.k - Y.k^2)
        Krig.err.k = rBind(Krig.err.k, 4*X.hat^2*ds.k + 2*ds.k^2)
      } else {
        err.k = rBind(err.k, X.hat-Y.k)
        Krig.err.k = c(Krig.err.k,ds.k)
      }
      XY.vec = rBind(XY.vec, XY)
      XX.vec = rBind(XX.vec, XX)
    }
  }

  ES   = mean(sqrt(colSums(XY.vec^2))) -0.5*mean(sqrt(colSums(XX.vec^2)))
  CRPS = mean(abs(XY.vec)) -0.5*mean(abs(XX.vec))
  cat('\n')
  cat('Crossvalidation results for ')
  if(transform.data)
    cat('transformed Gaussian model:\n')
  else
    cat('Gaussian model:\n')

  cat('E(r) = ', mean(err.k),'\n')
  cat('V(r) = ', var(as.vector(err.k)),'\n')
  cat('V(hr)= ', var(as.vector(err.k/sqrt(Krig.err.k))),'\n')
  cat('ES   = ', ES,'\n')
  cat('CRPS = ', CRPS,'\n')
}
}