require(LANG)
require(INLA)
require(fields)

#First estimate stationary model:
kappa2 = 2
phi2 = 0.5
sigma2.e = 0.01
theta.mu = 1
n.lattice = 30
n.obs=100

x=seq(from=0,to=10,length.out=n.lattice)
lattice=inla.mesh.lattice(x=x,y=x)
mesh=inla.mesh.create(lattice=lattice, extend=FALSE, refine=FALSE)
obs.loc = cbind(runif(n.obs)*diff(range(x))+min(x),
                runif(n.obs)*diff(range(x))+min(x))

spde <- spde.model(mesh,d=10,temporal.model="AR")
x = spde.sample(n.sim=1,spde, par.phi = sqrt(phi2),par.kappa2 = kappa2,par.ar=0.1)

xd = x; dim(xd) <- c(spde$n,spde$d)

proj <- inla.mesh.projector(mesh,dims=c(80,80))

par(mfrow=c(3,3))
for(i in 1:9){
  image.plot(proj$x,proj$y,inla.mesh.project(proj,xd[,i]),xlab="",ylab="")
}

A = kronecker(Diagonal(spde$d,1),inla.spde.make.A(mesh=mesh,loc=obs.loc))
B = rep(1,n.obs*spde$d)
Y = as.vector(B*theta.mu + A %*% x + rnorm(spde$d*n.obs)*sqrt(sigma2.e))

#res = lang.est(data = list(Y=Y, B=B, A=A),latent = spde,Optim=list(max.iter=1,use.chol=0,solver.drop.tol=1e-4),verbose=1)


# simulate non-gaussian AR
B.ar = matrix(rep(0.8),10,1)
B.ar[1] = 0.4
spde <- spde.model(mesh,d=10,temporal.model="AR",noise="GAL",B.ar = B.ar)

#spde$B.lambda = matrix(diag(spde$C),spde$n,1)
#spde$B.delta = matrix(diag(spde$C),spde$n,1)
x = spde.sample(n.sim=1,spde, par.kappa2 = kappa2,par.ar=0.8,
                par.delta=1,par.mu=1,par.sigma=.1,par.lambda=0.2)

xd = x; dim(xd) <- c(spde$n,spde$d)

proj <- inla.mesh.projector(mesh,dims=c(80,80))

par(mfrow=c(3,3))
for(i in 1:9){
  image.plot(proj$x,proj$y,inla.mesh.project(proj,xd[,i]),xlab="",ylab="")
}
