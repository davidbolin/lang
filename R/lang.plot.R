library(ggplot2)

##
#' function for plotting tracjotries of stochastic gradient estiamtion for estimation of GAL or NIG,
#' @param res lang.est result using GAL or NIG
##
plot.traj <- function(res, type="latent")
{
  cat("Warning very prelim version of plot.traj, prob not stable")
  res <- parse.res.theta.vec(res)

  if(type=="latent")
  {
    par(mfrow=c(2,2))
    plot(res$latent$noise$delta_vec,type='l',ylab="delta")
    plot(res$latent$noise$mu_vec,type='l',ylab="mu")
    plot(res$latent$noise$sigma_vec,type='l',ylab="sigma")
    plot( rowMeans(res$latent$noise$B.lambda)%*%res$latent$noise$lambda_vec,type='l',ylab="mean(B.lambda) * lambda")

  }
  if(type=="likelihood")
  {
    par(mfrow=c(2,1))
    Bbeta <- rowMeans(res$latent$noise$B.lambda)%*%
    Beta <- res$likelihood$beta_vec
    plot(1:nrow(Beta),Bbeta,type='l',ylab="beta")#,ylim = c(min(Beta),max(Beta)))
    #for(i in 2:dim(Beta)[2])
    #{
    #  lines(1:nrow(Beta),Beta[,i])
    #}
    plot(res$likelihood$sigma_vec,type='l',ylab="sigma")

  }



}

#TODO: add:
#TODO: make it so the tracjtories are stored in various parts of res
#TODO:

##
# takes the theta_vec from result and puts the parameter in the correct place!
##
parse.res.theta.vec <- function(res)
{
  Theta_vec <- res$Thetas
  n_lik <- length(res$likelihood$fixed)
  n_pars <- dim(res$Thetas)[2]
  #
  #   likelihood parameters
  #
  if(length(res$likelihood$beta) > 0)
  {
    n_beta <- length(res$likelihood$beta)
    res$likelihood$beta_vec <- Theta_vec[,1:n_beta]
    Theta_vec <- Theta_vec[,(1+n_beta):dim(Theta_vec)[2]]
  }
  n_sigma <- length(res$likelihood$sigma)
  res$likelihood$sigma_vec <- exp(Theta_vec[, 1:n_sigma])
  
  Theta_vec <- Theta_vec[,(1+n_sigma):dim(Theta_vec)[2]]
  #####
  # WARNING HACK HACK
  #######
  # Require operator n_params!
  # Require operator fixed!
  #############
  res$operator$kappa_vec <- exp(Theta_vec[,1])
  Theta_vec <- Theta_vec[,2:dim(Theta_vec)[2]]
  #####
  # WARNING HACK HACK
  ####### 
  cat("HACK solution for operator parameter at the moment")
  n_delta <- length(res$latent$noise$delta)
  if(length(res$latent$noise$delta) > 0)
  {
    res$latent$noise$delta_vec <- Theta_vec[,1:n_delta]
    Theta_vec <- Theta_vec[,(1+n_delta):dim(Theta_vec)[2]]
  }
  n_mu <- length(res$latent$noise$mu)
  if(length(res$latent$noise$mu) > 0)
  {
    res$latent$noise$mu_vec <- Theta_vec[,1:n_mu]
    Theta_vec <- Theta_vec[,(1+n_mu):dim(Theta_vec)[2]]
  }
  
  n_sigma <- length(res$latent$noise$sigma)
  if(length(res$latent$noise$sigma) > 0)
  {
    res$latent$noise$sigma_vec <- exp(Theta_vec[,1:n_sigma])
    Theta_vec <- as.matrix(Theta_vec[,(1+n_sigma):dim(Theta_vec)[2]])
  }
  n_lambda <- length(res$latent$noise$lambda)
  if(length(res$latent$noise$sigma) > 0)
    res$latent$noise$lambda_vec <- exp(Theta_vec[,1:n_lambda])
  
  return(res)
}