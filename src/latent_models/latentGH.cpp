#include "latentGH.h"
#include "error_check.h"
#include "eigen_add_on.h"

void stationarymatern::initFromList(Rcpp::List const &init_list)
{
   std::vector<std::string> check_names =  {"C", "G", "kappa","B.kappa"};
  check_Rcpplist(init_list, check_names, "stationarymatern::initFromList");

 
  
  C = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["C"]);
  G = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["G"]); 


  n = C.rows();
  

   eigen_vector_from_list(kappa, init_list, "kappa");
   eigen_matrix_from_list(Bkappa, init_list, "B.kappa");


  if(init_list.containsElementNamed("kappa_transformed"))
    kappa_transformed = Rcpp::as<int>(init_list["kappa_transformed"]);
  else
    kappa_transformed = 0;
    
  npars += kappa.size();
  initKappa();
  fixed.setZero(kappa.size());

  dkappa0.resize(kappa.size());
  ddkappa0.resize(kappa.size(), kappa.size());
  
   if(init_list.containsElementNamed("calc_ddk"))
    calc_ddk = Rcpp::as<int>(init_list["calc_ddk"]);
  else
    calc_ddk = 1; 

}

Eigen::VectorXd stationarymatern::exp_param()
{
  
  Eigen::VectorXd param_exp;
  param_exp.setZero(npars);
  Eigen::VectorXd param_exp_noise = noiseClass->estimate_exp();
   if(kappa_transformed == 1){
    for(int k=0; k< kappa.size(); k++)
      param_exp(k) = 1;
   }
  for(int k= 0;k<param_exp_noise.size();k++)
    param_exp(k + kappa.size()) = param_exp_noise(k);
  
  return(param_exp);
}
void stationarymatern::initKappa()
{
  if(kappa_transformed > 1)
  {
    Rcpp::Rcout << "stationarymatern::initKappa() , kappa_transformed should be either 0,1 \n";
    throw "stationarymatern::initKappa() , kappa_transformed should be either 0,1 ";
  }
  if(kappa_transformed == 1){
    kappa_vec  = &stationarymatern::kappa_exp_transform;
    dkappa_mat = &stationarymatern::dkappa_exp_transform;
  }
  else{
    kappa_vec  = &stationarymatern::kappa_no_transform;
    dkappa_mat = &stationarymatern::dkappa_no_transform;
  }
}
Eigen::MatrixXd stationarymatern::dkappa_exp_transform() 
{
  
  Eigen::VectorXd Bkappa_kappa = (this->*kappa_vec)(); 
  Eigen::MatrixXd temp = Bkappa_kappa.asDiagonal() * Bkappa ;
  return(temp);
}

void stationarymatern::initSolver(Rcpp::List const &init_list)
{
  use_chol = Rcpp::as<int>(init_list["use.chol"]);
  if(use_chol==1){
    Ksolver = new cholesky_solver;
  } else {
    Ksolver = new iterative_solver;
  }
  int iN =  Rcpp::as<int>(init_list["trace.iter"]);
  int imiter = Rcpp::as<int>(init_list["trace.solver.max.iter"]);
  double itol = Rcpp::as<double>(init_list["trace.solver.tol"]);
  (*Ksolver).init(n,iN,imiter,itol);
   K = G;
   Eigen::VectorXd Bkappa_kappa = (this->*kappa_vec)(); 
   K += Bkappa_kappa.asDiagonal() *C;
  (*Ksolver).analyze(K);
}


Rcpp::List stationarymatern::output_list()
{
  Rcpp::List List;
  List["C"]          = C;
  List["G"]          = G;
  List["kappa"]      = kappa;
  List["B.kappa"]     = Bkappa;
  //List["X"]          = (*X);
  List["n"]          = C.rows();
  List["noise"]      = noiseClass->toRcppList();
  List["kappa_transformed"]  = kappa_transformed;
  List["calc_ddk"]           = calc_ddk;
  return(List);
}
void stationarymatern::vec_to_theta(const Eigen::VectorXd & thetaVec) {
  kappa = thetaVec.head(Bkappa.cols());
  noiseClass->vec_to_theta(thetaVec.tail(thetaVec.size() - Bkappa.cols()));
}
Eigen::VectorXd stationarymatern::get_theta()
{
  Eigen::VectorXd thetaVec;
  thetaVec.resize(npars);
  for(int k=0; k< Bkappa.cols(); k++)
    thetaVec(k) = kappa(k);

   Eigen::VectorXd thetaNoise = noiseClass->get_theta();


  for(int k=0; k< thetaNoise.size(); k++)
    thetaVec(k + Bkappa.cols()) = thetaNoise(k);


  return(thetaVec);
}
void stationarymatern::dK_kappa_num_eps()
{
  if(kappa_transformed == 0){
    // might become depricatied warning
    SparseMatrix<double,0,int> Phik;
    Phik.resize(n,n);
    Eigen::VectorXd Bkappa_kappa = (Bkappa*kappa);  //Bkappa should not be changed
    double eps = 1e-4;
   for(int k= 0;k<kappa.size();k++)
    {
      if(fixed(k) == 0){
        
        K = G;
        Bkappa_kappa.array() += eps * Bkappa.col(k).array(); 
        K += Bkappa_kappa.asDiagonal() *C;
        (*Ksolver).compute(K);
        
        Bkappa_kappa.array() -= eps * Bkappa.col(k).array();
        for(int kk= k;kk<kappa.size();kk++)
        {
            if(fixed(kk) == 0){
              Phik.setZero();
              for(int i=0;i<n;i++)
                Phik.insert(i,i) = Bkappa(i,kk);
      
              SparseMatrix<double,0,int> tmp1 = Phik*C;
              double KiC  = (*Ksolver).trace(tmp1);
              if(k == kk)
                ddkappa0(k,kk) = ( KiC - dkappa0(k))/(eps);
              else{
                ddkappa0(k,kk)  = ( KiC - dkappa0(kk))/eps;
                ddkappa0(kk,k)  = ddkappa0(k,kk);
              }
            }
        }
      
      }
    }
    K = G; 
    K += Bkappa_kappa.asDiagonal() *C;
    (*Ksolver).compute(K);
  }
}
void stationarymatern::dK_kappa()
{
  K = G;
  Eigen::VectorXd Bkappa_kappa = (this->*kappa_vec)(); 
  K += Bkappa_kappa.asDiagonal() *C;
  (*Ksolver).compute(K);
    dkappa0.setZero();
    ddkappa0.setZero();
  if(fixed.head(kappa.size()).sum() < kappa.size())
  {
    SparseMatrix<double,0,int> Phik, Phil;
    Eigen::MatrixXd df_Bkappa = (this->*dkappa_mat)();
    Phil.resize(n,n);
    Phik.resize(n,n);
    for(int k= 0;k<kappa.size();k++)
    {
      if(fixed(k) == 0){
        Phik.setZero();
        for(int i=0;i<n;i++)
          Phik.insert(i,i) = df_Bkappa(i,k);

        SparseMatrix<double,0,int> tmp1 = Phik*C;
        double KiC  = (*Ksolver).trace(tmp1);
        dkappa0(k) = KiC;
        
        if(use_chol == 0 && kappa_transformed == 0){
          ddkappa0(k,k) = -(*Ksolver).trace2(tmp1,tmp1);
          //calculate d2kappa
          for(int l=(k+1);l<kappa.size();l++)
          {
            if(fixed(l) == 0){
              Phil.setZero();
              for(int i=0;i<n;i++)
                Phil.insert(i,i) = df_Bkappa(i,l); // df Bkappa
  
              SparseMatrix<double,0,int> tmp2 = Phil*C;
              ddkappa0(k,l) = -(*Ksolver).trace2(tmp1,tmp2);
              ddkappa0(l,k) = ddkappa0(k,l);
            }
          }
        }
      }
  }
  if(use_chol == 1){
    dK_kappa_num_eps();
  }
  }
}
void stationarymatern::show_results()
{
  Rcpp::Rcout << "\n*********************\n";
  Rcpp::Rcout << "latent: matern ::\n";
  if(kappa_transformed == 0)
    Rcpp::Rcout << "kappa = " << kappa << "\n";
  else
    Rcpp::Rcout << "exp(kappa) = " << kappa.exp() << "\n";
  Rcpp::Rcout << "*********************\n";
  noiseClass->show_results();
  
}
void stationarymatern::update_param()
{
  dK_kappa();
  noiseClass->update_param();
}


double stationarymatern::f(const Eigen::VectorXd& theta) {


  if( kappa_transformed == 0){
  for(int k= 0;k<kappa.size();k++){
    if(theta(k) < 0)
      return(NAN);
  }
  }
   return(noiseClass->f(theta.tail(theta.size() - Bkappa.cols())));
}

Eigen::VectorXd stationarymatern::df()
{

  Eigen::VectorXd df_vec;
  df_vec.resize(npars);
  for(int k= 0;k<kappa.size();k++)
    df_vec(k)  =  - dkappa0(k);

  Eigen::VectorXd df_noise = noiseClass->df();

  for(int k= 0;k<df_noise.size();k++)
    df_vec(k + kappa.size()) = df_noise(k);

    Eigen::VectorXd CX = C * (*X);
    Eigen::MatrixXd df_Bkappa = (this->*dkappa_mat)();
  for(int k=0; k< df_Bkappa.cols(); k++)
  {
    Eigen::VectorXd BCX = df_Bkappa.col(k).array() * CX.array(); // df Bkappa
    if(fixed(k) == 0)
      df_vec(k) += BCX.transpose() * noiseClass->resid;
  }
  return(df_vec);
}

void stationarymatern::updateNoise()
{
  Eigen::VectorXd E = K * (*X);
  noiseClass->setE(&E);
  noiseClass->update();
}
void stationarymatern::update()
{
  noiseClass->updateV();
  Q_post = K.transpose();
  if(mu_post.size() == 0)
     mu_post.resize(noiseClass->n);
  
  mu_post  =  Q_post * noiseClass->muE;
  Q_post  = Q_post  * (noiseClass->iVsigma).asDiagonal() ;
  Q_post  = Q_post  * K;
}
Eigen::MatrixXd stationarymatern::calcH()
{
  //MIGHT BE DEPRICATED
  Eigen::MatrixXd H;
  H.setZero(npars,npars);
  H.block(0, 0, Bkappa.cols(), Bkappa.cols())  = ddkappa0;
  H.bottomRightCorner(noiseClass->npars, noiseClass->npars) += noiseClass->calcH();
  Eigen::VectorXd CX = C * (*X);
  Eigen::MatrixXd df_Bkappa = (this->*dkappa_mat)();
  for(int k=0; k< Bkappa.cols(); k++)
  {

    if(fixed(k) == 0){
      Eigen::VectorXd BCX = df_Bkappa.col(k).array() * CX.array();
      Eigen::VectorXd BCX_noise = (noiseClass->iVsigma.asDiagonal() *  BCX);
      H(k,k) -= BCX.transpose() * BCX_noise;
      for(int l=(k+1);l<kappa.size();l++)
      {
        Eigen::VectorXd BCX2 = df_Bkappa.col(l).array() * CX.array();
          if(fixed(l) == 0){
            double c = BCX2.transpose() * BCX_noise;
            H(k,l) -= c;
            H(l,k) -= c;
          }
      }

    }else
    {
      H(k,k) = -1;
    }

  }
  return(H);
}
