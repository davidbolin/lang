#include "Qmatrix.h"
#include "error_check.h"
#include "eigen_add_on.h"
using namespace Rcpp;
using namespace std;
MultivariateMaternMatrixOperator::~MultivariateMaternMatrixOperator()
{
  
  delete kpv;
  delete phiv;
  delete d2kappa;
  delete d2phi;
  delete dkappadphi;
  delete Bkp;
  delete Bphi;
  delete Kappa;
  delete Phi;
  delete Klist;
  delete Clist;
  delete Cilist;
  delete Glist;
  
}
void MultivariateMaternMatrixOperator::initFromList(Rcpp::List const & init_list)
{
   std::vector<std::string> check_names =  {"S","C", "G", "Ci","B.kappa","B.phi","kappa","phi"};
  check_Rcpplist(init_list, check_names, "MaternMatrixOperator::initFromList");

  S = Rcpp::as<Eigen::MatrixXd>(init_list["S"]);
  dim = S.rows();
  nop = S.sum();
  nop_ij.resize(nop,2);
  int k = 0;
  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(S(i,j) == 1){
        nop_ij(k,0) = i;
        nop_ij(k,1) = j;
        k++;
      }
    }
  }
  nkp.setOnes(nop);
  nphi.setOnes(nop);

  kpv =        new VectorXd[nop];
  phiv =       new VectorXd[nop];
  d2kappa =    new MatrixXd[nop];
  d2phi =      new MatrixXd[nop];
  dkappadphi = new MatrixXd[nop];
  Bkp =        new MatrixXd[nop];
  Bphi =       new MatrixXd[nop];
  Kappa =      new DiagonalMatrix<double,Dynamic>[nop];
  Phi =        new DiagonalMatrix<double,Dynamic>[nop];
  Klist =      new SparseMatrix<double,0,int>[nop];
  Clist =      new SparseMatrix<double,0,int>[nop];
  Cilist =     new SparseMatrix<double,0,int>[nop];
  Glist =      new SparseMatrix<double,0,int>[nop];
  npars = 0;
  kappa_vec  = &g_exp_transform;
  dkappa_mat = &dg_exp_transform;
  for(int i=0;i<nop;i++){
    kpv[i]  =   Rcpp::as<Eigen::VectorXd>(init_list["kappa"]);
    phiv[i] =   Rcpp::as<Eigen::VectorXd>(init_list["phi"]);
    Bkp[i]  =   Rcpp::as<Eigen::MatrixXd>(init_list["B.kappa"]);
    Bphi[i] =   Rcpp::as<Eigen::MatrixXd>(init_list["B.phi"]);
    Clist[i] =  Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["C"]);
    Cilist[i] = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Ci"]);
    Glist[i] =  Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["G"]);
    nkp(i)  = kpv[i].size();
    nphi(i) = phiv[i].size();

    kappa   = kappa_vec(Bkp[i], kpv[i]);
    phi  = kappa_vec(Bphi[i],   phiv[i]);
    n = Glist[i].rows();
    Phi[i].resize(n);
    Phi[i].diagonal() = phi;
    Klist[i] = Glist[i] + kappa.asDiagonal()*Clist[i];
    npars   += nkp(i) + nphi(i);
  }
  

  d = n*dim;
  Ci.resize(d,d);
  G.resize(d,d);
  C.resize(d,d);
  Q.resize(d,d);
  int kk = 0;
  Eigen::SparseMatrix<double,0,int> KP;
  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(i==j)
        setSparseBlock(&Ci,i*n, j*n, Cilist[kk]);

      if(S(i,j) == 1){
        setSparseBlock(&C,i*n, j*n, Clist[kk]);
        setSparseBlock(&G,i*n, j*n, Glist[kk]);
        KP = Eigen::SparseMatrix<double,0,int>(Phi[kk]*Klist[kk]);
        KP.makeCompressed();
        setSparseBlock(&Q,i*n, j*n, KP);
        kk++;
      }
    }
  }

  theta.setZero(npars);
  kk=0;
  int ii=0;
  Phik = new SparseMatrix<double,0,int>[npars];
  for(int iop=0;iop<nop;iop++){
    theta.segment(kk,nkp(iop)) = kpv[iop];
    dkappa = dkappa_mat(Bkp[iop],kpv[iop]);
    kk +=nkp(iop);
    for(int i = 0;i<nkp(iop);i++){
      Phik[ii].resize(n,n);
      for(int j=0;j<n;j++){
        Phik[ii].insert(j,j) = dkappa(j,i);
      }
      Phik[ii].makeCompressed();
      ii++;
    }
  }

  for(int iop=0;iop<nop;iop++){
    theta.segment(kk,nphi(iop)) = phiv[iop];
    dphi = dkappa_mat(Bphi[iop],phiv[iop]);
    kk +=nphi(iop);
    for(int i = 0;i<nphi(iop);i++){
      Phik[ii].resize(n,n);
      for(int j=0;j<n;j++){
        Phik[ii].insert(j,j) = dphi(j,i);
      }
      Phik[ii].makeCompressed();
      ii++;
    }
  }

  //use_chol = Rcpp::as<int>(init_list["use.chol"]);
  //if(use_chol==1){
  Qsolver = new lu_sparse_solver;
  //} else {
  //  Qsolver = new iterative_solver;
  //}

  (*Qsolver).initFromList(d,init_list);
  (*Qsolver).analyze(Q);
  (*Qsolver).compute(Q);
  Mk = new SparseMatrix<double,0,int>[npars];
  if( init_list.containsElementNamed("theta"))
    this->vec_to_theta(Rcpp::as<Eigen::VectorXd>(init_list["theta"]));


    fixed = VectorXd::Zero(npars);
}
void MultivariateMaternMatrixOperator::initFromList(Rcpp::List const & init_list, Rcpp::List const & solver_list)
{
  this->initFromList(init_list);
}
Eigen::SparseMatrix<double,0,int> MultivariateMaternMatrixOperator::df(int i)
{

  int j=0;
  if(i<nkp.sum()) { //kappa derivative
    while(i > nkp.head(j).sum()){
      j++;
    }
    Mk[i].resize(n,n);
    Mk[i] = Phi[j] * (Phik[i]* Clist[j]);
  } else { //phi derivative
    while(i > nkp.sum()+nphi.head(j).sum()){
      j++;
    }
    Mk[i].resize(n,n);
    Mk[i] = Phik[i] * Klist[j];
  }

  SparseMatrix<double,0,int> dKphi;
  dKphi.resize(d,d);
  setSparseBlock(&dKphi,nop_ij(j,0)*n, nop_ij(j,1)*n, Mk[i]);
  return dKphi;
}


Eigen::SparseMatrix<double,0,int> MultivariateMaternMatrixOperator::d2f(int i,int j)
{
  throw("not implimented MaternMatrixOperator::d2f\n");
}
double MultivariateMaternMatrixOperator::trace(int i)
{
  SparseMatrix<double,0,int> tmp1 = this->df(i);
  return (*Qsolver).trace(tmp1);
}


double MultivariateMaternMatrixOperator::trace2(int i,int j)
{
   Rcpp::Rcout << "GeneralMatrixOperator trace2 not defined\n";
   return(std::numeric_limits<double>::infinity());
}

void MultivariateMaternMatrixOperator::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  
 int kk=0;
  for(int iop=0;iop<nop;iop++){
    kpv[iop] = theta_vec_in.segment(kk,nkp(iop));
    kk +=nkp(iop);
  }
  for(int iop=0;iop<nop;iop++){
    phiv[iop] = theta_vec_in.segment(kk,nphi(iop));
    kk +=nphi(iop);
  }
  kk =-1; 
  int ii=0;
 for(int iop=0;iop<nop;iop++){
    dkappa = dkappa_mat(Bkp[iop],kpv[iop]);
    for(int i = 0;i<nkp(iop);i++){
      
      for(int j=0;j<n;j++){
        Phik[ii].coeffRef(j,j) = dkappa(j,i);
      }
      ii++;
    }
  }
  
  for(int iop=0;iop<nop;iop++){
    dphi = dkappa_mat(Bphi[iop],phiv[iop]);
    for(int i = 0;i<nphi(iop);i++){
      for(int j=0;j<n;j++){
        Phik[ii].coeffRef(j,j) = dphi(j,i);
        
      }
      ii++;
    }
  }
  
    for(int i=0;i<nop;i++){
    kappa = kappa_vec(Bkp[i]  , kpv[i]);
    phi   = kappa_vec(Bphi[i], phiv[i]);
    Phi[i].diagonal() = phi;
    Klist[i] = Glist[i] + kappa.asDiagonal()*Clist[i];
    theta = theta_vec_in;
  }


  SparseMatrix<double,0,int> KP;

  kk =0; 
  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(S(i,j) == 1){
        KP = Phi[kk] * Klist[kk];
        setSparseBlock_update(&Q,i*n, j*n, KP);
        
        kk++;
      }
    }
  }
  
  (*Qsolver).compute(Q);
}

double MultivariateMaternMatrixOperator::f(const Eigen::VectorXd& theta_vec_in)
{
  double v = 0;
  //if(kappa.minCoeff() < 0)
  //  v = std::numeric_limits<double>::infinity();
  return v;
}




void MultivariateMaternMatrixOperator::show_results()
{
  Rcpp::Rcout << "operator  : \n"; 
   int kk=0;
  for(int iop=0;iop<nop;iop++){
     Rcpp::Rcout << "kappa[" <<iop << "]= " << kpv[iop].exp() <<"\n";
  }
  for(int iop=0;iop<nop;iop++){
     Rcpp::Rcout << "phi[" <<iop << "]= " << phiv[iop].exp() <<"\n";
  }
  Rcpp::Rcout << "***************************************\n"; 
}

Eigen::VectorXd MultivariateMaternMatrixOperator::get_theta()
{
  return theta;
}

Rcpp::List MultivariateMaternMatrixOperator::output_list()
{
  Rcpp::List List;
  List["theta"] = theta;
  List["S"]     = S;
  List["B.kappa"] =  Bkp[0];
  List["B.kappa"] =  Bphi[0];
  List["C"]       =  Clist[0];
  List["Ci"]      =  Cilist[0];
  List["G"]       =  Glist[0];
  List["K"]       =  Q;
    
  for(int i=0;i<nop;i++){
    std::ostringstream s;
    Rcpp::List OpList;
    OpList["kappa2"] = kpv[i];
    OpList["phi"] = phiv[i];
    List[s.str()] = OpList;
  }
  return(List);
}

Eigen::VectorXd MultivariateMaternMatrixOperator::exp_param()
{
  Eigen::VectorXd param_exp;
  double kk = 0;
  for(int iop=0;iop<nop;iop++)
    kk +=nkp(iop);
  
  for(int iop=0;iop<nop;iop++)
    kk +=nphi(iop);
  param_exp.resize(kk);
  param_exp.setOnes();
  return(param_exp);
}