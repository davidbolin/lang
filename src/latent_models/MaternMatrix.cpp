#include "Qmatrix.h"

using namespace std;

void MaternMatrix::initFromList(Rcpp::List const & init_list)
{
  G  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["G"]);
  C  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["C"]);
  Ci = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Ci"]);
  int n = G.rows();
  d = n;
  kpv = Rcpp::as<Eigen::VectorXd>(init_list["kappa"]);
  phiv = Rcpp::as<Eigen::VectorXd>(init_list["phi"]);

  nkp = kpv.size();
  nphi = phiv.size();

  Bkp = Rcpp::as<Eigen::MatrixXd>(init_list["B.kappa"]);
  Bphi = Rcpp::as<Eigen::MatrixXd>(init_list["B.phi"]);

  kappa = Bkp*kpv;
  phi = Bphi*phiv;
  Phi.resize(n);
  Phi.diagonal() = phi;
  K = G + kappa.asDiagonal()*C;

//  Rcpp::Rcout << "    Spatial model: Matern with n = " << d << endl;
  npars = nkp + nphi;

  theta.setZero(npars);
  theta.segment(0,nkp) = kpv;
  theta.segment(nkp,nphi) = phiv;

  use_chol = Rcpp::as<int>(init_list["use.chol"]);

  if(use_chol==1){
    Qsolver = new cholesky_solver;
  } else {
    Qsolver = new iterative_solver;
  }
  (*Qsolver).initFromList(n,init_list);
  (*Qsolver).analyze(K);
  (*Qsolver).compute(K);

  Phik = new SparseMatrix<double,0,int>[npars];
  int ii=0;
  for(int i = 0;i<nkp;i++){
    Phik[ii].resize(n,n);
    for(int j=0;j<n;j++){
      Phik[ii].insert(j,j) = Bkp(j,i);
    }
    ii++;
  }
  for(int i = 0;i<nphi;i++){
    Phik[ii].resize(n,n);
    for(int j=0;j<n;j++){
      Phik[ii].insert(j,j) = Bphi(j,i);
    }
    ii++;
  }
  KCK = K.transpose()*Ci*K;
  Q = Phi*KCK*Phi;

  Mk = new SparseMatrix<double,0,int>[nkp];
}

Eigen::SparseMatrix<double,0,int> MaternMatrix::df(int i)
{
  if(i<nkp) { //kappa derivative
    Mk[i] = C.transpose()*Phik[i]*Ci*K + K.transpose()*Ci*Phik[i]*C;
    return Phi*Mk[i]*Phi;
  } else { //phi derivative
    return Phi*KCK*Phik[i] + Phik[i]*KCK*Phi;
  }
}

Eigen::SparseMatrix<double,0,int> MaternMatrix::d2f(int i,int j)
{
  if(i<nkp) { //kappa
    if(j<nkp){
      return Phi*C.transpose()*(Phik[i]*Ci*Phik[j]+ Phik[j]*Ci*Phik[i])*C*Phi;
    } else {
      return  Phi*Mk[i]*Phik[j-nkp] + Phik[j-nkp]*Mk[i]*Phi;
    }
  } else {
    if(j<nkp){
      return Phi*Mk[j]*Phik[i-nkp] + Phik[i-nkp]*Mk[j]*Phi;
    } else {
      return Phik[i]*KCK*Phik[j]+Phik[j]*KCK*Phik[i];
    }
  }
}

double MaternMatrix::trace(int i)
{
  if(i<nkp) { //kappa
    SparseMatrix<double,0,int> tmp1 = Phik[i]*C;
    return (*Qsolver).trace(tmp1);
  } else {
    return (Bphi.col(i-nkp)).cwiseQuotient(phi).sum();
  }
}

double MaternMatrix::trace2(int i,int j)
{
  if(i<nkp){
    if(j<nkp){
      if(use_chol){
        double eps = 0.0001;
        double trj = trace(j);
        kpv(i) = kpv(i) + eps;
        kappa = Bkp*kpv;
        K = G+kappa.asDiagonal()*C;
        (*Qsolver).compute(K);
        double trje = trace(j);
        kpv(i) = kpv(i) - eps;
        kappa = Bkp*kpv;
        K = G+kappa.asDiagonal()*C;
        (*Qsolver).compute(K);
        return (trje - trj)/eps;
      } else {
        SparseMatrix<double,0,int> tmp1 = Phik[i]*C;
        SparseMatrix<double,0,int> tmp2 = Phik[j]*C;
        return - (*Qsolver).trace2(tmp1,tmp2);
      }
    } else {
      return 0;
    }
  } else {
    if(j<nkp){
      return 0;
    } else {
      VectorXd tmpv = Bphi.col(i-nkp).cwiseProduct(Bphi.col(j-nkp));
      return -tmpv.cwiseQuotient(phi2).sum();
    }
  }
}

void MaternMatrix::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  theta = theta_vec_in;
  kpv = theta_vec_in.segment(0,nkp);
  phiv = theta_vec_in.segment(nkp,nphi);
  kappa = Bkp*kpv;
  phi = Bphi*phiv;
  Phi.diagonal() = phi;

  K = G+kappa.asDiagonal()*C;
  KCK = K.transpose()*Ci*K;
  (*Qsolver).compute(K);
  ldet = 2*(*Qsolver).logdet() + 2*phi.array().log().sum();
  Q = Phi*KCK*Phi;
  phi2 = phi.array().square();
}

double MaternMatrix::f(const Eigen::VectorXd& theta_vec_in)
{
  double v = 0;
  if(theta_vec_in.minCoeff() < 0)
    v = numeric_limits<double>::infinity();
  return v;
}

void MaternMatrix::show_results()
{
  Rcpp::Rcout << ", kappa2 = " << kpv.transpose();
  Rcpp::Rcout << ", phi= " << phiv.transpose();
}

Eigen::VectorXd MaternMatrix::get_theta()
{
  return theta;
}

Rcpp::List MaternMatrix::output_list()
{
  Rcpp::List List;
  List["kappa2"] = kpv;
  List["phi"] = phiv;
  return(List);
}
