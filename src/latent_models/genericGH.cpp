#include "latentGH.h"
#include "error_check.h"


void genericKGH::initFromList(Rcpp::List const &init_list)
{
  std::vector<std::string> check_names =  {"estimateOperatorLatent"};
  check_Rcpplist(init_list, check_names, "genericKGH::initFromList");
  estimateOperator = init_list["estimateOperatorLatent"];
}

Eigen::VectorXd genericKGH::exp_param()
{ 
   if(estimateOperator == 0)
    return(noiseClass->estimate_exp());
  
    Eigen::VectorXd param_exp;
    param_exp.resize(Kobj->npars + noiseClass->npars);
    param_exp.head(Kobj->npars) = Kobj->exp_param();
    param_exp.tail(noiseClass->npars) = noiseClass->estimate_exp();
    return(param_exp);
}

Rcpp::List genericKGH::output_list()
{
  Rcpp::List List;
  List["operator"] = Kobj->output_list();
  List["noise"]      = noiseClass->toRcppList();
  List["estimateOperatorLatent"] = estimateOperator;
   
  return(List);
}
void genericKGH::vec_to_theta(const Eigen::VectorXd & thetaVec) {
  
  
   if(estimateOperator == 0){
    noiseClass->vec_to_theta(thetaVec);
   }else{
    Kobj->vec_to_theta(thetaVec.head(Kobj->npars));
    noiseClass->vec_to_theta(thetaVec.tail(noiseClass->npars));
   }
}
Eigen::VectorXd genericKGH::get_theta()
{
  if(estimateOperator == 0)
    return(noiseClass->get_theta());
  
  VectorXd theta_out;
  theta_out.setZero(noiseClass->npars + Kobj->npars);
  theta_out << Kobj->get_theta(),
               noiseClass->get_theta();
  return(theta_out);
}
void genericKGH::show_results()
{
  Rcpp::Rcout << "\n*********************\n";
  Rcpp::Rcout << "latent:\n";
  if(estimateOperator == 1)
    Kobj->show_results();
  noiseClass->show_results();
  Rcpp::Rcout << "*********************\n";  
}
void genericKGH::update_param()
{
  Kobj->update_param();
  noiseClass->update_param();
}

double genericKGH::f(const Eigen::VectorXd& theta_in) {

 if(estimateOperator == 0)
   return(noiseClass->f(theta_in));
   
   double f_noise = noiseClass->f(theta_in.tail(noiseClass->npars));
   if(f_noise != 0)
    return(f_noise);
    
   return(Kobj->f(theta_in.head(Kobj->npars)));
}

Eigen::VectorXd genericKGH::df()
{
  if(estimateOperator == 0)
    return( noiseClass->df());

 
  Eigen::VectorXd  df_Ki;
  df_Ki.setZero(Kobj->npars);

  Eigen::VectorXd g;
  g.setZero(Kobj->npars + noiseClass->npars);
  g.tail(noiseClass->npars) = noiseClass->df();
  // if repeated measuremetn change line below!
  // start looping here, over X, dK
  for(int i = 0; i < Kobj->npars; i++){
    Eigen::VectorXd dK_X = Kobj->df(i) * (*X);
    df_Ki(i) =  noiseClass->resid.dot(dK_X);
    df_Ki(i) -= Kobj->trace(i);
  }
  g.head(Kobj->npars) =  df_Ki;
  g.array() *= 1.0 -  fixed.array();
  return(g);
}

void genericKGH::updateNoise()
{
  // if repeated measuremetn change line below!
  Eigen::VectorXd E = Kobj->Q * (*X);
  noiseClass->setE(&E);
  noiseClass->update();
}
void genericKGH::update()
{
  Kobj->update();
  noiseClass->updateV();
  
  // if repeated measuremetn change line below!
  // start loop here, and now Q_post must be vector of Q_post, mu_post
  

  
  Q_post = Eigen::SparseMatrix<double,0,int>(Kobj->Q.transpose());
  mu_post  =  Q_post * noiseClass->muE;
  Q_post  = Q_post  * (noiseClass->iVsigma).asDiagonal() ;
  Q_post  = Q_post  * Kobj->Q;
}

void genericKGH::set_K(Qmatrix *K_in){ 
  Kobj = K_in;
  if(estimateOperator == 1)
    npars += Kobj->npars;
    
  if(npars > fixed.size()){
    Eigen::VectorXd  fixed2;
    fixed2.setZero(npars);
    fixed2.head(Kobj->npars) = Kobj->fixed;
    fixed2.tail(fixed.size()) = fixed;
    fixed.resize(npars);
    fixed = fixed2;
  }
}

void genericKGH::set_fixed(const Eigen::VectorXd& fixed_in)   
{ 
  fixed.resize(fixed_in.size());
  fixed = fixed_in; 
   
  noiseClass->fixed.resize(noiseClass->npars);
  noiseClass->fixed = fixed.tail(noiseClass->npars);
};

void genericKGH::setNoise(GHnoise *noise)
{
  noiseClass = noise; 
  npars += noiseClass->npars;
  mu_post.resize(noiseClass->n);
  if(npars > fixed.size()){
    Eigen::VectorXd  fixed2;
    fixed2.setZero(npars);
    fixed2.head(fixed.size()) = fixed;
    fixed.resize(npars);
    fixed = fixed2;
  }
  
  fixed.tail(noiseClass->npars) = noiseClass->fixed;
  
};