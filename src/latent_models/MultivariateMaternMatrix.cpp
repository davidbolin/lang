#include "Qmatrix.h"
#include "error_check.h"
using namespace Rcpp;
using namespace std;

void MultivariateMaternMatrix::initFromList(Rcpp::List const & init_list)
{
   std::vector<std::string> check_names =  {"S","C", "G", "Ci","B.kappa","B.phi","kappa","phi","use.chol"};
  check_Rcpplist(init_list, check_names, "MultivariateMaternMatrix::initFromList");

  S = Rcpp::as<Eigen::MatrixXd>(init_list["S"]);
  dim = S.rows();
  nop = S.sum();
  nop_ij.resize(nop,2);
  int k = 0;
  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(S(i,j) == 1){
        nop_ij(k,0) = i;
        nop_ij(k,1) = j;
        k++;
      }
    }
  }
  nkp.setOnes(nop);
  nphi.setOnes(nop);

  kpv =        new VectorXd[nop];
  phiv =       new VectorXd[nop];
  kappa =      new VectorXd[nop];
  phi =        new VectorXd[nop];
  d2kappa =    new MatrixXd[nop];
  d2phi =      new MatrixXd[nop];
  dkappadphi = new MatrixXd[nop];
  Bkp =        new MatrixXd[nop];
  Bphi =       new MatrixXd[nop];
  Kappa =      new DiagonalMatrix<double,Dynamic>[nop];
  Phi =        new DiagonalMatrix<double,Dynamic>[nop];
  Klist =      new SparseMatrix<double,0,int>[nop];
  Clist =      new SparseMatrix<double,0,int>[nop];
  Cilist =     new SparseMatrix<double,0,int>[nop];
  Glist =      new SparseMatrix<double,0,int>[nop];
  npars = 0;
  for(int i=0;i<nop;i++){
    kpv[i]  =   Rcpp::as<Eigen::VectorXd>(init_list["kappa"]);
    phiv[i] =   Rcpp::as<Eigen::VectorXd>(init_list["phi"]);
    Bkp[i]  =   Rcpp::as<Eigen::MatrixXd>(init_list["B.kappa"]);
    Bphi[i] =   Rcpp::as<Eigen::MatrixXd>(init_list["B.phi"]);
    Clist[i] =  Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["C"]);
    Cilist[i] = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Ci"]);
    Glist[i] =  Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["G"]);
    nkp(i)  = kpv[i].size();
    nphi(i) = phiv[i].size();

    kappa[i]= Bkp[i]*kpv[i];
    phi[i]  = Bphi[i]*phiv[i];
    n = Glist[i].rows();
    Phi[i].resize(n);
    Phi[i].diagonal() = phi[i];
    Klist[i] = Glist[i] + kappa[i].asDiagonal()*Clist[i];
    npars += nkp(i) + nphi(i);
  }

  d = n*dim;
  Ci.resize(d,d);
  G.resize(d,d);
  C.resize(d,d);
  K.resize(d,d);
  Kphi.resize(d,d);
  int kk = 0;
  Eigen::SparseMatrix<double,0,int> KP;
  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(i==j)
        setSparseBlock(&Ci,i*n, j*n, Cilist[kk]);

      if(S(i,j) == 1){
        setSparseBlock(&C,i*n, j*n, Clist[kk]);
        setSparseBlock(&G,i*n, j*n, Glist[kk]);
        setSparseBlock(&K,i*n, j*n, Klist[kk]);
        KP = Eigen::SparseMatrix<double,0,int>(Klist[kk]*Phi[kk]);
        KP.makeCompressed();
        setSparseBlock(&Kphi,i*n, j*n, KP);
        kk++;
      }
    }
  }

  theta.setZero(npars);
  kk=0;
  int ii=0;
  Phik = new SparseMatrix<double,0,int>[npars];
  for(int iop=0;iop<nop;iop++){
    theta.segment(kk,nkp(iop)) = kpv[iop];
    kk +=nkp(iop);
    for(int i = 0;i<nkp(iop);i++){
      Phik[ii].resize(n,n);
      for(int j=0;j<n;j++){
        Phik[ii].insert(j,j) = Bkp[iop](j,i);
      }
      Phik[ii].makeCompressed();
      ii++;
    }
  }

  for(int iop=0;iop<nop;iop++){
    theta.segment(kk,nphi(iop)) = phiv[iop];
    kk +=nphi(iop);
    for(int i = 0;i<nphi(iop);i++){
      Phik[ii].resize(n,n);
      for(int j=0;j<n;j++){
        Phik[ii].insert(j,j) = Bphi[iop](j,i);
      }
      Phik[ii].makeCompressed();
      ii++;
    }
  }

  use_chol = Rcpp::as<int>(init_list["use.chol"]);
  if(use_chol==1){
    Qsolver = new lu_sparse_solver;
  } else {
    Qsolver = new iterative_solver;
  }

  (*Qsolver).initFromList(d,init_list);
  (*Qsolver).analyze(Kphi);
  (*Qsolver).compute(Kphi);
  Q = Eigen::SparseMatrix<double,0,int>(Kphi.transpose()) * Ci * Kphi;
  Mk = new SparseMatrix<double,0,int>[npars];

}

Eigen::SparseMatrix<double,0,int> MultivariateMaternMatrix::df(int i)
{

  int j=0;
  if(i<nkp.sum()) { //kappa derivative
    while(i > nkp.head(j).sum()){
      j++;
    }
    Mk[i].resize(n,n);
    //Mk[i]  = Phi[j]*(SparseMatrix<double,0,int>(Clist[j].transpose())*Phik[i]*Cilist[j]*Klist[j]);
    Mk[i] = (Phik[i]* Clist[j])* Phi[j];
  } else { //phi derivative
    while(i > nkp.sum()+nphi.head(j).sum()){
      j++;
    }
    Mk[i].resize(n,n);
    //Mk[i] = Phi[j]*Klist[j]*Cilist[j]*Klist[j]*Phik[i];
    Mk[i] = Phik[i]*Klist[j]*Phi[j];
  }

  SparseMatrix<double,0,int> dKphi;
  dKphi.resize(d,d);
  setSparseBlock(&dKphi,nop_ij(j,0)*n, nop_ij(j,1)*n, Mk[i]);
  SparseMatrix<double,0,int> res =  SparseMatrix<double,0,int>(dKphi.transpose()) *  Ci * Kphi;
  res += SparseMatrix<double,0,int>(res.transpose());
  return res;

}

Eigen::SparseMatrix<double,0,int> MultivariateMaternMatrix::d2f(int i,int j)
{

  int j1=0; //The operator parameter i affects
  int j2=0; //The operator parameter j affects
  SparseMatrix<double,0,int> Out, tmp;
  Out.resize(d,d);
  if(i<nkp.sum()) { //kappa
    while(i > nkp.head(j1).sum()){
      j1++;
    }
    if(j<nkp.sum()){ //kappa
      while(j2 < nop &&  j > nkp.head(j2).sum()){
        j2++;
      }
      if(j1 == j2) {
        tmp = Phi[j1]*Eigen::SparseMatrix<double,0,int>(Clist[j1].transpose())*
        (Phik[i]*Cilist[j1]*Phik[j]+ Phik[j]*Cilist[j1]*Phik[i])*Clist[j1]*Phi[j1];
        setSparseBlock(&Out,nop_ij(j1,0)*n,nop_ij(j1,1)*n,tmp);
      }
    } else { //phi
      while(i > nkp.sum()+nphi.head(j2).sum()){
        j2++;
     }
      if(j1 == j2) {
        tmp = Phi[j1]*Mk[i]*Phik[j-nkp.sum()] + Phik[j-nkp.sum()]*Mk[i]*Phi[j1];
        setSparseBlock(&Out,nop_ij(j1,0)*n,nop_ij(j1,1)*n,tmp);
      }
    }
  } else {
    while(i > nkp.sum()+nphi.head(j1).sum()){
      j1++;
    }
    if(j<nkp.sum()){
      while(j > nkp.head(j2).sum()){
        j2++;
      }
      if(j1==j2){
        tmp = Phi[j1]*Mk[j]*Phik[i-nkp.sum()] + Phik[i-nkp.sum()]*Mk[j]*Phi[j1];
        setSparseBlock(&Out,nop_ij(j1,0)*n,nop_ij(j1,1)*n,tmp);
      }
    } else {
      while(j > nkp.sum() + nphi.head(j2).sum()){
        j2++;
      }
      if(j1==j2){
        tmp = Phik[i]*Eigen::SparseMatrix<double,0,int>(Klist[j1].transpose())*
        Cilist[j1]*Klist[j1]*Phik[j]+Phik[j]*
        Eigen::SparseMatrix<double,0,int>(Klist[j1].transpose())*Cilist[j1]*Klist[j1]*Phik[i];
        setSparseBlock(&Out,nop_ij(j1,0)*n,nop_ij(j1,1)*n, tmp);
      }
    }
  }
  return Out;
}

double MultivariateMaternMatrix::trace(int i)
{
  SparseMatrix<double,0,int> tmp1 = this->df(i);
  return (*Qsolver).trace(tmp1);
}


double MultivariateMaternMatrix::trace2(int i,int j)
{
  if(use_chol){
    double eps = 0.0001;

    theta(i) += eps;
    this->vec_to_theta(theta);
    SparseMatrix<double,0,int> tmp1 = this->df(i);
    double trje = (*Qsolver).trace(tmp1);

    theta(i) -= eps;
    this->vec_to_theta(theta);
    tmp1 = this->df(i);

    double trj = (*Qsolver).trace(tmp1);
    return (trje - trj)/eps;
  } else {
    SparseMatrix<double,0,int> tmp1 = this->df(i);
    SparseMatrix<double,0,int> tmp2 = this->df(j);
    return - (*Qsolver).trace2(tmp1,tmp2);
  }
}

void MultivariateMaternMatrix::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  theta = theta_vec_in;

  int kk=0;
  for(int iop=0;iop<nop;iop++){
    kpv[iop] = theta.segment(kk,nkp(iop));
    kk +=nkp(iop);
  }
  for(int iop=0;iop<nop;iop++){
    phiv[iop] = theta.segment(kk,nphi(iop));
    kk +=nphi(iop);
  }

  for(int i=0;i<nop;i++){
    kappa[i] = Bkp[i]*kpv[i];
    phi[i] = Bphi[i]*phiv[i];
    Phi[i].resize(n);
    Phi[i].diagonal() = phi[i];
    Klist[i] = Glist[i] + kappa[i].asDiagonal()*Clist[i];
  }

  kk = 0;
  Kphi.resize(d,d);
  K.resize(d,d);

  SparseMatrix<double,0,int> KP;

  for(int i=0;i<dim;i++){
    for(int j=0;j<dim;j++){
      if(S(i,j) == 1){
        setSparseBlock(&K,i*n, j*n, Klist[kk]);
        KP = Klist[kk]*Phi[kk];
        setSparseBlock(&Kphi,i*n, j*n, KP);
        kk++;
      }
    }
  }

  Q = Eigen::SparseMatrix<double,0,int>(Kphi.transpose())*Ci*Kphi;
  (*Qsolver).compute(Kphi);

  ldet = 2*(*Qsolver).logdet();
}

double MultivariateMaternMatrix::f(const Eigen::VectorXd& theta_vec_in)
{
  double v = 0;
  if(theta_vec_in.minCoeff() < 0)
    v = numeric_limits<double>::infinity();
  return v;
}

void MultivariateMaternMatrix::show_results()
{
  for(int i=0;i<nop;i++){
    Rcpp::Rcout << ", operator " << i << ": ";
    Rcpp::Rcout << ", kp2 = " << kpv[i].transpose();
    Rcpp::Rcout << ", phi= " << phiv[i].transpose();
  }
}

Eigen::VectorXd MultivariateMaternMatrix::get_theta()
{
  return theta;
}

Rcpp::List MultivariateMaternMatrix::output_list()
{
  Rcpp::List List;

  for(int i=0;i<nop;i++){
    Rcpp::List OpList;
    OpList["kappa2"] = kpv[i];
    OpList["phi"] = phiv[i];
    List[i] = OpList;
  }
  return(List);
}
