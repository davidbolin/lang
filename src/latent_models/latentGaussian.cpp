#include "latentGaussian.h"
#include <RcppEigen.h>
#include "TimeIt.h"

using namespace std;

void latentGaussian::initFromList(Rcpp::List const & init_list)
{
  Rcpp::List Qs_list = init_list;

  int Qs_type = Rcpp::as<int>(Qs_list["type"]);

  if(Qs_type==0){
    Qs = new MaternMatrix;
  } else if(Qs_type == -1){
    Qs = new constMatrix;
  } else if(Qs_type == 4){
    Qs = new MultivariateMaternMatrix;
  }
  (*Qs).initFromList(Qs_list);

  n = ((*Qs).d);
  Q_post = (*Qs).Q;

  npars = (*Qs).npars;

  g.setZero(npars);
  p.setZero(npars);
  H.setZero(npars,npars);
}

void latentGaussian::set_solver(solver * R)
{
  Qhsolver = R;
}

void latentGaussian::set_matrices(VectorXd * Xin)
{
  X = Xin;
}

VectorXd latentGaussian::df()
{
  double trMk;
  //Rcpp::Rcout << "in latentGaussian::df:" << endl;
  for(int i= 0;i<(*Qs).npars;i++)
  {
    Mk = (*Qs).df(i);
    trMk = (*Qhsolver).trace(Mk);
    g(i) = - (*Qs).trace(i) + ((*X).dot(Mk*(*X)) + trMk)/2.0;
    for(int j=i;j<(*Qs).npars;j++)
    {
      Mk = (*Qs).d2f(i,j);
      trMk = (*Qhsolver).trace(Mk);
      H(i,j) = (*Qs).trace2(i,j) -((*X).dot(Mk*(*X)) + trMk)/2.0;
      H(j,i) = H(i,j);
    }
  }

  //Rcpp::Rcout << "Done in latentGaussian::df\n" << endl;
  return g;
}


VectorXd latentGaussian::direction()
{
  VectorXd e = H.eigenvalues().real();

  if(e.maxCoeff() > 0){
    VectorXd Hd = H.diagonal();
    H.setZero(npars,npars);
    for(int i=0;i<npars;i++){
      H(i,i) = Hd(i);
    }

    e = H.eigenvalues().real();
    if(e.maxCoeff() > 0){
      cout << "Warning diag(Hlatent) not neg def" << endl;
      double m = 1.1*H.diagonal().maxCoeff();
      for(int i=0;i<npars;i++){
        H(i,i) -=m;
      }
    }
  }
  p = H.ldlt().solve(g);

  return p;
}

void latentGaussian::vec_to_theta(const VectorXd& theta_vec_in)
{
  (*Qs).vec_to_theta(theta_vec_in.segment(0,(*Qs).npars));
  logdetQ = (*Qs).logdet();
  Q_post = (*Qs).Q;
}

double latentGaussian::f(const VectorXd& theta_vec_in)
{
  vec_to_theta(theta_vec_in);

  return (*Qs).f(theta_vec_in.segment(0,(*Qs).npars));
}

void latentGaussian::show_results()
{
  (*Qs).show_results();
}

VectorXd latentGaussian::get_theta()
{
  return (*Qs).get_theta();
}

Rcpp::List latentGaussian::output_list()
{
  return((*Qs).output_list());
}
