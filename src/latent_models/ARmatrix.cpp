#include "Qmatrix.h"

using namespace std;

void ARmatrix::initFromList(Rcpp::List const & init_list)
{

  phiv = Rcpp::as<Eigen::VectorXd>(init_list["phi"]);
  Bphi = Rcpp::as<Eigen::MatrixXd>(init_list["B.phi"]);
  d = Bphi.rows();
  Rcpp::Rcout << "    Temporal model: AR with d = " << d << endl;

  npars = phiv.size();
  Phik = new SparseMatrix<double,0,int>[npars];

  for(int i = 0;i<npars;i++){
    Phik[i].resize(d,d);
    for(int j=0;j<d;j++){
      Phik[i].insert(j,j) = Bphi(j,i);
    }
  }

  I.resize(d,d);
  for(int i=0;i<d;i++){
    I.insert(i,i) = 1;
  }

  I0.resize(d,d);
  for(int i=0;i<d-1;i++){
    I0.insert(i,i) = 1;
  }

  I11.resize(d,d);
  for(int i=0;i<d-1;i++){
    I11.insert(i,i+1) = -1;
  }
  I12 = I11.transpose();

  phi = Bphi*phiv;
  phi2 = phi.array().square();

  Phi.diagonal() = phi;
  Q = I + Phi*I0*Phi + Phi*I11+I12*Phi;
  //Rcpp::Rcout << "I =  " << endl << I << endl;
  ///Rcpp::Rcout << "I0 =  " << endl << I0 << endl;
  //Rcpp::Rcout << "I11 =  " << endl << I11 << endl;
  //Rcpp::Rcout << "I12 =  " << endl << I12 << endl;
  //Rcpp::Rcout << "Q =  " << endl << Q << endl;

}

Eigen::SparseMatrix<double,0,int> ARmatrix::df(int i)
{
  return Phik[i]*I0*Phi + Phi*I0*Phik[i] + Phik[i]*I11 + I12*Phik[i];
}

Eigen::SparseMatrix<double,0,int> ARmatrix::d2f(int i,int j)
{
  return Phik[i]*I0*Phik[j] + Phik[j]*I0*Phik[i];
}


void ARmatrix::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  phiv = theta_vec_in;
  phi = Bphi*phiv;
  phi2 = phi.array().square();
  Phi.diagonal() = phi;
  Q = I + Phi*I0*Phi + Phi*I11+I12*Phi;
}

double ARmatrix::f(const Eigen::VectorXd& theta_vec_in)
{
  double v = 0;
  if(theta_vec_in.minCoeff() < -1)
    v = numeric_limits<double>::infinity();
  return v;
}

void ARmatrix::show_results()
{
  Rcpp::Rcout << ", ar = " << phiv.transpose();
}

Eigen::VectorXd ARmatrix::get_theta()
{
  return phiv;
}

Rcpp::List ARmatrix::output_list()
{
  Rcpp::List List;
  List["ar"] = phiv;
  return(List);
}