#include "latentGaussian.h"

#include <RcppEigen.h>
#include "TimeIt.h"

using namespace std;

void proportionalGaussian::initFromList(Rcpp::List const & init_list)
{
  Rcpp::List Qd_list = init_list["Qd.list"];
  Rcpp::List Qs_list = init_list;

  int Qs_type = Rcpp::as<int>(Qs_list["type"]);
  int Qd_type = Rcpp::as<int>(Qd_list["type"]);

  if(Qs_type==0){
    Qs = new MaternMatrix;
  } else if(Qs_type == -1){
    Qs = new constMatrix;
  } else if(Qs_type == 3){
    Qs = new generic0Matrix;
  } else {
    Rcpp::Rcout << "Should not be here, wrong Qs type" << std::endl;
  }
  (*Qs).initFromList(Qs_list);

  if(Qd_type==0){
    Qd = new MaternMatrix;
  } else if(Qd_type == -1){
    Qd = new constMatrix;
  } else if(Qd_type == 2){
    Qd = new ARmatrix;
  } else if(Qd_type == 3){
    Qd = new generic0Matrix;
  } else {
    Rcpp::Rcout << "Should not be here, wrong Qd type" << std::endl;
  }
  (*Qd).initFromList(Qd_list);

  n = ((*Qd).d)*((*Qs).d);

  Q_post = kronecker((*Qd).Q,(*Qs).Q);

  if((*Qd).npars>0)
    dtheta.setZero((*Qd).npars);
  dpsi.setZero((*Qs).npars);

  if((*Qd).npars>0)
    d2theta.setZero((*Qd).npars,(*Qd).npars);
  d2psi.setZero((*Qs).npars,(*Qs).npars);

  if((*Qd).npars>0)
    dpsidtheta.setZero((*Qs).npars,(*Qd).npars);

  npars = (*Qd).npars + (*Qs).npars;

  g.setZero(npars);
  p.setZero(npars);
}

void proportionalGaussian::set_solver(solver * R)
{
  Qhsolver = R;
}

void proportionalGaussian::set_matrices(VectorXd * Xin)
{
  X = Xin;
}

VectorXd proportionalGaussian::df()
{
  double trMk;
  SparseMatrix<double,0,int> tmp, tmp2;
  for(int i= 0;i<(*Qs).npars;i++)
  {
    tmp = (*Qs).df(i);
    
    Mk = kronecker((*Qd).Q,tmp);
    trMk = (*Qhsolver).trace(Mk);
    //Rcpp::Rcout << Mk << endl;
    dpsi(i) = (*Qd).d*(*Qs).trace(i) - ((*X).dot(Mk*(*X)) + trMk)/2.0;
    for(int j=i;j<(*Qs).npars;j++)
    {
      tmp = (*Qs).d2f(i,j);
      Mk = kronecker((*Qd).Q,tmp);
      trMk = (*Qhsolver).trace(Mk);
      d2psi(i,j) = (*Qd).d*(*Qs).trace2(i,j) -((*X).dot(Mk*(*X)) + trMk)/2.0;
      d2psi(j,i) = d2psi(i,j);
    }
    for(int j=0;j<(*Qd).npars;j++)
    {
      tmp = (*Qd).df(j);
      tmp2 = (*Qs).df(i);
      Mk = kronecker(tmp,tmp2);
      trMk = (*Qhsolver).trace(Mk);
      dpsidtheta(i,j) = -((*X).dot(Mk*(*X)) + trMk)/2.0;
    }
  }

  for(int i=0;i<(*Qd).npars;i++)
  {
    tmp = (*Qd).df(i);
    Mk = kronecker(tmp,(*Qs).Q);
    trMk = (*Qhsolver).trace(Mk);
    dtheta(i) = (*Qs).d*(*Qd).trace(i)-((*X).dot(Mk*(*X)) + trMk)/2.0;

    for(int j=i;j<(*Qd).npars;j++){
      tmp = (*Qd).d2f(i,j);
      Mk = kronecker(tmp,(*Qs).Q);
      trMk = (*Qhsolver).trace(Mk);
      d2theta(i,j) = (*Qs).d*(*Qd).trace2(i,j) -((*X).dot(Mk*(*X)) + trMk)/2.0;
      d2theta(j,i) = d2theta(i,j);
    }
  }

  if((*Qd).npars>0){
    g << dpsi,
         dtheta;
  } else {
    g << dpsi;
  }
  g = -g;

  return g;
}


VectorXd proportionalGaussian::direction()
{
  H.setZero(npars,npars);
  H.topLeftCorner((*Qs).npars, (*Qs).npars) = d2psi;
  if((*Qd).npars>0){
    H.topRightCorner((*Qs).npars, (*Qd).npars) = dpsidtheta;
    H.bottomLeftCorner((*Qd).npars,(*Qs).npars) = dpsidtheta.transpose();
    H.bottomRightCorner((*Qd).npars,(*Qd).npars) = d2theta;
  }

  VectorXd e = H.eigenvalues().real();

  if(e.maxCoeff() > 0){
    VectorXd Hd = H.diagonal();
    H.setZero(npars,npars);
    for(int i=0;i<npars;i++){
      H(i,i) = Hd(i);
    }

    e = H.eigenvalues().real();
    if(e.maxCoeff() > 0){
      cout << "Warning diag(Hlatent) not neg def" << endl;
      double m = 1.1*H.diagonal().maxCoeff();
      for(int i=0;i<npars;i++){
        H(i,i) -=m;
      }
    }
  }
  p = H.ldlt().solve(g);

  return p;
}

void proportionalGaussian::vec_to_theta(const VectorXd& theta_vec_in)
{
  (*Qs).vec_to_theta(theta_vec_in.segment(0,(*Qs).npars));
  if((*Qd).npars>0)
    (*Qd).vec_to_theta(theta_vec_in.segment((*Qs).npars,(*Qd).npars));
  logdetQ = (*Qd).d*(*Qs).logdet() + (*Qs).d*(*Qd).logdet();
  Q_post = kronecker((*Qd).Q,(*Qs).Q);
}

double proportionalGaussian::f(const VectorXd& theta_vec_in)
{
  vec_to_theta(theta_vec_in);
  return (*Qs).f(theta_vec_in.segment(0,(*Qs).npars)) + (*Qd).f(theta_vec_in.segment((*Qs).npars,(*Qd).npars));
}

void proportionalGaussian::show_results()
{
  (*Qs).show_results();
  (*Qd).show_results();
}

VectorXd proportionalGaussian::get_theta()
{
  VectorXd theta;
  theta.setZero(npars);
  theta.segment(0,(*Qs).npars) = (*Qs).get_theta();
  theta.segment((*Qs).npars,(*Qd).npars) = (*Qd).get_theta();
  return theta;
}

Rcpp::List proportionalGaussian::output_list()
{
  Rcpp::List List;
  List["Qs"] = (*Qs).output_list();
  List["Qd"] = (*Qd).output_list();
  return(List);
}

