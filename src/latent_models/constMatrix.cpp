#include "Qmatrix.h"

using namespace std;

void constMatrix::initFromList(Rcpp::List const & init_list)
{

  Q  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Q"]);
  d = Q.rows();
  npars = 0;
  v.setZero(1);
  m.resize(1,1);
}

Rcpp::List constMatrix::output_list()
{
  Rcpp::List List;
  return(List);
}
