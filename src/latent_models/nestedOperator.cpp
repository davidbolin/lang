#include "Qmatrix.h"
#include "error_check.h"
#include "TimeIt.h"

using namespace std;

void nestedOperator::initFromList(Rcpp::List const & init_list)
{
  theta = Rcpp::as<Eigen::VectorXd>(init_list["vec"]);
  npars = theta.size();
  Rcpp::List Bveclist = init_list["B.vec"];
  Bvec = new SparseMatrix<double,0,int>[npars];

  for(int i = 0; i <npars; i++){
    int j = i+1;
    stringstream ss;
    ss << j;
    string str = ss.str();

    Bvec[i] = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(Bveclist[i]);
  }
  n = Bvec[0].rows();
  I.resize(n,n);
  for(int i=0;i<n;i++){
    I.insert(i,i) = 1;
  }
}

void nestedOperator::initFromList(Rcpp::List const & init_list, Rcpp::List const & optlist)
{
  theta = Rcpp::as<Eigen::VectorXd>(init_list["vec"]);
  npars = theta.size();
  Rcpp::List Bveclist = init_list["B.vec"];
  Bvec = new SparseMatrix<double,0,int>[npars];

  for(int i = 0; i <npars; i++){
    int j = i+1;
    stringstream ss;
    ss << j;
    string str = ss.str();

    Bvec[i] = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(Bveclist[i]);
  }
  n = Bvec[0].rows();
  I.resize(n,n);
  for(int i=0;i<n;i++){
    I.insert(i,i) = 1;
  }
}


Eigen::SparseMatrix<double,0,int> nestedOperator::df(int i)
{
  if(i >= npars)
    throw("nestedOperator::df i must be less then npars");

  return Bvec[i];
}

Eigen::SparseMatrix<double,0,int> nestedOperator::d2f(int i,int j)
{
  throw("not implimented nestedOperator::d2f\n");
}

void nestedOperator::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  theta = theta_vec_in;
  Q = I;
  for(int i=0;i<npars;i++){
    Q += theta(i)*Bvec[i];
  }
}

double nestedOperator::f(const Eigen::VectorXd & theta_in)
{
  double v = 0;
  if(theta(0) < 0)
    v = std::numeric_limits<double>::infinity();
  return v;
}

void nestedOperator::show_results()
{
  Rcpp::Rcout << ", vec = " << theta.transpose();
}
Eigen::VectorXd nestedOperator::get_theta()
{
  return theta;
}
Rcpp::List nestedOperator::output_list()
{
  Rcpp::List List;
  List["theta"] = theta;

  return(List);
}
