#include "Qmatrix.h"
#include "error_check.h"

using namespace std;

void generic0Matrix::initFromList(Rcpp::List const & init_list)
{
  M  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Q"]);
  tau.setZero(1);
  tau(0) = Rcpp::as<double>(init_list["tau"]);
  n_scale = Rcpp::as<double>(init_list["n.scale"]);
  d = M.rows();
  npars = 1;
  O.resize(d,d);
  Q = tau(0)*M;
  Rcpp::Rcout << "using generic0 matrix\n";
}

double generic0Matrix::f(const Eigen::VectorXd & theta_in)
{
  double v = 0;
  if(theta_in.minCoeff() < 0)
    v = numeric_limits<double>::infinity();
  return v;

}
void generic0Matrix::vec_to_theta(const Eigen::VectorXd& theta_in)
{
  tau = theta_in;
  Q = tau(0)*M;
}


Rcpp::List generic0Matrix::output_list()
{
  Rcpp::List List;
  List["tau"] = tau(0);
  return(List);
}

Eigen::SparseMatrix<double,0,int> generic0Matrix::df(int i)
{
  return M;
}

Eigen::SparseMatrix<double,0,int> generic0Matrix::d2f(int i,int j)
{
  return O;
}

double generic0Matrix::trace(int)
{
  return 0.5*n_scale/tau(0);
}

double generic0Matrix::trace2(int,int)
{
  return -0.5*n_scale/(tau(0)*tau(0));
}

double generic0Matrix::logdet()
{
  return n_scale*log(tau(0));
}

void GeneralMatrixOperator::initFromList(Rcpp::List const & init_list)
{    
  std::vector<std::string> check_names =  {"K"};
  check_Rcpplist(init_list, check_names, "GeneralMatrixOperator::initFromList");
  
  K = Rcpp::as<Eigen::MatrixXd>(init_list["K"]);
  Q = K.sparseView();
  n = K.cols();
  npars = K.rows() * K.cols();
  
   if( init_list.containsElementNamed("fixed") )
    fixed =  Rcpp::as<Eigen::VectorXd>(init_list["fixed"]);
  else
    fixed.setZero(npars);
  Qsolver = new lu_solver;
  Qsolver->init(npars, 0, 0, 0);
  Qsolver->compute(K);
}
void GeneralMatrixOperator::initFromList(Rcpp::List const & init_list, Rcpp::List const & solver_list)
{
  initFromList(init_list);
}
Eigen::SparseMatrix<double,0,int> GeneralMatrixOperator::df(int i)
{
  Eigen::SparseMatrix<double,0,int> Df;
  Df.resize(n, n);
  Df.setZero();
  Df.insert((i/n), i % n) = 1.;
  return(Df);
}
double GeneralMatrixOperator::trace(int i)
{
  Eigen::SparseMatrix<double,0,int> Df;
  Df.resize(n, n);
  Df.setZero();
  Df.insert((i/n), i % n) = 1.;
  return(Qsolver->trace(Df));
}


Eigen::SparseMatrix<double,0,int> GeneralMatrixOperator::d2f(int i ,int j )
{
  Rcpp::Rcout << "GenericMatrixOperator::d2f not implimented yet\n";
   throw;
}

double GeneralMatrixOperator::f(const Eigen::VectorXd &theta)
{
  Eigen::VectorXd theta_in = theta;
  Eigen::Map<Eigen::MatrixXd> K_map(&theta_in(0),K.rows(),K.cols());
  Eigen::MatrixXd K_temp = K_map;
  Qsolver->compute(K_temp);
  if(std::isinf(Qsolver->logdet() ))
    return(1);
  return(0);
  
  }

void GeneralMatrixOperator::vec_to_theta(const Eigen::VectorXd &theta_vec)
{
  Eigen::VectorXd theta_in = theta_vec;
  Eigen::Map<Eigen::MatrixXd> K_map(&theta_in(0),K.rows(),K.cols());
  K = K_map;
  Q = K.sparseView();
  Qsolver->compute(K);
};

Eigen::VectorXd GeneralMatrixOperator::get_theta()
{
  Eigen::VectorXd theta_out;
  theta_out.resize(npars);
  Eigen::Map<Eigen::VectorXd> theta_map(&K(0),npars);
  theta_out = theta_map;
  return(theta_out);
  
};

Rcpp::List GeneralMatrixOperator::output_list()
{  Rcpp::List List_out;
  List_out["K"] = K;
  return(List_out);
};

void GeneralMatrixOperator::show_results()
{
  Rcpp::Rcout << "K = \n";
  Rcpp::Rcout << Q << "\n";
};

double GeneralMatrixOperator::logdet()
{Rcpp::Rcout << "GenericMatrixOperator logdet not defined\n"; throw;};

Eigen::VectorXd GeneralMatrixOperator::exp_param()
{
  Eigen::VectorXd out;
  out.setZero(npars);
  return(out);
  
};