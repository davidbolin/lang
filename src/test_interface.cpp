#include <Rcpp.h>
#include <RcppEigen.h>
#include "noiseGH.h"
#include "latentGH.h"
#include "modelGH.h"
#include "likelihoodGaussian.h"
#include "optim.h"
#include "solver.h"
#include "Qmatrix.h"
#include <Eigen/Dense>

// [[Rcpp::export]]
Rcpp::List  multivariate_operator_matern_gradient_test(List init_list_in, int i)
{
  // testing if the gradient is correctly calculated 
  
   List init_list = clone(init_list_in);
  MultivariateMaternMatrixOperator Mat;
  Mat.initFromList(init_list);
  SparseMatrix<double,0,int> df = Mat.df(i);
  Rcpp::List outList;
  df.makeCompressed();
  outList["df"]    = df;
  outList["Q"]     = Mat.Q;
  outList["trace"] = Mat.trace(i);
  return outList;
}


// [[Rcpp::export]]
Rcpp::List  multivariate_matern_gradient_test(List init_list_in, int i)
{
  // testing if the gradient is correctly calculated 
  
   List init_list = clone(init_list_in);
  MultivariateMaternMatrix Mat;
  Mat.initFromList(init_list);
  SparseMatrix<double,0,int> df = Mat.df(i);
  Rcpp::List outList;
  df.makeCompressed();
  outList["df"] = df;
  return outList;
}
// [[Rcpp::export]]
List multivariate_matern_test(List init_list_in)
{
  List init_list = clone(init_list_in);
  // SEXP names = Rf_getAttrib(init_list, R_NamesSymbol);
  //int n = Rf_length(names);
  //  for (int i=0; i < n; ++i) 
  //      Rcpp::Rcout << CHAR(STRING_ELT(names, i)) << "\n";
  SparseMatrix<double,0,int> Cbig = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["CCC"]);
  
  SparseMatrix<double,0,int> Kbig = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["KKK"]);
  SparseMatrix<double,0,int> Qbig = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["QQQ"]);
  
  MultivariateMaternMatrix Mat;
  Mat.initFromList(init_list);
  Mat.d2f(0,0);
  Mat.d2f(0,1);
  Mat.trace(0);
  Mat.trace2(0,0);
  Mat.trace2(0,1);
   solver *Qsolver = new cholesky_solver;
  

  
 
  (*Qsolver).analyze( Mat.Q );
  (*Qsolver).compute(Mat.Q  );
  
  SparseMatrix<double,0,int> Qbig2 = Kbig.transpose() * Cbig* Kbig;
    (*Qsolver).analyze( Qbig2);
  (*Qsolver).compute(Qbig2 );
  
   (*Qsolver).initFromList(Qbig.rows(), init_list);
  (*Qsolver).analyze( Qbig);
  (*Qsolver).compute(Qbig );
  

Rcpp::List outList;


  return(outList);
}

/*
Object to test if an operator simulate correctly
using Gaussian measurment noise, through std1DimGaussianWOperator

@param spdeObj_in -> list with lists:
                     latent
                     noise
                     likelihood
                     operator
                     solver

*/
// [[Rcpp::export]]
Rcpp::List TestingOperator(Rcpp::List spdeObj_in, int sim)
{
  Rcpp::List spdeObj         = clone(spdeObj_in);
  Rcpp::List latent_list     = spdeObj["latent"];
  Rcpp::List noise_list      = spdeObj["noise"];
  Rcpp::List operator_list   = spdeObj["operator"];
  Rcpp::List solver_list     = spdeObj["solver"];



  /***********************************
    SETTING up BASIC OBJECTS
  ************************************/

  //setting up basic likelihood
  likelihood* likGaussian;
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->set_estimateOperator(0);
  likGaussian->initFromList(spdeObj["likelihood"]);


  //setting up noise class
  GALnoise  noise_class;
  noise_class.initFromList(noise_list);


  //setting up latent class
  latent* latentGH;
  latentGH = new genericKGH;
  latentGH->initFromList( latent_list);
  latentGH->initSolver(solver_list);
  latentGH->setNoise(&noise_class);


   // Setting up operator
  Qmatrix* Kobj;
  /***********
    add if case if adding new operator
  ************/
  if(Rcpp::as<int>(operator_list["type"]) == 0){
     Kobj     = new MaternMatrixOperator;
  }else{
    Rcpp::Rcout << "operator.type " << Rcpp::as<int>(operator_list["type"]) << "not defined \n";
    throw("wrong input\n");
  }

  Kobj->initFromList(operator_list, solver_list);

  likGaussian->set_Kmatrix(*Kobj);
  latentGH->set_K(Kobj);

  modelGH model;
  if(latent_list.containsElementNamed("X") == 1){
    model.X = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
    latentGH->X = &(model.X);
  }else
  {
    Rcpp::Rcout << "modeldrawX2:: requires  X in spde$latent\n";
    throw;
  }

  model.set_latent(*latentGH);
  model.set_likelihood(*likGaussian);
  model.initSolver(spdeObj["solver"]);



  /***********************************
    DONE with SETUP of BASIC OBJECTS
  ************************************/
  noise_class.V = Rcpp::as<Eigen::VectorXd>(latent_list["V"]);
  likGaussian->set_estimateOperator(1 - latentGH->estimateOperator);
  model.update0();


  likGaussian->update();
  Eigen::VectorXd df = likGaussian->dfgX();

  model.simulateX();



   /***********************************
   Determinstic output results
  ************************************/
  Rcpp::List res;
  res["df"]      = df.tail(df.size()-1);
  res["Q"]      = latentGH->Q_post;
  res["Q_AtA"]  = model.Q;
  res["mupost"] = model.mu;


  Eigen::VectorXd X,Xm;
  Eigen::MatrixXd XtX;
   X.setZero(model.mu.size());
   Xm.setZero(model.mu.size());
   XtX.setZero(model.mu.size(),model.mu.size());
   for(int i =0; i < sim; i++)
   {
     model.simulateX();
     Xm.array() *= (i / (double)(i+1) );
     Xm.array() += model.X.array() / ((double) (i+1));
     X.array() = model.X.array() - Xm.array();

     XtX += X * X.transpose();
   }

    /***********************************
     Stochastic output results
    ************************************/
   res["Xm"] = Xm;
   res["XtX"] = XtX/sim;


  return(res);
}

// [[Rcpp::export]]
Rcpp::List Testingstd1DimGaussianWoperator(Rcpp::List lik_in)
{
  Rcpp::List lik_list         = clone(lik_in);
  Rcpp::List res;
  likelihood* likGaussian;
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->set_estimateOperator(0);

  likGaussian->initFromList(lik_list);
  Eigen::VectorXd X = Rcpp::as<Eigen::VectorXd>(lik_list["X"]);
  likGaussian->X = &X;
  likGaussian->update_param();
  likGaussian->update();
  res["Q_post"] = likGaussian->Q_post;
  res["mu_post"] = likGaussian->mu_post;
  res["df"] = likGaussian->dfgX();

  delete likGaussian;
  return(res);
}

// [[Rcpp::export]]
Rcpp::List Testingstd1DimGaussianWoperator2(Rcpp::List lik_in, Rcpp::List solver_in)
{
  /*
  testing gradient for Gaussian likelihood i.e
  df()
  */
  Rcpp::List lik_list         = clone(lik_in);
  Rcpp::List solver_list      = clone(solver_in);
  Rcpp::List res;
  likelihood* likGaussian;
  solver * R;
  R = new cholesky_solver;
  SparseMatrix<double,0,int> Q;
  Q  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(solver_list["Q"]);

  int solvertype = Rcpp::as<int>(solver_list["type"]);
  int N = Rcpp::as<int>(solver_list["trace.iter"]);
  double tol = Rcpp::as<double>(solver_list["tol"]);
  int max_iter = Rcpp::as<int>(solver_list["solver.max.iter"]);
  int n = Q.rows();
   (*R).init(n,N,max_iter,tol);

  (*R).analyze(Q);
  (*R).compute(Q);
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->set_estimateOperator(0);

  likGaussian->initFromList(lik_list);
    Eigen::VectorXd X = Rcpp::as<Eigen::VectorXd>(lik_list["X"]);
    
  likGaussian->X = &X;
  likGaussian->update_param();
  likGaussian->update();

  res["Q_post"] = likGaussian->Q_post;
  res["mu_post"] = likGaussian->mu_post;
  likGaussian->set_solver(R);
  
  res["df"] = likGaussian->df();
  res["dir"] = likGaussian->direction();
  delete likGaussian;

  return(res);
}



// [[Rcpp::export]]
Rcpp::List test_std1DimGaussianWOperator(Rcpp::List likelihood_in){
  Rcpp::List likelihood_list = clone(likelihood_in);
  std1DimGaussianWOperator likGaussian;
  likGaussian.initFromList(likelihood_in);
  Eigen::VectorXd X = Rcpp::as<Eigen::VectorXd>(likelihood_list["X"]);
  likGaussian.update_param();
  likGaussian.X = &X;
  likGaussian.update();
  likelihood_list["df"] = likGaussian.dfgX();
  likelihood_list["H"] = likGaussian.calcHgX();
  return(likelihood_list);

}


// [[Rcpp::export]]
double logdetWithLU(Eigen::MatrixXd A)
{
  lu_solver SolverObj;

  SolverObj.init(A.cols(), 0, 0, 0);
  SolverObj.compute(A);
  return(SolverObj.logdet());
}
// [[Rcpp::export]]
double traceWithLUSparse(Eigen::MatrixXd A, Eigen::SparseMatrix<double,0,int>  B)
{
  lu_solver SolverObj;

  SolverObj.init(A.cols(), 0, 0, 0);
  SolverObj.compute(A);
  return(SolverObj.trace(B));
}

// [[Rcpp::export]]
double traceWithLU(Eigen::MatrixXd A, Eigen::MatrixXd B)
{
  lu_solver SolverObj;

  SolverObj.init(A.cols(), 0, 0, 0);
  SolverObj.compute(A);
  return(SolverObj.trace(B));
}

// [[Rcpp::export]]
Eigen::VectorXd solveWithLU(Eigen::MatrixXd A, Eigen::VectorXd b)
{
  lu_solver SolverObj;

  SolverObj.init(A.cols(), 0, 0, 0);
  SolverObj.compute(A);
  return(SolverObj.solve(b,b));
}

// [[Rcpp::export]]
Rcpp::List likeOperatorTest(Rcpp::List spdeObj_in){

   Rcpp::List spdeObj      = clone(spdeObj_in);
   Rcpp::List optim_list   = spdeObj["solver"];
   std1DimGaussianWOperator likeObj;

   likeObj.initFromList(spdeObj);
   likeObj.set_estimateOperator(1);
   MaternMatrixOperator Kobj;
   Kobj.initFromList(spdeObj,optim_list );
   likeObj.set_Kmatrix(Kobj);
   Eigen::VectorXd X =  Rcpp::as<Eigen::VectorXd>(spdeObj["X"]);
   likeObj.X = &X;
   likeObj.update();
   likeObj.update_param();
   spdeObj["df"]  =  likeObj.dfgX();
   spdeObj["exp_param"]  =  likeObj.exp_param();
   return(spdeObj);
}

// [[Rcpp::export]]
Rcpp::List GHgrad(Rcpp::List spdeObj_in){
   Rcpp::List spdeObj      = clone(spdeObj_in);
   Rcpp::List latent_list  = spdeObj["latent"];
   Rcpp::List optim_list   = spdeObj["optim"];
   modelGH model;
   stationarymatern latentGH;
   model.X = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
   latentGH.X = &(model.X);
   latentGH.initFromList( latent_list);
   latentGH.initSolver(spdeObj["solver"]);

   // setting up the objects

  GALnoise         noise_class;
  noise_class.initFromList(latent_list["noise"]);
  latentGH.setNoise(&noise_class);

  standard1dim likGaussian;
  likGaussian.initFromList(spdeObj["likelihood"]);

  model.nGibbsIter = Rcpp::as<int>(optim_list["gibbs.iter"]);
  model.prenGibbsIter = Rcpp::as<int>(optim_list["gibbs.preiter"]);
  model.set_likelihood(likGaussian);
  model.set_latent(latentGH);
  model.initSolver(spdeObj["solver"]);
  model.update0();


  Eigen::VectorXd theta = Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]);

  if( spdeObj.containsElementNamed("fixed") )
    model.set_fixed(Rcpp::as<Eigen::VectorXd>(spdeObj["fixed"]));
  if( spdeObj.containsElementNamed("theta") )
    model.vec_to_theta(theta);



  int max_iter  = Rcpp::as<int>(optim_list["max.iter"]);
  int optimtype = Rcpp::as<int>(optim_list["type"]);
  optim Optim(&model, optimtype);

  Optim.stochastic_alpha = Rcpp::as<int>(optim_list["stochastic.alpha"]);
  Optim.alpha_type = Rcpp::as<int>(optim_list["alpha.type"]);
  Optim.line_search_condition = Rcpp::as<int>(optim_list["line.search.condition"]);
  Optim.alpha0 = Rcpp::as<double>(optim_list["alpha0"]);

  double tol= Rcpp::as<double>(optim_list["tol"]);
  int use_chol = Rcpp::as<int>(optim_list["use.chol"]);
  int verbose = Rcpp::as<int>(spdeObj["verbose"]);
  int iterations = 0;
  VectorXd  df;
  MatrixXd Thetas,Xs;
  Thetas.resize(theta.size(),max_iter);
  Xs.resize(model.X.size(),max_iter);

  for(int i= 0; i < max_iter; i++)
  {
    Thetas.col(i) = theta;
    Xs.col(i) = model.X;
    Optim.descent_direction(theta);
    Optim.step();
    theta = Optim.theta_current;
    df = Optim.func->df(theta);
    if(i % 500000 == 500000-1){
      Rcpp::Rcout << "i = " << i << ":\n";
      Rcpp::Rcout << "theta = ";
      for(int k = 0; k < theta.size(); k++)
        Rcpp::Rcout << theta[k] << ", ";
      Rcpp::Rcout << "\n";
      Rcpp::Rcout << "df = ";
      for(int k = 0; k < theta.size(); k++)
        Rcpp::Rcout << df[k] << ", ";
      Rcpp::Rcout << "\n";
    }
    if(0){
      model.show_results();
    }

    iterations++;
    if(df.norm() < tol)
      break;
  }
  spdeObj["theta"] = theta;
  spdeObj["df"] = model.df(theta);
  spdeObj["H"] = model.H;
  spdeObj["X"]  = model.X;
  spdeObj["Xs"]  = Xs;
  spdeObj["V"]  = noise_class.V;
  spdeObj["Thetas"] = Thetas;
  spdeObj["fixed"] = model.get_fixed();
  return(spdeObj);

}


// [[Rcpp::export]]
Rcpp::List test_standard1dimGauss(Rcpp::List likelihood_in){

  Rcpp::List likelihood_list = clone(likelihood_in);
  standard1dim likGaussian;
  likGaussian.initFromList(likelihood_in);

  Eigen::VectorXd X = Rcpp::as<Eigen::VectorXd>(likelihood_list["X"]);
  likGaussian.update_param();
  likGaussian.X = &X;
  likGaussian.update();
  likelihood_list["df"] = likGaussian.dfgX();
  likelihood_list["H"] = likGaussian.calcHgX();
  return(likelihood_list);
}




// [[Rcpp::export]]
Rcpp::List modeldrawX(Rcpp::List spdeObj_in, int sim)
{
  Rcpp::List spdeObj      = clone(spdeObj_in);
  Rcpp::List latent_list  = spdeObj["latent"];
  modelGH model;
  model.X = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
  stationarymatern latentGH;
  latentGH.initFromList( latent_list);

  latentGH.initSolver(spdeObj["solver"]);


  GALnoise         noise_class;
  noise_class.initFromList(latent_list["noise"]);
  if( spdeObj.containsElementNamed("V") )
    noise_class.V = Rcpp::as<Eigen::VectorXd>(spdeObj["V"]);
  latentGH.setNoise(&noise_class);

  standard1dim likGaussian;
  likGaussian.initFromList(spdeObj["likelihood"]);

  model.set_likelihood(likGaussian);
  model.set_latent(latentGH);
  model.initSolver(spdeObj["solver"]);

  model.update0();


 if( spdeObj.containsElementNamed("theta") )
    model.vec_to_theta(Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]));



 model.simulateX();
 spdeObj["Qpost"] = model.Q;
 spdeObj["mupost"] = model.mu;
 Eigen::VectorXd X,Xm;
 Eigen::MatrixXd XtX;
 X.setZero(model.mu.size());
 Xm.setZero(model.mu.size());
 XtX.setZero(model.mu.size(),model.mu.size());
 for(int i =0; i < sim; i++)
 {
   model.simulateX();
   Xm.array() *= (i / (double)(i+1) );
   Xm.array() += model.X.array() / ((double) (i+1));
   X.array() = model.X.array() - Xm.array();

   XtX += X * X.transpose();
 }
 spdeObj["Xm"] = Xm;
 spdeObj["XtX"] = XtX/sim;
 return(spdeObj);
}

// [[Rcpp::export]]
Rcpp::List modelEstdf_lik(Rcpp::List spdeObj_in, int sim,int double_sim = 0)
{
  Rcpp::List spdeObj      = clone(spdeObj_in);
  Rcpp::List latent_list  = spdeObj["latent"];
  modelGH model;
  model.X = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
  stationarymatern latentGH;
  latentGH.initFromList( latent_list);

  latentGH.initSolver(spdeObj["solver"]);


  GALnoise         noise_class;
  noise_class.initFromList(latent_list["noise"]);
  if( spdeObj.containsElementNamed("V") )
    noise_class.V = Rcpp::as<Eigen::VectorXd>(spdeObj["V"]);
  latentGH.setNoise(&noise_class);

  standard1dim likGaussian;
  likGaussian.initFromList(spdeObj["likelihood"]);

  model.set_likelihood(likGaussian);
  model.set_latent(latentGH);
  model.initSolver(spdeObj["solver"]);

  model.update0();


 if( spdeObj.containsElementNamed("theta") )
    model.vec_to_theta(Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]));



 model.simulateX();
 spdeObj["Qpost"] = model.Q;
 spdeObj["mupost"] = model.mu;
 Eigen::VectorXd X, df_vec;
 X.setZero(model.mu.size());
 df_vec.setZero(model.like->npars);
 Eigen::MatrixXd H;
 H.setZero(model.like->npars,model.like->npars);
 for(int i =0; i < sim; i++)
 {
   model.simulateX();
   X.array() = model.X.array();
   df_vec += model.like->dfgX();
   H += model.like->calcHgX();

 }
 if(double_sim == 1){
   H.setZero(model.like->npars,model.like->npars);
  for(int i =0; i < sim; i++){
     model.simulateX();
     H += model.like->calcHgX();
  }
 }
 df_vec.array() /= ((double)sim);
 H /= ((double)sim);
 spdeObj["df"] = df_vec;
 spdeObj["H"]  = H;
 spdeObj["X"] = X;
 return(spdeObj);
}





// [[Rcpp::export]]
Rcpp::List noiseSimV(Eigen::VectorXd theta,Rcpp::List noise_list_in, Eigen::VectorXd E, int sim) {

  Rcpp::List noise_list = clone(noise_list_in);

    GALnoise   noise_class;
    noise_class.initFromList(noise_list);
    noise_class.E = E;
    noise_class.vec_to_theta(theta);
    noise_class.update_param();
    noise_class.update();

    Eigen::VectorXd V, V2;

    noise_class.simulateV();
    V = noise_class.V;
    V2 = noise_class.V;
    V2.array() = noise_class.V.array().pow(2);
    for(int i = 1; i < sim; i++){
       noise_class.simulateV();
       V.array() += noise_class.V.array();
      V2.array() += noise_class.V.array().pow(2);
    }
    V.array() /= sim;
    V2.array() /= sim;
    noise_list = noise_class.toRcppList();
    noise_list["aGIG"] = noise_class.aGIG;
    noise_list["pGIG"] = noise_class.pGIG;
    noise_list["bGIG"] = noise_class.bGIG;
    noise_list["V"]     = V;
    noise_list["V2"]    = V2;
    return(noise_list);
}

// [[Rcpp::export]]
Rcpp::List latentGHgrad( Eigen::VectorXd theta, Rcpp::List latent_list_in, Eigen::VectorXd V)
{
    Rcpp::List latent_list = clone(latent_list_in);


    // init noise
    GALnoise   noise_class;
    noise_class.initFromList(latent_list["noise"]);
    noise_class.updateV();
    noise_class.V = V;

    // init noise
    stationarymatern latentGH;
    Eigen::VectorXd X = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
    latentGH.X = &X;

    latentGH.initFromList( latent_list);
    latentGH.initSolver(latent_list);
    latentGH.setNoise(     &noise_class);
    latentGH.update_param();
    latentGH.update();

    latentGH.vec_to_theta(theta);
    latentGH.update_param();
    latentGH.update();
    latentGH.updateNoise();

    latent_list = latentGH.output_list();
    latent_list["df"] = latentGH.df();
    latent_list["theta"] = latentGH.get_theta();
    latent_list["H"] = latentGH.calcH();
    latent_list["X"] = X;
    return(latent_list);
}

// [[Rcpp::export]]
Rcpp::List noiseGalgrad( Eigen::VectorXd theta,Rcpp::List noise_list_in, Eigen::VectorXd E, Eigen::VectorXd V) {

  Rcpp::List noise_list = clone(noise_list_in);

    GALnoise   noise_class;
    noise_class.initFromList(noise_list);
    noise_class.E = E;
    V.array() +=noise_class.V_adj;
    noise_class.V = V;
    noise_class.vec_to_theta(theta);
    noise_class.updateV();
    Eigen::VectorXd df = noise_class.df();
    noise_class.calcH();
    noise_list = noise_class.toRcppList();
    noise_list["df"] = noise_class.df();
    noise_list["H"] = noise_class.calcH();
    noise_list["f"] = noise_class.f(noise_class.get_theta());
    noise_class.update_param();
    noise_class.update();
    noise_list["aGIG"] = noise_class.aGIG;
    noise_list["pGIG"] = noise_class.pGIG;
    noise_list["bGIG"] = noise_class.bGIG;

    return(noise_list);
}
