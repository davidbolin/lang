#include "gauss_models.h"
#include <time.h>
#include <TimeIt.h>
#include <sys/time.h>
using namespace std;
void gaussianmodel::initfromlist(likelihood * like_in, latent * late_in,Rcpp::List const & init_list)
{

  like = like_in;
  late = late_in;
  d = (*like).d;
  Rcpp::List solver_list = init_list["solver"];

  N = Rcpp::as<int>(solver_list["trace.iter"]);
  int max_iter = Rcpp::as<int>(solver_list["trace.solver.max.iter"]);
  tol = Rcpp::as<double>(solver_list["trace.solver.tol"]);

  use_f = Rcpp::as<int>(solver_list["use.chol"]);
  use_chol = 0;
  if(use_f)
    use_chol = 1;

  n = (*late).n;
  int ntheta = (*like).npars + (*late).npars;


  theta_vec.setZero(ntheta);
  theta_vec << (*like).get_theta(),
               (*late).get_theta();

  g.setZero((*like).npars + (*late).npars);
  p.setZero((*like).npars + (*late).npars);

  df_called = 0;
  f_called = 0;
  df_called = 0;
  f_val = 0.0;

  X.setZero(n*d,1);
  Qh_p.resize(n*d,n*d);
  if(use_chol) {
    Qsolver = new cholesky_solver;
  } else {
    Qsolver = new iterative_solver;
  }
  (*Qsolver).initFromList(n*d,solver_list);
  (*like).set_solver(Qsolver);
  (*late).set_solver(Qsolver);
  (*like).set_matrices(&X);
  (*late).set_matrices(&X);
  //timer_start(1);
  (*late).vec_to_theta(theta_vec.segment((*like).npars,(*late).npars));


  Qh_p = (*late).Q_post + (*like).Q_post;
  if(Qh_p.isCompressed() == 0)
    Qh_p.makeCompressed();

  (*Qsolver).analyze(Qh_p);
  (*Qsolver).compute(Qh_p);
  X = (*Qsolver).solve((*like).mu_post,X);
}

double gaussianmodel::f(VectorXd& theta_vec_in)
{
  double f_val = 0;
  vec_to_theta(theta_vec_in);
  if(f_called == 0){
    if(use_f){

      f_val = (*like).mu_post.dot(X) - (*like).resid - (*like).logdetQ;
      f_val -= (*Qsolver).logdet();
      f_val += (*late).logdetQ;
      f_called = 1;
      f_val = -f_val/2.0;

    } else {
      VectorXd theta_s = theta_vec_in.segment((*like).npars,(*late).npars);
      f_val = (*late).f(theta_s);
    }
  }

  return f_val;
}

VectorXd gaussianmodel::df(VectorXd& theta_vec_in)
{
  vec_to_theta(theta_vec_in);
  if(df_called == 0){
    #ifdef TIMEIT
      timer_start(1)
    #endif
    VectorXd g_late = (*late).df();
    #ifdef TIMEIT
      timer_end(1,"latent df took: ");
      timer_start(2)
    #endif
    VectorXd g_like = (*like).df();
    #ifdef TIMEIT
      timer_end(2,"like df took: ");
    #endif
    g << g_like,
         g_late;

    df_called = 1;
  }
  return g;
}

VectorXd gaussianmodel::direction(VectorXd& theta_vec_in)
{
  vec_to_theta(theta_vec_in);
  df(theta_vec_in);
  VectorXd p_late = (*late).direction();
  VectorXd p_like = (*like).direction();
  p << p_like,
       p_late;

  return p;

}


void gaussianmodel::vec_to_theta(const VectorXd& theta_vec_in)
{
  if((theta_vec-theta_vec_in).norm() > 1e-15){
    df_called = 0;
    f_called = 0;
    theta_vec = theta_vec_in;
    (*late).vec_to_theta(theta_vec.segment((*like).npars,(*late).npars));
    (*like).vec_to_theta(theta_vec.segment(0,(*like).npars));
    (*like).update_param();
    Qh_p = (*like).Q_post + (*late).Q_post;
    (*Qsolver).compute(Qh_p);
    X = (*Qsolver).solve((*like).mu_post,X);
  }
}

VectorXd gaussianmodel::get_theta()
{
	return theta_vec;
}

void gaussianmodel::print_results(string path)
{
  FILE * pFile;
  pFile = fopen((path+"theta.bin").c_str(), "wb");
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}
  if(theta_vec.size()!=fwrite(theta_vec.data(), sizeof(double), theta_vec.size(), pFile)){
    fputs ("Write error theta\n",stderr); exit (1);
  }
  fclose(pFile);

  pFile = fopen((path+"X.bin").c_str(), "wb");
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}
  if(X.size()!=fwrite(X.data(), sizeof(double), X.size(), pFile)){
    fputs ("Write error X\n",stderr); exit (1);
  }
  fclose(pFile);



  VectorXd vars = (*Qsolver).Qinv_diag();

  pFile = fopen((path+"vars.bin").c_str(), "wb");
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}
  if(vars.size()!=fwrite(vars.data(), sizeof(double), vars.size(), pFile)){
    fputs ("Write error vars\n",stderr); exit (1);
  }
  fclose(pFile);
}

VectorXd gaussianmodel::get_X(){
  return X;
}

VectorXd gaussianmodel::get_vars(){
  return (*Qsolver).Qinv_diag();

}

void gaussianmodel::show_results()
{
  (*like).show_results();
  (*late).show_results();
  Rcpp::Rcout << endl;
}


Rcpp::List gaussianmodel::output_list()
{
  Rcpp::List List;
  List["likelihood.parameters"] = (*like).output_list();
  List["latent.parameters"] = (*late).output_list();
  List["X.mean"] = X;
  return(List);
}
