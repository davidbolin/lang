#include "modelGH.h"
#include <math.h>
#include "eigen_add_on.h"

modelGH::modelGH()
{
  random_engine.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  nGibbsIter = 1;
  n = -1;
  iter_H = 0;
  df_updated = 0;
  alpha_vec.resize(0);
  Qsolver = NULL;
}
modelGH::~modelGH()
{
    delete Qsolver;
    delete like;
    delete lat;
  
};

void modelGH::init_Hestimation(Rcpp::List const &init_list)
{
  if(init_list.containsElementNamed("estimateH"))
    estimateH = Rcpp::as<int>(init_list["estimateH"]);
  else
    estimateH = 0;

  if(init_list.containsElementNamed("c0_H"))
    c0_H = Rcpp::as<int>(init_list["c0_H"]);
  else
    c0_H = 10;

  if(init_list.containsElementNamed("c1_H"))
    c1_H = Rcpp::as<int>(init_list["c1_H"]);
  else
    c1_H = 0.1;

  if(init_list.containsElementNamed("c2_H"))
    c2_H = Rcpp::as<int>(init_list["c2_H"]);
  else
    c2_H = 0.6;

  if(init_list.containsElementNamed("alpha_vec"))
    {eigen_vector_from_list(alpha_vec, init_list, "alpha_vec");}
}

void modelGH::set_H0()
{
  if(estimateH ==1 || estimateH ==2 ){
    int n_ = like->npars + lat->npars;
    H.setZero(n_,n_);
    H0.setZero(n_,n_);
    for(int i =0; i <like->npars;i++)
      H0(i,i) = -100.*fmax(like->nd, like->nd);
    for(int i =like->npars; i <like->npars + lat->npars;i++)
      H0(i,i) = -100.*fmax(like->nd, lat->n);

      iter_H = 0;
  }

}

void modelGH::estimate_H()
{
  
  if(estimateH == 1){
    //dfs
    //df_vec
    Eigen::MatrixXd VH;
    int n_ = like->npars + lat->npars;
    VH.setZero(n_, n_);
    for(int i = 0; i < dfs.rows(); i++){
      Eigen::MatrixXd row_diff = (dfs.row(i).transpose() - df_vec);
      VH += row_diff *row_diff.transpose();
    }
    Eigen::VectorXd exp_param;
    exp_param.setZero(lat->npars + like->npars);
    exp_param.tail(lat->npars)    = lat->exp_param();
    exp_param.head(like->npars) = like->exp_param();
    Eigen::MatrixXd De = MatrixXd::Zero(lat->npars + like->npars,lat->npars + like->npars);
    for(int i = 0; i < lat->npars +like->npars;i++)
      De(i,i) += exp_param(i)*exp(-theta_vec(i)) + 1 - exp_param(i);


    VH.array() /=dfs.rows();
    VH =De * VH;
    VH = VH * De;
    for(int i = 0; i < n_; i++){
      if(VH(i,i) == 0)
        VH(i,i) =1;
    }
    iter_H++;
      double v_ = pow(c0_H + c1_H*iter_H,-c2_H);

      if(iter_H>1)
        H = (1-v_) * H  - v_ * VH;
      else
        H = -VH;

  }else if(estimateH == 2)
  {
    Eigen::MatrixXd VH;
    int n_ = like->npars + lat->npars;
    VH.setZero(n_, n_);
    for(int i = 0; i < dfs.rows(); i++){
      Eigen::MatrixXd row_diff = (dfs.row(i).transpose() - df_vec);
      VH += row_diff *row_diff.transpose();
    }
    VH.array() /=dfs.rows();
    for(int i = 0; i < n_; i++){
      if(VH(i,i) == 0)
        VH(i,i) =1;
    }
    iter_H++;
      double v_ = 0.95;

      if(iter_H>1)
        H = (1-v_) * H  - v_ * VH;
      else
        H = -VH;

  }
    
}


void modelGH::initSolver(Rcpp::List const &init_list)
{
  delete Qsolver;
  int use_chol = Rcpp::as<int>(init_list["use.chol"]);
  if(use_chol==1){
    Qsolver = new cholesky_solver;
  } else {
    Qsolver = new iterative_solver;
  }
  int iN =  Rcpp::as<int>(init_list["trace.iter"]);
  int imiter = Rcpp::as<int>(init_list["trace.solver.max.iter"]);
  double itol = Rcpp::as<double>(init_list["trace.solver.tol"]);
  if(n <= 0)
  {
    Rcpp::Rcout << "modelGH::initSolver need n > 0, for setting Qsolver object\n";
    throw("error");
  }
  (*Qsolver).init(n,iN,imiter,itol);
}



void modelGH::set_latent( latent &lat_in){
  lat = &lat_in;
  lat->X = &X;
  n = X.size();
}
void modelGH::simulateX()
{
  if(mu.size()==0)
    mu.resize(lat->mu_post.size());
    
  Q  = lat->Q_post  + like->Q_post;
  mu = lat->mu_post + like->mu_post;
  (*Qsolver).compute(Q);


    
  Eigen::VectorXd  z;
  z.resize(mu.size());


  for(int i =0; i <mu.size(); i++)
    z[i] =  normal(random_engine);
    
  X = (*Qsolver).rMVN(mu, z);
  
    
  lat->updateNoise();
  like->update();
}

void modelGH::simulateNoise()
{
  lat->simulateNoise();
  lat->update();
}

void modelGH::update_param()
{
  like->update_param();
  lat->update_param();
}
void modelGH::update0()
{
  update_param();
  lat->updateNoise();
  lat->update();
  
  Q  = lat->Q_post  + like->Q_post;
  (*Qsolver).init(Q.cols(), 0, 0,0);
  (*Qsolver).analyze(Q);
  theta_vec.resize(lat->npars + like->npars);
  

}
void modelGH::show_results()
{
  lat->show_results();
  like->show_results();
  if(df_var.size() >0 )
  {
    Rcpp::Rcout << "\ndf = " << df_vec.transpose() << "\n";
    Rcpp::Rcout << "var(df) = " << df_var.transpose() << "\n";
  }

}
void modelGH::simulate()
{
  simulateNoise();
  simulateX();

}
Eigen::VectorXd modelGH::kriging(Eigen::SparseMatrix<double,0,int> & A,const int n_sim,const int n_thin)
{
  Eigen::VectorXd E_kriging;
  E_kriging.setZero(A.rows());
  for(int i = 0; i < n_sim; i++)
  {
    for(int ii = 0; ii < n_thin; ii++)
      simulate();


    E_kriging.array() += (A * X).array();
  }
 E_kriging.array() /= n_sim;
  return(E_kriging);
}
double modelGH::f(Eigen::VectorXd& theta_vec)
{

  if( lat->f(theta_vec.tail(lat->npars)) == 0)
    return(like->f(theta_vec.head(like->npars)));

  return(NAN);

}

void modelGH::set_likelihood( likelihood &lik_in)
{ 
  like = &lik_in;
  like->X = &X;
}
  

Eigen::VectorXd  modelGH::direction(Eigen::VectorXd& theta_vec)
{
 df(theta_vec);
 Eigen::VectorXd dir;
 if(0){
 if(H.eigenvalues().real().maxCoeff() < 0    )
    dir = H.ldlt().solve(df_vec);
  else
  {
    dir = df_vec;
    for(int i = 0 ; i < dir.size(); i++)
    {
        if(H(i,i) < 0)
          dir(i) /= H(i,i);
        else
          dir(i) /= -(double)like->nd;
    }
  }
   return(dir);
 }
 estimate_H();
 Eigen::VectorXd temp;
 if(estimateH==0)
  temp = -df_vec;
 else if(estimateH==1){
   if(iter_H < 20)
    temp = H0.ldlt().solve(df_vec);
   else
   {
  Eigen::VectorXd exp_param;
  exp_param.setZero(lat->npars + like->npars);
  exp_param.tail(lat->npars)    = lat->exp_param();
  exp_param.head(like->npars) = like->exp_param();
   Eigen::VectorXd temp2=  df_vec;
  for(int i = 0; i < theta_vec.size(); i++){
    if(exp_param(i) == 1)
      temp2[i] *= exp(-theta_vec(i));
  }
  temp = H.ldlt().solve(temp2);
  for(int i = 0; i < theta_vec.size(); i++){
    if(exp_param(i) == 1)
      temp[i] *= exp(-theta_vec(i));
  }}
 }else if(estimateH == 2)
 {
   if(iter_H < 50)
    temp = H0.ldlt().solve(df_vec);
    else{
      Eigen::SelfAdjointEigenSolver<MatrixXd> es(-H);
      Eigen::MatrixXd V = es.eigenvectors();

      Eigen::VectorXd egenvarden = es.eigenvalues();
      egenvarden = egenvarden.array().pow(0.5);
      temp = V.inverse() * df_vec;
      for(int i = 0; i < H.rows(); i++)
      {
        if(egenvarden(i) < 0)
          temp(i) = 0;
        temp(i) /= -egenvarden(i);
      }
      temp = V * temp;
      
    }
    temp.array() *= 1 - get_fixed().array();
 }


 if(alpha_vec.size() > 0 )
  temp.array() *= alpha_vec.array();

 return(temp);
}


void modelGH::set_fixed(const Eigen::VectorXd& fixed_vec)
{
  like->fixed = fixed_vec.head(like->npars);
  lat->set_fixed(fixed_vec.tail(lat->npars));
}
Eigen::VectorXd modelGH::get_fixed()
{

  Eigen::VectorXd fixed_vec;
  fixed_vec.resize(like->npars+lat->npars);
  fixed_vec.head(like->npars) = like->fixed;
  fixed_vec.tail(lat->npars) = lat->get_fixed();
  return(fixed_vec);
}

Eigen::VectorXd  modelGH::df(Eigen::VectorXd& theta_vec)
{
  this->vec_to_theta(theta_vec);
  if(df_updated == 0){
    Eigen::VectorXd df_temp;
    df_temp.setZero(theta_vec.size());
    df_vec.setZero(theta_vec.size());
    df_var.setZero(theta_vec.size());
    if(estimateH == 0)
      H.setZero(theta_vec.size(),theta_vec.size());
      
     for(int i = 0; i < prenGibbsIter; i++)
      simulate();
      


    dfs.setZero(nGibbsIter, theta_vec.size());
    Xs.resize(nGibbsIter, X.size());
    for(int i = 0; i < nGibbsIter; i++)
    {
      simulate();
      Xs.row(i) = X;
      df_temp.tail(lat->npars) = lat->df();
      df_temp.head(like->npars) = like->dfgX();
      df_vec.array()+= df_temp.array();
      dfs.row(i) = df_temp;
      df_temp.array() = df_temp.array().pow(2);
      df_var.array() += df_temp.array();
      if(estimateH == 0){
        H.topLeftCorner(like->npars, like->npars)  += like->calcHgX();
        H.bottomRightCorner(lat->npars, lat->npars) += lat->calcH();
      }
    }
    df_vec.array() /= (double)nGibbsIter;
    df_var.array() /= (double)nGibbsIter;
    df_updated = 1;
    if(estimateH == 0)
      H.array() /= (double)nGibbsIter;

  df_var.array() -= df_vec.array().pow(2);
  }
  return(df_vec);
}
VectorXd modelGH::get_theta()
{
  theta_vec.resize(like->npars+lat->npars);
  theta_vec.head(like->npars) = like->get_theta();
  theta_vec.tail(lat->npars)  = lat->get_theta();
  return theta_vec;
}
void modelGH::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  int equal = 1;
  if(theta_vec.size() == theta_vec_in.size()){
    for(int i = 0; i < theta_vec_in.size(); i++)
    {
  
      if(theta_vec[i] != theta_vec_in[i])
      {
        equal = 0;
        break;
      }
    }
  }
  else
  {
    equal = 0;
  }
  if(equal == 0){
    lat->vec_to_theta(theta_vec_in.tail(lat->npars));
    
    like->vec_to_theta(theta_vec_in.head(like->npars));
    
    df_updated = 0;
    update_param();
    lat->update();
    like->update();
    
    theta_vec.resize(theta_vec_in.size());
    theta_vec = theta_vec_in;
  }

}

Rcpp::List modelGH::output_list()
{
  Rcpp::List List;
  List["likelihood"] = (*like).output_list();
  List["latent"]     = (*lat).output_list();
  return(List);
}


void modelGH_mult::set_nreplicat(int N)
{
  X.resize(N);
  Q.resize(N);
  mu.resize(N);
  lik_vec.resize(N);
  lat_vec.resize(N);
}
void modelGH_mult::set_latent(int     i, latent_ptr lat_in)
{
  lat_vec[i] = lat_in;
}
void modelGH_mult::set_likelihood(int i, likelihood_ptr lik_in)
{
  lik_vec[i] =  lik_in;
}

