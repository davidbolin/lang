#include "transformationFunctions.h"
#include <iostream>
#include <stdio.h>

Eigen::VectorXd g_no_transform(Eigen::MatrixXd   &  B , Eigen::VectorXd & beta){return(B*beta);};
Eigen::MatrixXd dg_no_transform(Eigen::MatrixXd  & B  , Eigen::VectorXd & beta){return(B);};
Eigen::VectorXd g_exp_transform(Eigen::MatrixXd & B, Eigen::VectorXd & beta)
{
  Eigen::VectorXd temp =  B*beta; 
  temp.array() = temp.array().exp();
  return(temp);
}


Eigen::MatrixXd dg_exp_transform(Eigen::MatrixXd & B, Eigen::VectorXd & beta) 
{
  
  Eigen::VectorXd G = g_exp_transform(B, beta); 
  Eigen::MatrixXd temp = G.asDiagonal() * B ;
  return(temp);
}