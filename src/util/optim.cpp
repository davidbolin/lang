//
//  optim.cpp
//  mixture_library
//
//  Created by Jonas Wallin on 30/05/14.
//  Copyright (c) 2014 Jonas Wallin. All rights reserved.
//

#include "optim.h"

#include <algorithm>    // std::max
using namespace Eigen;


optim::optim(optim_func* f, int type_)
{
	iter_count = 0;
	type = type_;
	func = f;
	c1 = 0.0001;
	c2 = 0.1;
	line_search_iter = 100;
	line_search_condition = 0;
	tau_alpha = 0.5;
	line_search_interp = 1;
	alpha_type = 1;
	alpha0 = 1;
	alpha = 1;
	CG_type = -1;
	stochastic_alpha = 0;
  stochastic_c0 = 1;
  stochastic_c1 = 1;
  stochastic_c2 = 0.8;
	if(type == 0){ // gradient descent


	}else if (type == 1) // preconditoned gradient descent
	{

	}else if (type==2) // CG
	{

		CG_type = 0;
		direction_old.resize(0);
	}else if (type ==3)
	{
		df_old.resize(0);
		theta_old.resize(0);
	}

	QN_matrix.resize(0,0);
	alphas.resize(2);
	fs.resize(2);
}

void optim::descent_direction(VectorXd& theta)
{
	if(type== 0)
		gradient_descent(theta);
	if(type== 2)
		CG(theta);
	if(type== 1)
		precond_gradient_descent(theta);
	if(type==3)
		QuasiNewton(theta);
}

VectorXd& optim::step()
{
	select_alpha();
	linesearch();

	iter_count++;
	return theta_current;
}

void optim::gradient_descent(VectorXd& theta)
{
	theta_current = theta;
	df = (*func).df(theta);
	direction = -df;
	//direction /= direction.norm();
}
void optim::precond_gradient_descent(VectorXd& theta)
{
	theta_current = theta;
	//df = (*func).df(theta);
	direction = (*func).direction(theta);
	df.resize(0);
}


void optim::CG_beta()
{
	double beta;
	if(CG_type == 0)
	{
		double norm_old = df_old.dot(-direction0_old);
		beta     = df.dot(-direction0);
		beta /= norm_old;


	}

	// in case one can not evalute the desecent direction
	// CG is typically unstable however seems like one can cut it off
	// if beta is to large and still get convergence
	// at least initially faster then gradient descent
	if(line_search_condition == -1)
	{
		if(beta > 1.5 || beta < 0)
			beta  = 0;
	}

	direction = direction0 + beta * direction_old;
}


void optim::QuasiNewton(VectorXd& theta)
{
	theta_current = theta;
	df = (*func).df(theta);
	if(df_old.size() == 0)
	{
		QN_matrix.setIdentity(theta.size(), theta.size() );

	}else{

		Eigen::VectorXd x = theta - theta_old;

		Eigen::VectorXd y = df - df_old;
		double div_xTy = 1./x.dot(y);
		y *=  div_xTy;

		Eigen::MatrixXd xyT = y*x.transpose();
		Eigen::MatrixXd I_xy;

		I_xy.setIdentity(theta.size(), theta.size() );
		I_xy -= xyT;

		QN_matrix = I_xy.transpose() * QN_matrix * I_xy;
		x *= sqrt(div_xTy);
		Eigen::MatrixXd xxT = x*x.transpose();
		QN_matrix += xxT;
		Eigen::EigenSolver<MatrixXd> es(QN_matrix);

		double eig_min =   es.eigenvalues().col(0).real().minCoeff();
		if (eig_min < 1e-15)
			QN_matrix.setIdentity(theta.size(), theta.size() );

	}

	direction = - QN_matrix * df;
	df_old = df;
	theta_old = theta;
}


void optim::CG(VectorXd& theta)
{

	df = (*func).df(theta);
	direction0 = (*func).direction(theta);
	//direction0 = -df;
	theta_current = theta;
	if(direction_old.rows() == 0){
		direction = direction0;
		direction_old = direction;
	}else{

		CG_beta();
		direction_old = direction;
	}

	df_old = df;
	direction0_old = direction0;
}



void optim::select_alpha()
{
	if(alpha_type == -1){
		// do nothing
	}
	if(alpha_type == 1){
		if( iter_count == 0){
			alpha = 1;
			alpha /= df.norm();
			if(alpha > 1)
				alpha = 1;
		}
		else
			alpha = 1.;
	}else if(alpha_type == 2)
	{
		alpha = alpha0;
	}else if(alpha_type == 3)
	{
		if(iter_count==0)
		{
			alpha = alpha0;

		}else{
			VectorXd dtheta = theta_current - theta_old;
			VectorXd dg     = - direction + direction_old;
			alpha = dtheta.dot(dtheta);
			alpha /= dg.dot(dtheta);
		}
		direction_old = direction;
		theta_old = theta_current;


	}

	if(stochastic_alpha > 0){
			alpha /= pow(stochastic_c0 + stochastic_c1 * iter_count, stochastic_c2);
  }
}
void optim::linesearch()
{
	if( line_search_condition == 0)
		linesearch_Armijo();
	else if( line_search_condition == -1)
		linesearch_no();
}

void optim::linesearch_no()
{
	double alpha_temp =  alpha;
	VectorXd theta_star;
	VectorXd dtheta  = alpha * direction;
	theta_star = theta_current + dtheta;
	double f_star =(*func).f(theta_star);
	while(std::isnan(f_star) || std::isinf(f_star))
	{
		dtheta = (*func).valid_step(theta_current, dtheta);
		theta_star = theta_current + dtheta;
		f_star =(*func).f(theta_star);
	}
	theta_current =  theta_star;
}
void optim::linesearch_Armijo()
{
	f = (*func).f(theta_current);
	if(df.rows() == 0)
		df = (*func).df(theta_current);
	gtd = df.dot(direction.transpose());
	double c1_gtd = c1 * gtd;


	VectorXd theta_star;
	double f_star;
	for(linesearch_i = 0; linesearch_i < line_search_iter; linesearch_i++)
	{
		VectorXd dtheta  = alpha * direction;
		theta_star = theta_current + dtheta;
		f_star =(*func).f(theta_star);
		while(std::isnan(f_star) || std::isinf(f_star))
		{
			dtheta = (*func).valid_step(theta_current, dtheta);
			theta_star = theta_current + dtheta;
			f_star =(*func).f(theta_star);
		}

		if(f_star - f - alpha * c1_gtd < 0)
			break;

		linesearch_interpolation( f_star);


	}

	if (linesearch_i == line_search_iter){
		alpha = -1;
	}
	theta_current =  theta_star;
	f = f_star;
}

void optim::linesearch_interpolation(double f_star)
{


	if(line_search_interp == 0)
		alpha *= tau_alpha;
	else if (line_search_interp == 1)
	{
		Poly_interpolation(f_star);
	}

}

void optim::Poly_interpolation(double f_star)
{
	double alpha_star;
	if (linesearch_i == 0)
	{
		alphas(1) = alpha;
		fs(1) = f_star;
		double c2 = f_star - f - alpha * gtd;
		c2 /= pow(alpha,2);
		double c1 = gtd;
		alpha_star =  - c1/(2*c2);


	}else
	{
		alphas(0) = alphas(1);
		fs(0)     = fs(1);
		fs(1)     = f_star;
		alphas(1) = alpha;
		MatrixXd A(2,2);
		A << pow(alphas(0), 2)  , - pow(alphas(1), 2),
		- pow(alphas(0), 3),   pow(alphas(1), 3);
		A /= pow(alphas(0), 2) * pow(alphas(1), 2) * (alphas(1)- alphas(0));
		VectorXd b(2);
		b(0) = fs(1) - f - alphas(1) * gtd;
		b(1) = fs(0) - f - alphas(0) * gtd;
		VectorXd ab = A*b;
		double c = pow(ab(1),2) - 3* ab(0)*gtd;
		if(c> 0){
			alpha_star = -ab(1) + pow(c,0.5);
			alpha_star /= 3 * ab(0);
		}
		else
			alpha_star = 2*alpha;

	}

	if(alpha_star > 0.75 * alpha)
		alpha_star = 0.75 *alpha;
	else if (alpha_star < 0.1 * alpha)
		alpha_star = 0.1 * alpha;
	alpha = alpha_star;
}
