#include "likelihoodGaussian.h"
using namespace std;

void nestedspde::initFromList(Rcpp::List const & init_list)
{
  standard1dim::initFromList(init_list);

  thetavec = Rcpp::as<Eigen::VectorXd>(init_list["vec"]);

  n_vec = thetavec.size();

  Rcpp::List Bveclist = init_list["B.vec"];

  Bvec = new SparseMatrix<double,0,int>[n_vec];

  for(int i = 0; i <n_vec; i++){
    int j = i+1;
    stringstream ss;
    ss << j;
    string str = ss.str();

    Bvec[i] = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(Bveclist[i]);
  }

    npars += n_vec;

  n = Bvec[0].rows();
  g.setZero(npars);
  p.setZero(npars);

  dthetavec.setZero(n_vec);
  d2thetavec.setZero(n_vec,n_vec);
  dbetadthetavec.setZero(n_beta,n_vec);
  dsigmadthetavec.setZero(1,n_vec);

  I.resize(n,n);
  for(int i =0;i<n;i++){
    I.insert(i,i) = 1.0;
  }

  Hvec = I;
  for(int i=0;i<n_vec;i++){
    Hvec += thetavec(i)*Bvec[i];
  }

  Q_post = Hvec.transpose()*AtA*Hvec/sigma2;

  SparseMatrix<double,0,int> QA = Hvec.transpose()*A.transpose()/sigma2;

  if(n_beta > 0){
    mu_post = QA*(Y-B*beta);
    resid = (Y-B*beta).cwiseProduct(Y-B*beta).sum()/sigma2;
  }else{
    mu_post = QA*Y;
    resid = Y.cwiseProduct(Y).sum()/sigma2;
  }
  logdetQ = nd*log(sigma2);

}

void  nestedspde::init(string path){}

void  nestedspde::show_results()
{
  standard1dim::show_results();
  Rcpp::Rcout << ", vec = " << thetavec.transpose();
}

VectorXd  nestedspde::df()
{
  double d2dt, trest;
  SparseMatrix<double,0,int> tmp;
  VectorXd diffk = (Y-A*Hvec*(*X)-B*beta);

  dbeta = B.transpose()*diffk/sigma2;
  d2beta = -B.transpose()*B/sigma2;

  SparseMatrix<double,0,int> tmp1 = Hvec.transpose()*AtA*Hvec;
  trest  = (*Qsolver).trace(tmp1);

  d2dt = (diffk.squaredNorm() + trest)/(2.0*sigma2);
  dsigma= -nd/2.0 + d2dt;
  d2sigma = -d2dt;
  dsigmadbeta = -dbeta;

  VectorXd abx;
  for(int i= 0; i <n_vec; i++){
    abx = A*Bvec[i]*(*X);

    tmp = Bvec[i].transpose()*AtA*Hvec + Hvec.transpose()*AtA*Bvec[i];
    trest = (*Qsolver).trace(tmp);

    dthetavec(i) = -((*X).cwiseProduct(tmp*(*X)).sum() + trest)/(2.0*sigma2);
    dthetavec(i) += (Y-B*beta).cwiseProduct(abx).sum()/sigma2;

    dbetadthetavec.row(i) = - abx.transpose()*B/sigma2;
    dsigmadthetavec(i) = - dthetavec(i);
    for(int j=i; j< n_vec; j++){
      tmp = Bvec[i].transpose()*AtA*Bvec[j] + Bvec[j].transpose()*AtA*Bvec[i];
      trest = (*Qsolver).trace(tmp);

      d2thetavec(i,j) = -((*X).cwiseProduct(tmp*(*X)).sum() + trest)/(2.0*sigma2);

      d2thetavec(j,i) = d2thetavec(i,j);
    }
  }

  g << dbeta,
       dsigma,
       dthetavec;
  g = -g;

  return g;
}

VectorXd  nestedspde::direction()
{
  H.setZero(npars,npars);
  H << d2beta, dsigmadbeta, dbetadthetavec,
       dsigmadbeta, d2sigma, dsigmadthetavec,
       dbetadthetavec.transpose(), dsigmadthetavec.transpose(), d2thetavec;
  VectorXd e = H.eigenvalues().real();
  //Rcpp::Rcout << "H" << endl;
  //Rcpp::Rcout << H << endl;
  if(e.maxCoeff() > 0){
    VectorXd tmp = H.diagonal();
    H = tmp.asDiagonal();
    e = H.eigenvalues().real();
    if(e.maxCoeff() > 0){
      cout << "Warning diag(Hlike) not neg def" << endl;
    }
  }
  p = H.ldlt().solve(g);
  return p;
}

void  nestedspde::vec_to_theta(const VectorXd& theta_vec_in)
{
  beta = theta_vec_in.segment(0,n_beta);
  sigma2 = exp(theta_vec_in(n_beta));
  thetavec = theta_vec_in.segment(n_beta+1,n_vec);
}


void nestedspde::update_param()
{
  Hvec = I;
  for(int i=0;i<n_vec;i++){
    Hvec += thetavec(i)*Bvec[i];
  }

  Q_post = Hvec.transpose()*AtA*Hvec/sigma2;

  SparseMatrix<double,0,int> QA = Hvec.transpose()*A.transpose()/sigma2;

  if(n_beta > 0){
    mu_post = QA*(Y-B*beta);
    resid = (Y-B*beta).cwiseProduct(Y-B*beta).sum()/sigma2;
  }else{
    mu_post = QA*Y;
    resid = Y.cwiseProduct(Y).sum()/sigma2;
  }
  logdetQ = nd*log(sigma2);
}


VectorXd nestedspde::get_theta()
{
  VectorXd theta;
  theta.setZero(npars);
  theta << beta,
           log(sigma2),
           thetavec;
  return theta;
}



