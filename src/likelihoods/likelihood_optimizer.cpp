#include "likelihood.h"

double likelihood_optim_function::f(Eigen::VectorXd&  theta)
{
  return lik->f(theta);
  
}

void likelihood_optim_function::set_likelihood( likelihood &lik_in)
{ 
  lik = &lik_in;
  lik->X = &X;
}

Eigen::VectorXd likelihood_optim_function::df(Eigen::VectorXd& theta_vec)
{
  this->vec_to_theta(theta_vec);
  return lik->dfgX();
}
void likelihood_optim_function::vec_to_theta(Eigen::VectorXd& theta_vec)
{
  theta = theta_vec;
  lik->vec_to_theta(theta_vec);
  lik->update();
  lik->update_param();
}
VectorXd likelihood_optim_function::get_theta()
{
  return lik->get_theta();
}


Eigen::VectorXd likelihood_optim_function::direction(Eigen::VectorXd&  theta_vec)
{
  Eigen::VectorXd dir = -this->df(theta_vec);
  dir.array() /=  lik->nd;
  dir.array() *= 1 - lik->fixed.array(); 
  return dir;
}

void likelihood_optim_function::show_results()
{
  lik->show_results();
    Rcpp::Rcout << "\n df = " << this->df(theta).transpose() << "\n";
}