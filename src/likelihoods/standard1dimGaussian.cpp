#include "likelihoodGaussian.h"
using namespace std;
#include "TimeIt.h"


void standard1dim::initFromList(Rcpp::List const & init_list)
{

 std::vector<std::string> check_names =  {"A", "sigma2"};
  check_Rcpplist(init_list, check_names, "standard1dim::initFromList");

  A = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["A"]);
  Y = Rcpp::as<Eigen::VectorXd>(init_list["Y"]);
  vector<std::string> names = Rcpp::as<vector<string> >(init_list.names());
  if (std::find(names.begin(), names.end(), "B") != names.end()){
    B        = Rcpp::as<Eigen::MatrixXd>(init_list["B"]);
    n_beta   = B.cols();
    beta     = Rcpp::as<Eigen::VectorXd>(init_list["beta"]);
    dbeta.setZero(n_beta);
    d2beta.setZero(n_beta,n_beta);
    dsigmadbeta.setZero(1,n_beta);
  } else {
    B.resize(0,0);
    n_beta = 0;
    beta.resize(0);
  }
  AtA = A.transpose()*A;
  AtY = A.transpose()*Y;
  AtB = A.transpose()*B;
  sigma2   = Rcpp::as<double>(init_list["sigma2"]);

  d = 1;
  nd = Y.rows();
  npars = n_beta+1;
  g.setZero(npars);
  p.setZero(npars);
  logdetQ = 0.0;
  resid   = 0.0;

  Q_post = AtA/sigma2;
  if(n_beta > 0){
    mu_post = (AtY-AtB*beta)/sigma2;
    resid = (Y-B*beta).cwiseProduct(Y-B*beta).sum()/sigma2;
  }else{
    mu_post = AtY/sigma2;
    resid = Y.cwiseProduct(Y).sum()/sigma2;
  }
  logdetQ = nd * log(sigma2);


  if( init_list.containsElementNamed("fixed") )
  {
    fixed =  Rcpp::as<Eigen::VectorXd>(init_list["fixed"]);
  }
  else
  {
    fixed = VectorXd::Zero(npars);
  }
}

void standard1dim::add_mean(Eigen::VectorXd& X)
{
  if(n_beta>0)
    X.array() += (B*beta).array();
}

void standard1dim::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  if(n_beta>0)
    beta   = theta_vec_in.segment(0,n_beta);
  sigma2 = exp(theta_vec_in(n_beta));
}

void standard1dim::update_param()
{
  Q_post = AtA/sigma2;
  if(n_beta > 0){
    mu_post = (AtY-AtB*beta)/sigma2;
    d2beta   = -B.transpose()*B/sigma2;
  }else{
    mu_post = AtY/sigma2;
  }

}
void standard1dim::update()
{
   if(n_beta > 0){
      diffk = (Y-A*(*X)- B*beta);
      dbeta    = B.transpose()*diffk/sigma2;
   } else {
     diffk = (Y-A*(*X));
     dbeta.resize(0);
   }
  d2dt = diffk.transpose()*diffk;
  d2dt /= (2.0 * sigma2);
}

Eigen::VectorXd standard1dim::direction()
{
  H.setZero(npars,npars);
  if(n_beta>0){
    H << d2beta, dsigmadbeta,
         dsigmadbeta.transpose(), d2sigma;
  } else {
    H << d2sigma;
  }
  Eigen::VectorXd e = H.eigenvalues().real();

  if(e.maxCoeff() > 0){
    Eigen::VectorXd tmp;
    if(n_beta>0){
      dsigmadbeta.setZero(dsigmadbeta.rows(),dsigmadbeta.cols());
      tmp = d2beta.diagonal();
      d2beta = tmp.asDiagonal();
      H << d2beta, dsigmadbeta,
           dsigmadbeta,d2sigma;
    }
    e = H.eigenvalues().real();
    if(e.maxCoeff() > 0){
      tmp = H.diagonal();
      H = tmp.asDiagonal();
      e = H.eigenvalues().real();
      if(e.maxCoeff() > 0){
        std::cout << "Warning diag(H) not neg def" << std::endl;
        show_results();
      }
    }
  }
  p = H.ldlt().solve(g);
  p.array() *= 1.0 -  fixed.array();

  return p;
}
Eigen::MatrixXd standard1dim::calcHgX()
{
  H.setZero(npars,npars);
  d2sigma = -d2dt;
  dsigmadbeta = -dbeta;
  H << d2beta, dsigmadbeta,
       dsigmadbeta.transpose(),d2sigma;
  for(int i =0; i < H.cols(); i++){
    if(fixed[i] == 1)
    {
      H.col(i) *= 0;
      H.row(i) *= 0;
    }
    H(i,i) -= fixed[i];
  }
  return(H);
}

Eigen::VectorXd standard1dim::dfgX()
{
  dsigma = -nd/2.0 + d2dt;
  g << dbeta, dsigma;
  g.array() *= 1.0 -  fixed.array();
  return(-g);

}
Eigen::VectorXd standard1dim::df()
{
  double  trest;
  if(n_beta > 0){
    diffk = (Y-A*(*X)- B*beta);
    dbeta    = B.transpose()*diffk/sigma2;
    d2beta   = -B.transpose()*B/sigma2;
    dsigmadbeta = -dbeta;
  } else {
    diffk = (Y-A*(*X));
  }
  trest  = (*Qsolver).trace(AtA);
  d2dt   = (diffk.transpose()*diffk + trest)/(2.0*sigma2);
  dsigma = -nd/2.0 + d2dt;
  d2sigma = -d2dt;

  if(n_beta>0){
    g << dbeta,
         dsigma;
  } else {
    g << dsigma;
  }
  g = -g;
  g.array() *= 1.0 -  fixed.array();
  return g;
}

void standard1dim::show_results()
{
  
  Rcpp::Rcout << "LIKELIHOOD:\n";
  if(n_beta>0)
    Rcpp::Rcout << "  beta   = " << beta.transpose();
  Rcpp::Rcout << ", sigma2 = " << sigma2;
  Rcpp::Rcout << "**********\n";
  Rcpp::Rcout << "Operator\n";
  Rcpp::Rcout << "Operator\n";
  
}



Rcpp::List standard1dim::output_list()
{
  Rcpp::List List;
  if(n_beta > 0 ){
    List["beta"] = beta;
    List["B"]  = B;
  }
  List["sigma2"] = sigma2;
  List["A"] = A;
  List["Y"]  = Y;
  List["fixed"] = fixed;


  return(List);
}

VectorXd standard1dim::get_theta()
{
  VectorXd theta;
  theta.setZero(npars);
  theta << beta,
           log(sigma2);
  return theta;
}

Eigen::VectorXd standard1dim::exp_param()
{
  Eigen::VectorXd param_exp;
  param_exp.setZero(npars);
  param_exp(npars-1) = 1;
  return(param_exp);

}

