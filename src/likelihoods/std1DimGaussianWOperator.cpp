#include "likelihoodGaussian.h"
using namespace std;
#include "TimeIt.h"
#include "eigen_add_on.h"


void std1DimGaussianWOperator::initFromList(Rcpp::List const & init_list)
{
  std::vector<std::string> check_names =  {"Y","A", "sigma"};
  check_Rcpplist(init_list, check_names, "std1DimGaussianWOperator::initFromList");
  A = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["A"]);
  
  mu_post.resize(A.cols());
  eigen_vector_from_list(Y, init_list, "Y");
  if (init_list.containsElementNamed("B")){
    eigen_matrix_from_list(B, init_list , "B"); 
    n_beta   = B.cols();
    eigen_vector_from_list(beta, init_list, "beta");
  }else
  {
    B.resize(0,0);
    n_beta = 0;
    beta.resize(0);
  }
  if (init_list.containsElementNamed("B.sigma")){
  eigen_matrix_from_list(Bsigma, init_list , "B.sigma"); 
  }else
    Bsigma.setOnes(Y.size(),1);
  eigen_vector_from_list(sigma, init_list, "sigma");
  n_sigma = Bsigma.cols();
  sigma_vec   = &std1DimGaussianWOperator::sigma_exp_transform;
  dsigma_vec  = &std1DimGaussianWOperator::dsigma_exp_transform;
  d = 1;
  nd = Y.rows();
  dsigma.setZero(n_sigma);
  
  
  dbeta.setZero(n_beta);
  d2beta.setZero(n_beta,n_beta);

  if(n_beta > 0)
    dsigmadbeta.setZero(n_sigma,n_beta);
  else
    dsigmadbeta.setZero(0,0);
    
  npars = n_beta+n_sigma + estimateOperator;
  ;
  g.setZero(npars);
  p.setZero(npars);
  logdetQ = 0.0;
  resid   = 0.0;
  update_param();
  

  
  if( init_list.containsElementNamed("fixed") ){
    eigen_vector_from_list(fixed, init_list, "fixed"); //Rcpp::as<Eigen::VectorXd>(init_list["fixed"]);
  }else
    fixed = VectorXd::Zero(npars);
    
    // for calculation of df, direction
    
  dQ_e = new SparseMatrix<double,0,int>[n_sigma];
  
  int ii=0;
  for(int i = 0;i<n_sigma;i++){
    dQ_e[ii].resize(nd , nd);
    for(int j=0;j<nd;j++)
      dQ_e[ii].insert(j,j) = -2.*Bsigma(j,i);
    
    ii++;
  }
  
}
void std1DimGaussianWOperator::set_estimateOperator(int estOin)
{ 
  if(Kobj)
    npars -= Kobj->npars * (estimateOperator - estOin); 
  estimateOperator = estOin; 
  if(Kobj)
    update_Kparam();
  
  
  
};

void std1DimGaussianWOperator::vec_to_theta(const Eigen::VectorXd& theta_vec_in)
{
  beta   = theta_vec_in.segment(0,n_beta);
  sigma  = theta_vec_in.segment(n_beta,n_sigma);
  if(estimateOperator == 1)
    Kobj->vec_to_theta(theta_vec_in.tail(Kobj->npars));
}

void std1DimGaussianWOperator::update_param()
{
  Eigen::SparseMatrix<double,0,int> QA;
  
  sigma2_vec.setOnes(Bsigma.rows(),1);
  sigma2_vec.array() /= (this->*sigma_vec)().array();
  sigma2_vec.array() = sigma2_vec.array().pow(2);
  QA      = A.transpose() * sigma2_vec.asDiagonal();
  Q_post  = QA * A;


  if(n_beta > 0){
    d2beta   = -B.transpose()*sigma2_vec.asDiagonal()*B;
    mu_post = QA*(Y-B*beta);
  }
  else
    mu_post = QA*Y;
    
  
  if(Kobj)
    Kobj->update_param();
}
void std1DimGaussianWOperator::update()
{
  
  if(n_beta > 0){
    diffk = (Y-A*(*X)- B*beta);
    dbeta    = B.transpose()*sigma2_vec.asDiagonal()*diffk;
  }
  else
  {
    diffk = (Y-A*(*X));
    dbeta.resize(0);
  }
 
  d2dt = diffk.transpose()*diffk;
  
  

  if(Kobj)
    Kobj->update();
}

double std1DimGaussianWOperator::trace_Qeps(int i)
{
  SparseMatrix<double,0,int> AdQ_eQ_e = A.transpose() * dQ_e[i]; //* sigma2_vec.asDiagonal() * A; 
  AdQ_eQ_e = AdQ_eQ_e * sigma2_vec.asDiagonal();
  SparseMatrix<double,0,int>  AdQ_eQ_eAt = AdQ_eQ_e * A;
  return (*Qsolver).trace(AdQ_eQ_eAt);
}
double std1DimGaussianWOperator::trace_Qeps2(int i, int j)
{
  SparseMatrix<double,0,int> AdQ_eQ_e = A.transpose() * dQ_e[i]; //* sigma2_vec.asDiagonal() * A; 
  AdQ_eQ_e = AdQ_eQ_e * dQ_e[j];
  AdQ_eQ_e = AdQ_eQ_e * sigma2_vec.asDiagonal();
  SparseMatrix<double,0,int>  AdQ_eQ_eAt = AdQ_eQ_e * A;
  return (*Qsolver).trace(AdQ_eQ_eAt);
}
Eigen::VectorXd std1DimGaussianWOperator::df()
{
  update_param();
  update();

  
  Eigen::VectorXd diffk_s, temp;
  
  // Caclulating - d/dQ_e E[ (Ax)^T Q_e Ax ] =  tr(d/dQ_e Q_e   A E[xx^T] A^T ) 
  
  
  
  //sigma2_vec.array();
  // gradient d/d\sigma \log(\sigma)
  //det_.setOnes(Bsigma.rows());
  //det_.array() /=  (this->*sigma_vec)().array();
    
  // (Y-AX -B\beta)^T * (Y-AX -B\beta)
  diffk_s.resize(Bsigma.rows());
  
  
  
  // dsigmadbeta
  if(n_beta>0){
    temp = diffk;
    temp.array() *= sigma2_vec.array();
    dsigmadbeta = Bsigma.transpose() * temp.asDiagonal() * B;
    dsigmadbeta.array() *= -2.; 
  }
  // dsigmadbeta done
  
  diffk_s.array() = diffk.array().pow(2);
  diffk_s.array() *= sigma2_vec.array();
  
  dsigma.resize(n_sigma);
  dsigma = (Bsigma.transpose()* diffk_s);
  d2sigma.setZero(n_sigma,n_sigma);
  d2sigma = -2. * Bsigma.transpose()* diffk_s.asDiagonal() * Bsigma;
  for(int i = 0; i < n_sigma; i++){
    dsigma[i] -=  trace_Qeps(i)/2.;
    dsigma[i] -=  Bsigma.col(i).sum();
  }
  
  if(n_beta>0){
    g << dbeta,
         dsigma;
  } else {
    g << dsigma;
  }
  
  if(estimateOperator == 1)
    throw("df is not implmented for estiamte operator\n"); 
    
  
  
  
  g.array() *= 1.0 -  fixed.array();
  
  return(-g);
}



Eigen::VectorXd std1DimGaussianWOperator::direction()
{
  
  for(int i = 0; i < n_sigma;i++)
  {
    
    d2sigma(i,i) -= trace_Qeps2(i,i)/2.;
    for(int j = 0 ; j <i; j++)
    {
      double c_eps = -trace_Qeps2(i,j)/2.;
      d2sigma(i,j)  += c_eps;
      d2sigma(j,i)  += c_eps; 
    }
  }
  
  H.setZero(npars,npars);
  
  if(n_beta>0){
    H << d2beta, dsigmadbeta.transpose(),
         dsigmadbeta, d2sigma;
  } else {
    H << d2sigma;
  }
  
  
  Eigen::VectorXd e = H.eigenvalues().real();

  if(e.maxCoeff() > 0){
    Eigen::VectorXd tmp, tmp2;
    if(n_beta>0){
      dsigmadbeta.setZero(dsigmadbeta.rows(),dsigmadbeta.cols());
      tmp = d2beta.diagonal();
      d2beta = tmp.asDiagonal();
      tmp2 = d2sigma.diagonal();
      d2sigma = tmp2.asDiagonal();
      H << d2beta, dsigmadbeta.transpose(),
           dsigmadbeta,d2sigma;
    }
  
  e = H.eigenvalues().real();
      if(e.maxCoeff() > 0){
        tmp = H.diagonal();
        H = tmp.asDiagonal();
        e = H.eigenvalues().real();
        if(e.maxCoeff() > 0){
          std::cout << "Warning diag(H) not neg def" << std::endl;
          show_results();
        }
      }
    }
    p = H.ldlt().solve(g);
    p.array() *= 1.0 -  fixed.array();

  return p;
}
Eigen::MatrixXd std1DimGaussianWOperator::calcHgX()
{
  H.setIdentity(npars,npars);
  H.array() *= -1;
  return(H);
}
Eigen::VectorXd std1DimGaussianWOperator::dfgX()
{
  g.resize(npars);
  Eigen::VectorXd diffk_s, temp;
  

    det_.setOnes(Bsigma.rows());
    det_.array() /=  (this->*sigma_vec)().array();
    diffk_s.resize(Bsigma.rows(),1);
    diffk_s.array() = diffk.array().pow(2);
    diffk_s.array() *= sigma2_vec.array();
    diffk_s.array()  /= (this->*sigma_vec)().array(); 
    temp =  diffk_s - det_;
    dsigma = (this->*dsigma_vec)(temp).transpose();
 
  if(estimateOperator == 1){ 
    Eigen::VectorXd  df_Ki;
    df_Ki.setZero(Kobj->npars);
    for(int i = 0; i < Kobj->npars; i++)
    {
      diffk_s.array() = diffk.array()* sigma2_vec.array();
      Eigen::VectorXd dK_X = Kobj->df(i) * (*X);
      Eigen::VectorXd dk_Kvec_g = df_Kvec_guess.row(i);
      Eigen::VectorXd dtheta_K_vec = A * Kobj->solve(dK_X,dk_Kvec_g);
      //df_Kvec_guess.row(i) = dtheta_K_vec;
      df_Ki(i) = - diffk_s.dot(dtheta_K_vec);
    }
    g << dbeta, dsigma, df_Ki;
  }else{g << dbeta, dsigma;}
  

  g.array() *= 1.0 -  fixed.array();
  return(-g);

}

void std1DimGaussianWOperator::show_results()
{
  
  Rcpp::Rcout << "LIKELIHOOD:\n";
  if(n_beta>0)
    Rcpp::Rcout << "  beta   = " << beta.transpose();
  Rcpp::Rcout << ", sigma = " << sigma;
  Rcpp::Rcout << "\n**********\n";
  if(estimateOperator == 1)
    Kobj->show_results();
}



Rcpp::List std1DimGaussianWOperator::output_list()
{
  Rcpp::List List;
  if(n_beta > 0 ){
    List["beta"] = beta;
    List["B"]  = B;
  }
  if(Bsigma.cols() > 0)
    List["B.sigma"] = Bsigma;
  
  List["sigma"] = sigma;
  List["A"] = A;
  List["Y"]  = Y;
  
  List["fixed"] = fixed;


  return(List);
}

VectorXd std1DimGaussianWOperator::get_theta()
{
  VectorXd theta;
  theta.setZero(npars);
  if(estimateOperator == 1){
    
    theta << beta,
             sigma,
             Kobj->get_theta();
  }else
  {
    
    theta << beta,
             sigma;    
  }
  return theta;
}

void std1DimGaussianWOperator::init(std::string path)
{
  Rcpp::Rcout << "not implimented: std1DimGaussianWOperator::init\n";
  throw("init not impleneted in with string in std1DimGaussianWOperator\n");
}
Eigen::VectorXd std1DimGaussianWOperator::exp_param()
{
  Eigen::VectorXd param_exp;
  param_exp.setZero(npars);
  for(int i = 0; i < n_beta; i++)
    param_exp(i) = 0;
  for(int i = n_beta; i < n_beta + n_sigma; i++)
    param_exp(i) = 1;
  if(estimateOperator == 1){
    Eigen::VectorXd param_exp_K = Kobj->exp_param();
    int count = 0;
      for(int i = n_beta + n_sigma; i < n_beta + n_sigma + Kobj->npars; i++)
        param_exp(i) = param_exp_K(i - n_beta - n_sigma);
  }
  return(param_exp);
  
}
double std1DimGaussianWOperator::f(Eigen::VectorXd& theta_in)
{
  Eigen::VectorXd temp = theta_in.tail(Kobj->npars);
  if(estimateOperator == 1)
    return(Kobj->f(temp));
  else
    return(0.);
  
}
void std1DimGaussianWOperator::set_Kmatrix(Qmatrix &K_in) 
{
  Kobj = &K_in; 
  update_Kparam();

}
void std1DimGaussianWOperator::update_Kparam()
{
  /*
    corrects the size of the fixed vector when chaning to estimate the parameter from
    in likelihood or latent
  */
    if(npars > fixed.size()){
    Eigen::VectorXd  fixed2;
    fixed2.setZero(npars);
    fixed2.head(fixed.size()) = fixed;
    fixed2.tail(Kobj->npars) = Kobj->fixed;
    fixed = fixed2;
  }else if(npars < fixed.size())
  {
    Rcpp::Rcout << " std1DimGaussianWOperator::updet_Kparam::does resize do the right thing?\n";
    fixed.resize(npars);
    
  }
  if(estimateOperator == 1)
    df_Kvec_guess.setZero(Kobj->npars, A.cols());
  g.setZero(npars);
  p.setZero(npars);
}



Eigen::VectorXd std1DimGaussianWOperator::dsigma_exp_transform(const Eigen::VectorXd & df_pointwise)
{
  Eigen::VectorXd temp =  (this->*sigma_vec)(); 
  temp.array() *= df_pointwise.array();
  return(Bsigma.transpose()*temp);
}

