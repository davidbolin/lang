#include "likelihoodGaussian.h"
using namespace std;

/*------------------------------------------------------------------------------
  simpleDdim likelihood
 -----------------------------------------------------------------------------*/

void simpleDdim::init(string path)
{
	FILE * pFile;
	read_MatrixXd(B,path,"B");
	read_SparseMatrix(A,path,"A");

	read_MatrixXd(Yk,path,"y");
	nd = Yk.rows();
	d = Yk.cols();

	Y = vec(Yk); // only needed if d>1...

	AtA = A.transpose()*A;
	Qsig.resize(d,d);
	for(int k=0;k<d;k++){
		Qsig.insert(k,k) = 1.0;
	}

	for(int i=0;i<nd;i++){
		I.insert(i,i) = 1.0;
	}
	dmu.setZero(d);
	dsigma.setZero(d);
	d2mu.setZero(d,d);
	d2sigma.setZero(d,d);
	dsigmadmu.setZero(d,d);
	npar = 2*d;
	g.setZero(npar);
	p.setZero(npar);
}

void simpleDdim::set_matrices(MatrixXd * Xin, Nin)
{
  X = Xin;
  N = Nin;
}

void simpleDdim::show_results()
{
	cout << "mu = " << beta_mu.transpose();
	cout << ", sigma2 = " << sigma2.transpose();
}


VectorXd simpleDdim::df()
{
	double d2dt, trest;
	VectorXd diffk;

	for(int k=0; k < d; k++){

		diffk = Yk.col(k)-A*((*Xk).col(k));
		diffk.array()-=beta_mu(k);
		dmu(k) = diffk.sum()/sigma2(k);

		SparseMatrix<double,0,int> tmp(d,d);
		tmp.insert(k,k) = 1.0;
		SparseMatrix<double,0,int> tmp2;
		tmp2 = kroneckerProduct(tmp,AtA);
		MatrixXd tmp3 = (*U).transpose()*tmp2*(*QU); //these rows can be
		trest = tmp3.diagonal().sum()/N;       //made more efficent
		d2dt = (diffk.transpose()*diffk + trest)/(2.0*sigma2(k));
    	dsigma(k)= -nd/2.0 + d2dt;

	 	d2sigma(k) = -d2dt;
   	 	dsigmadmu(k) = -dmu(k);
    	d2mu(k) = -nd/sigma2(k);
    }

    g << dmu,
		 dsigma;
	g = -g;
	return g;
}

VectorXd simpleDdim::direction()
{

    H.setZero(g.size(), g.size());
    H << d2mu, dsigmadmu,
    	 dsigmadmu, d2sigma;

 	VectorXd e = H.eigenvalues().real();

    if(e.maxCoeff() > 0){
       dsigmadmu.setZero(dsigmadmu.rows(),dsigmadmu.cols());

       VectorXd tmp = d2mu.diagonal();
		d2mu = tmp.asDiagonal();
		tmp = d2sigma.diagonal();
		d2sigma = tmp.asDiagonal();
        H << d2mu, dsigmadmu,
    	 	dsigmadmu,d2sigma;
        e = H.eigenvalues().real();
        if(e.maxCoeff() > 0){
        	tmp = H.diagonal();
            H = tmp.asDiagonal();
			e = H.eigenvalues().real();
            if(e.maxCoeff() > 0){
                cout << "Warning diag(H) not neg def" << endl;
            }
        }
    }
	p = H.ldlt().solve(g);
	return p;
}

void simpleDdim::vec_to_theta(const VectorXd& theta_vec_in)
{
	VectorXd logsigma2;
	logsigma2.setZero(d);
	beta_mu = theta_vec_in.segment(0,d);
	logsigma2 = theta_vec_in.segment(d,d);
	sigma2 = logsigma2.array().exp();
	for(int i=0;i<d;i++){
		Qsig.coeffRef(i,i) = 1/sigma2(i);
	}
	Qh = kroneckerProduct(Qsig,AtA);
	SparseMatrix<double,0,int> QA;
	QA =kroneckerProduct(Qsig,A.transpose());

	b = QA*(Y-B*beta_mu);
	QA =kroneckerProduct(Qsig,I);
	resid = (Y-B*beta_mu).transpose()*QA*(Y-B*beta_mu);
	logdetQ = nd*logsigma2.sum();
}
