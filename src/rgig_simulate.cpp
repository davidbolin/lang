#include <Rcpp.h>
#include <RcppEigen.h>
using namespace Rcpp;
#include <chrono>

#include "rgig.h"


// [[Rcpp::export]]
Eigen::VectorXd rgig_sample(Eigen::VectorXd p, Eigen::VectorXd a, Eigen::VectorXd b) {
   gig rgig(std::chrono::high_resolution_clock::now().time_since_epoch().count());
   Eigen::VectorXd out;
   out.resize(p.size());
   for(int i =0; i < p.size(); i++)
    out(i) = rgig.sample(p(i) ,a(i) ,b(i) );
   
   return(out);
}
