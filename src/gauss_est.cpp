#include "MatrixAlgebra.h"
#include "gauss_models.h"
#include "optim.h"
#include <ctime>
#include "TimeIt.h"
#include <Rcpp.h>
using namespace std;
using namespace Rcpp;

// [[Rcpp::export]]
List gauss_est(List spdeObj_in){

  clock_t start = clock();
  const double ticks_per_ms = static_cast<double>(CLOCKS_PER_SEC);

  List spdeObj = clone(spdeObj_in);

  List optim_list = spdeObj["optim"];
  List like_list = spdeObj["likelihood"];
  List late_list = spdeObj["latent"];
  List solver_list = spdeObj["solver"];

  VectorXd theta, df;
  gaussianmodel model;
  likelihood* like;
  latent* late;

  int like_type = Rcpp::as<int>(like_list["type"]);
  int late_type = Rcpp::as<int>(late_list["type"]);

  int d = Rcpp::as<int>(late_list["d"]);

  int max_iter  = Rcpp::as<int>(optim_list["max.iter"]);
  int optimtype = Rcpp::as<int>(optim_list["type"]);

  optim Optim(&model, optimtype);

  Optim.stochastic_alpha = Rcpp::as<int>(optim_list["stochastic.alpha"]);
  Optim.alpha_type = Rcpp::as<int>(optim_list["alpha.type"]);
  Optim.line_search_condition = Rcpp::as<int>(optim_list["line.search.condition"]);
  Optim.alpha0 = Rcpp::as<double>(optim_list["alpha0"]);

  double tol= Rcpp::as<double>(optim_list["tol"]);
  int use_chol = Rcpp::as<int>(optim_list["use.chol"]);
  int verbose = Rcpp::as<int>(spdeObj["verbose"]);

  if(verbose>0){
    Rcout << "Optimizer options: " << endl;
    Rcout << "    Type = " << optimtype << endl;
    Rcout << "    Max iter = " << max_iter << endl;
    Rcout << "    Stochastic alpha = " << Optim.stochastic_alpha << endl;
    Rcout << "    alpha type = " << Optim.alpha_type << endl;
    Rcout << "    Line search = " << Optim.line_search_condition << endl;
    Rcout << "    alpha0 = " << Optim.alpha0 << endl;
    Rcout << "    tolerance = " << tol << endl;
    if(use_chol){
      Rcout << "    Cholesky method is used." << endl;
  } else {
      Rcout << "    Iterative method is used with:" << endl;
      Rcout << "      tolerance       : ";
      Rcout << Rcpp::as<double>(optim_list["trace.solver.tol"]) << endl;
      Rcout << "      Trace iter      :  ";
      Rcout << Rcpp::as<int>(optim_list["trace.iter"]) << endl;
      Rcout << "      Solver max iter : ";
      Rcout << Rcpp::as<int>(optim_list["trace.solver.max.iter"]) << endl;
      Rcout << "      Solver drop tol : ";
      Rcout << Rcpp::as<double>(optim_list["solver.drop.tol"]) << endl;
      Rcout << "      Solver fill factor : ";
      Rcout << Rcpp::as<double>(optim_list["solver.fill.factor"]) << endl;
  }
    Rcout << endl;
  }

  if(like_type == 0 && late_type != 2)
  {
    like = new standard1dim;
    (*like).initFromList(like_list);
    if(verbose>0){
      Rcout << "Type of likelihood  : Gaussian Y = AX + e" << endl;
    }
  }else if(like_type == 0){
    like = new nestedspde;
    like_list["B.vec"] = late_list["B.vec"];
    like_list["vec"] = late_list["vec"];
    (*like).initFromList(like_list);
    if(verbose>0){
      Rcout << "Type of likelihood : Gaussian Y = AX + e" << endl;
    }
	/*}
	else if(like_type == 1){
		like = new simpleDdim;
		if(verbose>0){
			cout << "Type of likelihood = simple d dim" << endl;
		}
*/
  } else {
    Rcout << "Likelihood not implemented!" << endl;
  }

  late_list["use.chol"] = solver_list["use.chol"];
  late_list["trace.iter"] = solver_list["trace.iter"];
  late_list["trace.solver.max.iter"] = solver_list["trace.solver.max.iter"];
  late_list["trace.solver.tol"] = solver_list["trace.solver.tol"];
  late_list["solver.drop.tol"] = solver_list["solver.drop.tol"];
  late_list["solver.fill.factor"] = solver_list["solver.fill.factor"];

  if(d==1 || late_type == 4){
    late = new latentGaussian;
    if(verbose>0){
      Rcout << "Latent Gaussian model" << endl;
    }
  } else {
    late = new proportionalGaussian;
    if(verbose>0){
      Rcout << "Latent Gaussian kronecker model" << endl;
    }
  }

  //Rcout << "Init latent" << endl;
  (*late).initFromList(late_list);
  double time_init1 = static_cast<double>(clock()-start)  / ticks_per_ms;
  start = clock();

  //Rcout << "Init model" << endl;
  model.initfromlist(like, late,spdeObj);

  theta = model.get_theta();
  double time_init2 = static_cast<double>(clock()-start)  / ticks_per_ms;
  start = clock();
  int iterations = 0;

  //Rcout << "Start loop" << endl;

  for(int i= 0; i < max_iter; i++)
  {
    //Rcout << "descent_direction" << endl;
    Optim.descent_direction(theta);
    //Rcout << "step" << endl;
	  Optim.step();
	  //Rcout << "theta" << endl;
    theta = Optim.theta_current;
	  //Rcout << "df" << endl;
    df = Optim.func->df(theta);

    if(verbose){
      Rcout << i << ": |grad|= " << df.norm();
      Rcout << ": ";
      //Rcout << model.f(theta) << ": ";
      model.show_results();
    }

    iterations++;
    if(df.norm() < tol)
      break;
  }
  Rcout << endl;
  double time_optim = static_cast<double>(clock()-start)  / ticks_per_ms;
	start = clock();

  double time_finish = static_cast<double>(clock()-start)  / ticks_per_ms;
	  Rcout << "done" << endl;
  if(verbose){
    Rcout << "Timings: " << endl;
    Rcout << "  Init  : " << time_init1 << endl;
        Rcout << "  Init  : " << time_init2 << endl;
    Rcout << "  Optim : " << time_optim << " (" << time_optim/iterations << " per iteration)" << endl;
    Rcout << "  Finish: " << time_finish << endl;
  }

  return(model.output_list());
}