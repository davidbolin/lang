#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#include "optim.h"
#include "likelihoodGaussian.h"
#include "latent.h"
#include "latentGaussian.h"
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <Eigen/Sparse>
#include "MatrixAlgebra.h"
#include "gauss_models.h"
#include "solver.h"
#include "Qmatrix.h"
#include <Rcpp.h>
using namespace std;
using namespace Rcpp;
using namespace Eigen;

// [[Rcpp::export]]
List matern_tests(List init_list)
{
  List late_list = init_list["latent"];


  latent* late = new latentGaussian;
  (*late).initFromList(late_list);


  int N = Rcpp::as<int>(late_list["trace.iter"]);
  int max_iter = Rcpp::as<int>(late_list["trace.solver.max.iter"]);
  double tol = Rcpp::as<double>(late_list["trace.solver.tol"]);
  int use_chol = Rcpp::as<int>(late_list["use.chol"]);

  int n = (*late).n;
  VectorXd X;

  solver * Qsolver;
  if(use_chol) {
    Qsolver = new cholesky_solver;
  } else {
    Qsolver = new iterative_solver;
  }
  (*Qsolver).init(n,N,max_iter,tol);

  (*late).set_solver(Qsolver);
  (*late).set_matrices(&X);

  SparseMatrix<double,0,int> Qh;
  Qh  = Rcpp::as<Eigen::SparseMatrix<double,0,int> >(init_list["Qh"]);
  (*Qsolver).analyze(Qh);
  (*Qsolver).compute(Qh);

  X = Rcpp::as<Eigen::MatrixXd>(init_list["X"]);

  VectorXd theta = Rcpp::as<VectorXd>(init_list["theta"]);
  (*late).vec_to_theta(theta);

  VectorXd df = (*late).df();
  VectorXd dir = (*late).direction();

  Rcpp::List outList;
  outList["df"] = df;
  outList["dir"] = dir;

  return(outList);
}

// [[Rcpp::export]]
List model_tests(List spdeObj_in){

  List spdeObj = clone(spdeObj_in);

  List optim_list = spdeObj["optim"];
  List like_list = spdeObj["likelihood"];
  List late_list = spdeObj["latent"];
  List solver_list = spdeObj["solver"];

  VectorXd theta, df, dir;
  gaussianmodel model;
  likelihood* like;
  latent* late;

  int like_type = Rcpp::as<int>(like_list["type"]);
  int late_type = Rcpp::as<int>(late_list["type"]);



  if(like_type == 0 && late_type != 2){
    like = new standard1dim;
  }else if(like_type == 0){
    like = new nestedspde;
    like_list["B.vec"] = late_list["B.vec"];
    like_list["vec"] = late_list["vec"];
  }

  (*like).initFromList(like_list);

  late_list["use.chol"] = solver_list["use.chol"];
  late_list["trace.iter"] = solver_list["trace.iter"];
  late_list["trace.solver.max.iter"] = solver_list["trace.solver.max.iter"];
  late_list["trace.solver.tol"] = solver_list["trace.solver.tol"];

  late = new latentGaussian;
  (*late).initFromList(late_list);

  model.initfromlist(like, late,spdeObj);

  theta = model.get_theta();
  double f  = model.f(theta);

  df = model.df(theta);

  dir = model.direction(theta);

  Rcpp::List outList;
  outList["f"] = f;
  outList["df"] = df;
  outList["dir"] = dir;
  outList["theta"] = theta;

  return(outList);
}



// [[Rcpp::export]]
List Qmatrix_tests(List init_list)
{

  Rcpp::List Qs_list = init_list;

  int Qs_type = Rcpp::as<int>(Qs_list["type"]);

  Qmatrix * Qs;
  if(Qs_type==0){
    Rcpp::Rcout << "MaternMatrix" << endl;
    Qs = new MaternMatrix;
  } else if(Qs_type == -1){
    Rcpp::Rcout << "constMatrix" << endl;
    Qs = new constMatrix;
  } else if(Qs_type == 2){
    Rcpp::Rcout << "ARmatrix" << endl;
    Qs = new ARmatrix;
  }
  Rcpp::Rcout << "Init Q " << endl;
  (*Qs).initFromList(Qs_list);

  int i = Rcpp::as<int>(Qs_list["i"]);
  int j = Rcpp::as<int>(Qs_list["j"]);

  Rcpp::List outList;
  Rcpp::Rcout << "Calculate dQ " << endl;
  outList["dQ"] = (*Qs).df(i);
  Rcpp::Rcout << "Calculate trace" << endl;
  outList["trace"] = (*Qs).trace(i);
  Rcpp::Rcout << "Calculate d2Q" << endl;
  outList["d2Q"] = (*Qs).d2f(i,i);
  Rcpp::Rcout << "Calculate trace2" << endl;
  outList["trace2"] = (*Qs).trace2(i,j);

  return(outList);
}