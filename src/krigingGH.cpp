#include <Rcpp.h>
#include <RcppEigen.h>
#include "noiseGH.h"
#include "latentGH.h"
#include "likelihoodGaussian.h"
#include "modelGH.h"


// [[Rcpp::export]]
Eigen::MatrixXd sampleKriging(List spdeObj_in) {

   Rcpp::List spdeObj      = clone(spdeObj_in);
   
   
   Rcpp::List latent_list  = spdeObj["latent"]; 
   Rcpp::List likelihood_list  = spdeObj["likelihood"]; 
   Rcpp::List noise_list   = latent_list["noise"];
   Rcpp::List optim_list   = spdeObj["optim"]; 
   Rcpp::List solver_list   = spdeObj["solver"];
  modelGH model;
  
   if(latent_list.containsElementNamed("X") == 1){
     Eigen::VectorXd Xtemp = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
     model.X.resize(Xtemp.rows(), Xtemp.cols());
     model.X = Xtemp;
   }
    else
    {
      Rcpp::Rcout << "GH_est::must supply X in latent_list\n";
      throw;
    }
    
   // setting up the objects
  GALnoise         noise_class;
  noise_class.initFromList(noise_list);
  
  
  latent* latentGH;
  likelihood* likGaussian;
  
  int GAL_ = 0;
  if(noise_list.containsElementNamed("GAL") == 1)
    GAL_ = Rcpp::as<int>(noise_list["GAL"]);
    
    
  Qmatrix* Kobj;   
  if(Rcpp::as<int>(latent_list["type"]) == 4){
    Kobj     = new MultivariateMaternMatrixOperator;
  }else{
    Kobj     = new MaternMatrixOperator;
  }
   Kobj->initFromList(latent_list, spdeObj["solver"]);
   
   latentGH = new genericKGH;
   latentGH->X = &(model.X);
   latentGH->initFromList( latent_list);
   latentGH->setNoise(&noise_class);  
   latentGH->set_K(Kobj);
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->initFromList(spdeObj["likelihood"]);
  
  
  likGaussian->set_Kmatrix(*Kobj);
  model.set_likelihood(*likGaussian);
  likGaussian->set_estimateOperator(1 - latentGH->estimateOperator);


  
  
  
  
  model.set_latent(*latentGH);
  model.initSolver(spdeObj["solver"]);
  Eigen::VectorXd theta;
  
  
  if( spdeObj.containsElementNamed("fixed") )
    model.set_fixed(Rcpp::as<Eigen::VectorXd>(spdeObj["fixed"]));
    
    
  if( spdeObj.containsElementNamed("theta") ){
     theta = Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]);
    model.vec_to_theta(theta);
  }
  else
  {
    theta = model.get_theta();
  }
   model.update0();
  model.update_param();  

  // updating the inital parameter
  model.simulate();
  int nsim_krigin     = Rcpp::as<int>(spdeObj["nsim_kriging"]);
  int nthin_kriging   = Rcpp::as<int>(spdeObj["nthin_kriging"]);
  int nburning_kriging   = Rcpp::as<int>(spdeObj["nburnin_kriging"]);
  int RBmean;
  if( spdeObj.containsElementNamed("RBmean"))
   RBmean   = Rcpp::as<int>(spdeObj["RBmean"]);
  else
   RBmean   = 0;
  Eigen::MatrixXd Xs;
  Xs.setZero(nsim_krigin, model.X.size());

  for(int i = 0; i < nburning_kriging; i++)
    model.simulate();

  for(int i = 0; i < nsim_krigin; i++)
  {
    for(int ii = 0; ii < nthin_kriging; ii++)
      model.simulate();
    if(RBmean){
      Xs.row(i) = model.return_mu();
    } else {
      Xs.row(i) = model.X;
    }
    if( i % 1000 == 0)
      Rcpp::Rcout << ".";
  }
  return(Xs);
}

