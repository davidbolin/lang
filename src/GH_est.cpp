#include <Rcpp.h>
#include <RcppEigen.h>
#include "noiseGH.h"
#include "latentGH.h"
#include "modelGH.h"
#include "likelihoodGaussian.h"
#include "optim.h"
#include <cmath>
#include <Eigen/Dense>
#include "eigen_add_on.h"

// [[Rcpp::export]]
Rcpp::List GH_est(Rcpp::List spdeObj_in){
   Rcpp::List spdeObj      = clone(spdeObj_in);
   
   int verbose = Rcpp::as<int>(spdeObj["verbose"]);
   
   Rcpp::List latent_list  = spdeObj["latent"]; 
   Rcpp::List likelihood_list  = spdeObj["likelihood"]; 
   Rcpp::List noise_list   = latent_list["noise"];
   Rcpp::List optim_list   = spdeObj["optim"]; 
   Rcpp::List solver_list   = spdeObj["solver"];
  modelGH model;
   if(latent_list.containsElementNamed("X") == 1){
     Eigen::VectorXd Xtemp = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
     model.X.resize(Xtemp.rows(), Xtemp.cols());
     model.X = Xtemp;
   }
    else
    {
      Rcpp::Rcout << "GH_est::must supply X in latent_list\n";
      throw;
    }
    
   // setting up the objects
  GALnoise         noise_class;
  noise_class.initFromList(noise_list);
  
  
  latent* latentGH;
  likelihood* likGaussian;
  
  int GAL_ = 0;
  if(noise_list.containsElementNamed("GAL") == 1)
    GAL_ = Rcpp::as<int>(noise_list["GAL"]);
    
  Rcpp::Rcout << "here\n";
  Qmatrix* Kobj;   
  if(Rcpp::as<int>(latent_list["type"]) == 4){
    Kobj     = new MultivariateMaternMatrixOperator;
  }else{
    Kobj     = new MaternMatrixOperator;
  }
   Kobj->initFromList(latent_list, spdeObj["solver"]);
   
   latentGH = new genericKGH;
   latentGH->X = &(model.X);
   latentGH->initFromList( latent_list);
   latentGH->setNoise(&noise_class);  
   latentGH->set_K(Kobj);
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->initFromList(spdeObj["likelihood"]);
  
  likGaussian->set_Kmatrix(*Kobj);
  model.set_likelihood(*likGaussian);
  likGaussian->set_estimateOperator(1 - latentGH->estimateOperator);


  
  
  model.nGibbsIter = Rcpp::as<int>(optim_list["gibbs.iter"]);
  model.prenGibbsIter = Rcpp::as<int>(optim_list["gibbs.preiter"]);
  model.set_latent(*latentGH);
  model.initSolver(spdeObj["solver"]);
  model.update0();


    

    

  
  int max_iter  = Rcpp::as<int>(optim_list["max.iter"]);
  int optimtype = Rcpp::as<int>(optim_list["type"]); 
  optim Optim(&model, optimtype);

  Optim.stochastic_alpha      = Rcpp::as<int>(optim_list["stochastic.alpha"]);
  Optim.alpha_type            = Rcpp::as<int>(optim_list["alpha.type"]);
  Optim.line_search_condition = Rcpp::as<int>(optim_list["line.search.condition"]);
  Optim.alpha0                = Rcpp::as<double>(optim_list["alpha0"]);

  double tol= Rcpp::as<double>(optim_list["tol"]);
  int use_chol = Rcpp::as<int>(optim_list["use.chol"]);
  int iterations = 0;


  Eigen::VectorXd theta;
  
  
  if( spdeObj.containsElementNamed("fixed") )
    model.set_fixed(Rcpp::as<Eigen::VectorXd>(spdeObj["fixed"]));
  if( spdeObj.containsElementNamed("theta") ){
     theta = Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]);
    model.vec_to_theta(theta);
  }
  else
  {
    theta = model.get_theta();
  }

  
  if(Optim.alpha_type != 2)
    throw "alpha_type, must be 2 \n";
    
  int max_iter2  = Rcpp::as<int>(optim_list["max.iter2"]);
  int max_iter3 = Rcpp::as<int>(optim_list["max.iter3"]); 
  int burnin = Rcpp::as<int>(optim_list["burnin"]); 
  int iter_count = 0;
  double alpha0 = Optim.alpha0; 
  MatrixXd Thetas;
  
  Thetas.setZero(max_iter + max_iter2 + max_iter3,theta.size());


  for(int i = 0; i < burnin; i++)
    model.simulate();
 
  
  model.init_Hestimation(optim_list);
  model.set_H0();
  
  for(int i = 0; i < max_iter3; i++){
    if(((theta - theta).array() != (theta - theta).array()).all())
      break;
     Optim.alpha0 = alpha0 * pow(10., -1. - 2.*(max_iter3 - i)/((double) max_iter3) );
    Thetas.row(iter_count) = theta;
    Optim.descent_direction(theta);
    
   
    Optim.step();
    theta = Optim.theta_current;
    Eigen::VectorXd df = Optim.func->df(theta);
    iter_count++;
    if(verbose){
      Rcpp::Rcout << "max iter3: " << i << "/"<< max_iter3;
      model.show_results();
    }
      ;

  }
  
  Optim.alpha0 = alpha0;
  
  for(int i = 0; i < max_iter2; i++){
    if(((theta - theta).array() != (theta - theta).array()).all())
      break;
    Thetas.row(iter_count) = theta;
    Optim.descent_direction(theta);
    Optim.step();
    theta = Optim.theta_current;
    iter_count++;  
    if(verbose){
      Rcpp::Rcout << "max iter2: " << i << "/"<< max_iter2 ;
      model.show_results();
    }
      
  
  }
  Optim.stochastic_alpha = 1;
  Optim.stochastic_c0 = Rcpp::as<double>(optim_list["stochastic_c0"]);
  Optim.stochastic_c1 = Rcpp::as<double>(optim_list["stochastic_c1"]); 
  Optim.stochastic_c2 = Rcpp::as<double>(optim_list["stochastic_c2"]);   
  Optim.iter_count = 0;
  
  for(int i= 0; i < max_iter; i++)
  {
    if(((theta - theta).array() != (theta - theta).array()).all())
      break;
    Thetas.row(iter_count) = theta;
    Optim.descent_direction(theta);
    Optim.step();
    
    theta = Optim.theta_current;
    Eigen::VectorXd df = Optim.func->df(theta);
    iter_count++;  
    
    if(verbose){
      Rcpp::Rcout << "max iter1: " << i << "/"<< max_iter ;
      model.show_results();
    }
    
    iterations++;
    if(df.norm() < tol)
      break;


  }
    if(((theta - theta).array() != (theta - theta).array()).all() ==1)
    {
      Rcpp::Rcout << "theta becomes nan unstable\n";
      Rcpp::Rcout << "theta = " << theta.transpose() <<"\n";
      throw("error");
    }
  Rcpp::List result = model.output_list();
  result["operator"] =  model.like->get_Kobj()->output_list();
  result["Thetas"] = Thetas;
  result["fixed"] = model.get_fixed();
  result["theta"] = model.get_theta();
  result["X"] = model.X;
  return(result);

}