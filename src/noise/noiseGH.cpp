// [[Rcpp::depends(RcppEigen)]]
#include "noiseGH.h"
#include "error_check.h"
#include "eigen_add_on.h"
GHnoise::GHnoise()
{
  V_adj = 1e-10;
  b_adj = 1e-14;
}
void GHnoise::simulateV()
{
    for(int i = 0; i < n; i++){
      V[i] = rgig.sample(pGIG[i], aGIG[i] ,bGIG[i] + b_adj);  
      V[i] +=  V_adj;
    }
    
}
Eigen::VectorXd GALnoise::estimate_exp()
{
  Eigen::VectorXd est_exp;
  est_exp.setZero(npars);
  int n_par = Bdelta.cols() + Bmu.cols();
  if(sigma_transformed == 1 ){
    for(int i =n_par; i <  n_par + Bsigma.cols(); i++)
      est_exp(i) = 1;
  }
  n_par += Bsigma.cols();
  if(lambda_transformed == 1 || lambda_transformed == 2){
    for(int i =n_par; i <  n_par + Blambda.cols(); i++)
      est_exp(i) = 1;
  }  
  return(est_exp);
}

void GALnoise::show_results()
{
  Rcpp::Rcout << "noise::";
  if(GAL==1)
    Rcpp::Rcout << "Generalised asymmetric Laplace\n";
  else
    Rcpp::Rcout << "NIG\n";
    
  Rcpp::Rcout << "delta = " << delta <<"\n";
  Rcpp::Rcout << "mu = " << mu << "\n";
  if(sigma_transformed == 0){
    Rcpp::Rcout << "sigma = " << sigma <<"\n";
  }else if(sigma_transformed ==1)
  {
    Rcpp::Rcout << "sigma = " << sigma.array().exp() <<"\n";
  }
  if(lambda_transformed == 0){
    Rcpp::Rcout << "lambda = " << lambda <<"\n";
  }else
  {
    Rcpp::Rcout << "lambda = " << lambda.array().exp() <<"\n";
  }
  Rcpp::Rcout << "lambda_transformed = " << lambda_transformed << "\n";
}
void GALnoise::initFromList(Rcpp::List const &init_list)
{
  std::vector<std::string> check_names =  {"B.mu", "B.delta", "B.sigma","B.lambda","delta","mu","sigma","lambda"};
  check_Rcpplist(init_list, check_names, "GALnoise::initFromList");
  
  rgig.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  npars = 0;
  eigen_matrix_from_list(Bmu, init_list, "B.mu");
  //Bmu     = Rcpp::as<Eigen::MatrixXd>(init_list["B.mu"]);
  npars += Bmu.cols();
  //Bdelta  = Rcpp::as<Eigen::MatrixXd>(init_list["B.delta"]);
  eigen_matrix_from_list(Bdelta, init_list, "B.delta");
  npars += Bdelta.cols();
  //Bsigma  = Rcpp::as<Eigen::MatrixXd>(init_list["B.sigma"]);
  eigen_matrix_from_list(Bsigma, init_list, "B.sigma");
  
  npars += Bsigma.cols();
  //Blambda = Rcpp::as<Eigen::MatrixXd>(init_list["B.lambda"]);
  eigen_matrix_from_list(Blambda, init_list, "B.lambda");
  npars += Blambda.cols();
  
  
  eigen_vector_from_list(delta, init_list, "delta");
  eigen_vector_from_list(mu, init_list, "mu");
  eigen_vector_from_list(sigma, init_list, "sigma");
  eigen_vector_from_list(lambda, init_list, "lambda");
  //delta  = Rcpp::as<Eigen::VectorXd>(init_list["delta"]);
  //mu     = Rcpp::as<Eigen::VectorXd>(init_list["mu"]);
  //sigma  = Rcpp::as<Eigen::VectorXd>(init_list["sigma"]);
  //lambda = Rcpp::as<Eigen::VectorXd>(init_list["lambda"]);
  if(init_list.containsElementNamed("fixed")){
    eigen_vector_from_list(fixed, init_list, "fixed");
  }
  else
    fixed = Eigen::VectorXd::Zero(npars);
  
  
  if(init_list.containsElementNamed("sigma_transformed"))
    sigma_transformed = Rcpp::as<int>(init_list["sigma_transformed"]);
  else
    sigma_transformed = 0;

  if(init_list.containsElementNamed("lambda_transformed"))
    lambda_transformed = Rcpp::as<int>(init_list["lambda_transformed"]);
  else
    lambda_transformed = 0;

  if(init_list.containsElementNamed("GAL"))
    GAL = Rcpp::as<int>(init_list["GAL"]);
  else
    GAL = 0;

  initSigma();
  initLambda();
  n = Bdelta.rows();
  V.setOnes(n);
  
  aGIG.resize(n);
  bGIG.resize(n);
  pGIG.resize(n);
  muE.resize(n);
  E.resize(n);
  resid.resize(n);
  
  
}

void GALnoise::initLambda()
{
  if(lambda_transformed > 2)
  {
    throw "GALnoise::initLambda() , lambda_transformed should be either 0,1,2 ";
  }
  if(lambda_transformed == 1){
    lambda_vec = &GALnoise::lambda_exp_transform;
    dlambda_vec =  &GALnoise::dlambda_exp_transform;
  }
  else if(lambda_transformed == 2){
    lambda_vec = &GALnoise::lambda_expi_transform;
    dlambda_vec =  &GALnoise::dlambda_expi_transform;
  }else{
    lambda_vec  = &GALnoise::lambda_no_transform;
    dlambda_vec = &GALnoise::dlambda_no_transform;
  }
  
  
}
void GALnoise::initSigma()
{
  if(sigma_transformed > 1)
  {
    throw "GALnoise::initSigma() , sigma_transformed should be either zero or one ";
  }
  if(sigma_transformed == 1){
    sigma_vec = &GALnoise::sigma_exp_transform;
    dsigma_vec =  &GALnoise::dsigma_exp_transform;
  }
  else{
    sigma_vec  = &GALnoise::sigma_no_transform;
    dsigma_vec = &GALnoise::dsigma_no_transform;
  }
}

Rcpp::List GALnoise::toRcppList()
{
  Rcpp::List GalList;
  if( Bmu.cols() > 0)
    GalList["B.mu"]     = Bmu;
  if( Bdelta.cols() > 0)
    GalList["B.delta"] = Bdelta;
    
  GalList["B.sigma"]   = Bsigma;
  GalList["B.lambda"]  = Blambda;
  GalList["delta"]    = delta;
  GalList["mu"]       = mu;
  GalList["sigma"]    = sigma;
  GalList["lambda"]  = lambda;
  GalList["fixed"] = fixed;
  GalList["GAL"] = GAL;
  GalList["sigma_transformed"] = sigma_transformed;
  GalList["lambda_transformed"] = lambda_transformed;
  if( V.size() > 0)
    GalList["V"]       = V;
  
  return(GalList);
}

Eigen::VectorXd  GALnoise::get_theta() {
  int count = 0;
  int i;
  Eigen::VectorXd thetaVec;
  thetaVec.resize(npars);
  for(i = 0; i < Bdelta.cols(); i++)
    thetaVec[i + count] = delta[i] ;
  count  += Bdelta.cols();
  
  for(i = 0; i < Bmu.cols(); i++)
    thetaVec[i + count] = mu[i] ;
  count  += Bmu.cols();
  
  for(i = 0; i < Bsigma.cols(); i++)
    thetaVec[i + count] = sigma[i] ;
  count  += Bsigma.cols();
  
  for(i = 0; i < Blambda.cols() ; i++)
    thetaVec[i + count] = lambda[i]; 
  
  return(thetaVec);
}
void GALnoise::vec_to_theta(const Eigen::VectorXd & thetaVec) { 
  int count = 0;
  int i;
  for(i = 0; i < Bdelta.cols(); i++)
    delta[i] = thetaVec[i + count];
  count  += Bdelta.cols();
  
  for(i = 0; i < Bmu.cols(); i++)
    mu[i] = thetaVec[i + count];
  count  += Bmu.cols();
  
  for(i = 0; i < Bsigma.cols(); i++)
    sigma[i] = thetaVec[i + count];
  count  += Bsigma.cols();
  
  for(i = 0; i < Blambda.cols() ; i++)
    lambda[i] = thetaVec[i + count];

};
void GALnoise::vec_to_theta(const double* thetaVec) { 
  int count = 0;
  int i;
  
  for(i = 0; i < Bdelta.cols(); i++)
    delta[i] = thetaVec[i + count];
  count  += Bdelta.cols();
  
  for(i = 0; i < Bmu.cols(); i++)
    mu[i] = thetaVec[i + count];
  count  += Bmu.cols();
  
  for(i = 0; i < Bsigma.cols(); i++)
    sigma[i] = thetaVec[i + count];
  count  += Bsigma.cols();
  
  for(i = 0; i < Blambda.cols() ; i++)
    lambda[i] = thetaVec[i + count];

};
void GALnoise::update()
{
  
   bGIG = E;
   bGIG -= Bdelta * delta; 
   bGIG = bGIG.array() / (this->*sigma_vec)().array();
   bGIG = bGIG.array().square();
    if(GAL== 0)
      bGIG.array() +=((this->*lambda_vec)()).array();
      
}
void GALnoise::updateV()
{
    iVsigma.setOnes(V.size()); 
    iVsigma.array() /= V.array()* ((this->*sigma_vec)()).array().pow(2);
    muE = Bmu*mu;      // the mean parameter used by latent
    muE.array() /=   ((this->*sigma_vec)()).array().pow(2);
    muE.array() += (Bdelta * delta).array() * iVsigma.array();
    
}
void GALnoise::update_param()
{
  if(n<=0)
    throw "GALnoise::update_param() , must bet: n >= 0 ";
    
  
  

   
   if( Bmu.cols() > 0 )
   {
      
     aGIG = Bmu * mu;
     aGIG.array() = pow(aGIG.array()/ ((this->*sigma_vec)()).array(),2);
   }else{
        aGIG.setZero(n);
   }
   aGIG.array() += 2;
   if(GAL== 1){
    pGIG = ((this->*lambda_vec)()).array(); 
    pGIG.array() -= 0.5;
   }
   else
   {
     pGIG.setOnes(n);
     pGIG.array() *=-1.;
   }
}

void GALnoise::setB(const Eigen::MatrixXd* Bdelta_in, const Eigen::MatrixXd* Bsigma_in, const Eigen::MatrixXd *Bmu_in,const Eigen::MatrixXd * Blambda_in)
{
  Bmu    = *Bmu_in;
  npars += Bmu_in->cols(); 
  Bdelta = *Bdelta_in;
  npars += Bdelta_in->cols();
  
  if(Bsigma_in->cols() == 0)
    throw "GALnoise::setB() , Bsigma needs at least one col ";
    
  npars += Bsigma_in->cols();
  Bsigma = *Bsigma_in;
  
  if(Blambda_in->cols() == 0)
    throw "GALnoise::setB() , Blambda needs at least one col ";
  
  Blambda = *Blambda_in;
  npars += Blambda_in->cols();
  
  mu.resize(Bmu.cols());
  lambda.resize(Blambda.cols());
  delta.resize(Bdelta.cols());
  sigma.resize(Bsigma.cols());
}

Eigen::VectorXd GALnoise::df()
{
  
  Eigen::VectorXd df_vec;
  df_vec.resize(npars);
  count_df = 0;
  resid = E;

  if( Bdelta.cols() > 0)
    resid.array() -= (Bdelta*delta).array();
  if( Bmu.cols() > 0)
    resid.array() -= V.array()*(Bmu * mu).array();
  resid_square = resid.array().pow(2);
  resid.array() /=  V.array()*  ((this->*sigma_vec)()).array().pow(2);
  resid_square.array() /= V.array()  *  ((this->*sigma_vec)()).array().pow(2);
  ddelta(df_vec);
  dmu(df_vec);
  dsigma(df_vec);
  dgamma(df_vec);
  df_vec.array() *= 1 - fixed.array();
  return(-df_vec);
}

void GALnoise::dmu(Eigen::VectorXd& df_vec)
{
  
  if(Bmu.cols() > 0){
    Eigen::VectorXd resV(resid.size());
    resV.array() = resid.array()*V.array();
    df_vec.segment(count_df, Bmu.cols()).noalias() =  Bmu.transpose() * resV;
    count_df += Bmu.cols();
  }
}
void GALnoise::ddelta(Eigen::VectorXd& df_vec)
{
  if(Bdelta.cols() > 0){
    df_vec.segment(count_df, Bdelta.cols()).noalias() =  Bdelta.transpose() *resid ;
    
    count_df += Bdelta.cols();
  }
}
void GALnoise::dsigma(Eigen::VectorXd& df_vec)
{
  Eigen::VectorXd resSigmares(resid.size());
  Eigen::VectorXd det_;
  det_.setOnes(resid.size());
  det_.array() /=  ((this->*sigma_vec)()).array();
  resSigmares = resid_square.array() / ((this->*sigma_vec)()).array() - det_.array();
  df_vec.segment(count_df,  Bsigma.cols() ).noalias() =  (this->*dsigma_vec)(resSigmares).transpose();

  count_df += Bsigma.cols();
  
}

Eigen::VectorXd GALnoise::dsigma_exp_transform(const Eigen::VectorXd & df_pointwise)
{
  Eigen::VectorXd temp =  (this->*sigma_vec)(); 
  temp.array() *= df_pointwise.array();
  return(Bsigma.transpose()*temp);
}
Eigen::VectorXd GALnoise::dlambda_exp_transform(const Eigen::VectorXd & df_pointwise)
{
  Eigen::VectorXd temp =  (this->*lambda_vec)(); 
  temp.array() *= df_pointwise.array();
  return(Blambda.transpose()*temp);
}
Eigen::VectorXd GALnoise::dlambda_expi_transform(const Eigen::VectorXd & df_pointwise)
{
  
  Eigen::VectorXd temp = lambda;
  temp.array() = temp.array().exp();
  Eigen::MatrixXd Blambda_exp =  Blambda * temp.asDiagonal(); 
  return(Blambda_exp.transpose()*df_pointwise);
}

void GALnoise::dgamma(Eigen::VectorXd& df_vec)
{
  
  Eigen::VectorXd Blambda_lambda(resid.size());
  Eigen::VectorXd dlambdas(resid.size());
  Blambda_lambda = ((this->*lambda_vec)());
  if(GAL == 1){
    for(int i = 0; i < resid.size(); i++){
      if(Blambda_lambda(i) >1){
      dlambdas(i) =  -R::digamma(Blambda_lambda(i)) + log(V(i) -   V_adj);
      }
      else
      {
        double x_ = 1e-2;
        if(V(i)>x_ )
          dlambdas(i) =  -R::digamma(Blambda_lambda(i) + 1) + 1./Blambda_lambda(i) + log(1000*(V(i) -   V_adj)) - log(1000);
        else
        {
          double s_ = Blambda_lambda(i) ;
          double e_ = 0.0001;
           //dlambdas(i) = (R::pgamma(x_, s_ + e_, 1. ,1, 1) - R::pgamma(x_, s_ -e_, 1. ,1, 1))/(2*e_);
           dlambdas(i) =  -R::digamma(Blambda_lambda(i) + 1) + 1./Blambda_lambda(i) + log(1000*(V(i) -   V_adj)) - log(1000);
          
        }
      }
      
    }
  }
  else
  {
    for(int i = 0; i < resid.size(); i++)
      dlambdas(i) = 0.5*( 1./Blambda_lambda(i) - 1./(V(i) )  ) + pow(2*Blambda_lambda(i),-0.5);
  }
  df_vec.segment(count_df, Blambda.cols() ).noalias() = (this->*dlambda_vec)( dlambdas); 
    count_df += Bsigma.cols();
}

Eigen::MatrixXd GALnoise::calcH()
{
  Eigen::MatrixXd H;
  count_df = 0;
  H.setZero(npars,npars);
  Eigen::VectorXd isigma_vec;
  isigma_vec.setOnes(V.size());
  isigma_vec.array() /=  ((this->*sigma_vec)()).array();
  Eigen::VectorXd  resSigmares = -   resid.array() * isigma_vec.array() ;
  Eigen::MatrixXd tmp;
  
  
  
  Eigen::MatrixXd BmuIsigma;
  if(Bmu.cols()> 0)
    BmuIsigma = Bmu.array().colwise()  *isigma_vec.array().pow(2);
  
 if((Bdelta.cols() > 0)){ 
   //**********************************************
   // H() delta,delta
   //**********************************************
   tmp = Bdelta;
   tmp.array().colwise() *= iVsigma.array();
  H.block(count_df,count_df,Bdelta.cols(),Bdelta.cols()) = - Bdelta.transpose() * tmp;
  count_df += Bdelta.cols();
  //**********************************************
  // H() delta, mu
  //**********************************************
  if(Bmu.cols() > 0){ 
    H.block(count_df - Bdelta.cols(), count_df, Bdelta.cols(), Bmu.cols()) = 
    - Bdelta.transpose() * BmuIsigma;
    H.block(count_df, count_df - Bdelta.cols() , Bmu.cols(), Bdelta.cols()) = H.block(count_df - Bdelta.cols(), count_df, Bdelta.cols(), Bmu.cols()).transpose();
  }
 //**********************************************
  // H() delta, sigma
 //**********************************************
   count_df += Bmu.cols();
   tmp = Bsigma;
   tmp.array().colwise() *= resSigmares.array();
   H.block(count_df, count_df - Bdelta.cols() - Bmu.cols(), Bdelta.cols(), Bsigma.cols())  = - 2 * (Bdelta.transpose() * tmp);
  H.block(count_df - Bdelta.cols() - Bmu.cols(), count_df, Bsigma.cols(), Bdelta.cols())  = H.block(count_df, count_df - Bdelta.cols() - Bmu.cols(), Bdelta.cols(), Bsigma.cols()).transpose();

 }
 count_df = Bdelta.cols();
 //**********************************************
 // H() mu, mu
 //**********************************************
 if(Bmu.cols() > 0){ 
    tmp = BmuIsigma;
    tmp.array().colwise() *=  V.array();
    H.block(count_df,count_df,Bmu.cols(),Bmu.cols()) =  - Bmu.transpose() * tmp;
    count_df += Bmu.cols();
     tmp = Bsigma;
     tmp.array().colwise() *=  resSigmares.array();
     tmp.array().colwise() *=  V.array();
    H.block(count_df, count_df  - Bmu.cols(), Bmu.cols(), Bsigma.cols())  =- 2 * (Bmu.transpose() * tmp);
    H.block(count_df  - Bmu.cols(), count_df, Bsigma.cols(), Bmu.cols()) = H.block(count_df, count_df  - Bmu.cols(), Bmu.cols(), Bsigma.cols()).transpose();
 }
 
 
 //**********************************************
 // H sigma sigma
 //**********************************************
 Eigen::VectorXd  resddSigmares  = 3 * resid_square.array() * isigma_vec.array().pow(2);
 Eigen::VectorXd  dddet;
 dddet.setOnes(resid.size());
 dddet.array() *= isigma_vec.array().pow(2);
 tmp = Bsigma;
 tmp.array().colwise() *=  resddSigmares.array();
 H.block(count_df,count_df,Bsigma.cols(),Bsigma.cols()) = -Bsigma.transpose() * tmp ;
 tmp = Bsigma;
 tmp.array().colwise() *=  dddet.array();
 H.block(count_df,count_df,Bsigma.cols(),Bsigma.cols()) +=  Bsigma.transpose() * tmp ; 
 count_df += Bsigma.cols();
 //**********************************************
  Eigen::VectorXd Blambda_lambda(resid.size());
  Eigen::VectorXd dlambda_vec(resid.size());
  Blambda_lambda =  ((this->*lambda_vec)());
  for(int i = 0; i < resid.size(); i++)
    dlambda_vec(i) =  -R::trigamma(Blambda_lambda(i));
  
  tmp = Blambda;
  tmp.array().colwise() *=  dlambda_vec.array();
  H.block(count_df,count_df,Blambda.cols(),Blambda.cols())   = Blambda.transpose() * tmp;


  for(int i =0; i < H.cols(); i++){
    if(fixed[i] == 1)
    {
      H.col(i) *= 0;
      H.row(i) *= 0; 
    }
    
    H(i,i) -= fixed[i];
  }
    
  return(H);
}
//
// checks if coeffients for sigma or lambda ar negative!
double GALnoise::f(const Eigen::VectorXd& theta)
{
   int count;
    count = Bdelta.cols() + Bmu.cols();
   if(sigma_transformed == 1)
    count += Bsigma.cols();
    if(lambda_transformed == 1)
      count += Blambda.cols();
      
   int max_count  = count + Bsigma.cols() + Blambda.cols();
   if(sigma_transformed == 1)
    max_count -= 2*Bsigma.cols();
    if(lambda_transformed == 1)
      max_count -= 2*Blambda.cols();
  for(int i = count; i < max_count; i++)
  {
    if(theta(i)< 0)
      return(NAN);
  }
  return(0);
    
}
