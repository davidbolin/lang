#ifndef __SPATIAL__LATENTGH__
#define __SPATIAL__LATENTGH__
//' @TODO: not implimented for openmp support right now!
///
/// stationarymatern
/// this is the latent class for fields where the latent field is GenHyperbolic class
/// the model is build on the equation
///  K*X = E
///  here E is a noise class (GHnoise)
#include <RcppEigen.h>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include "Qmatrix.h"
#include "latent.h"
using namespace Rcpp;

class stationarymatern :  public virtual latent {
  private:

    Eigen::SparseMatrix<double,0,int>  K;
    Eigen::SparseMatrix<double,0,int>  C;
    Eigen::SparseMatrix<double,0,int>  G; // the operator matrix
    solver * Ksolver;
    DiagonalMatrix<double,Dynamic> Phi;

    void dK_kappa();
    void dK_kappa_num_eps();
    Eigen::MatrixXd ddkappa0;
    Eigen::VectorXd dkappa0;
    int iN;
    int imiter;
    double itol;
  public:

    int calc_ddk;
    int kappa_transformed;
    void initKappa();
    
    Eigen::VectorXd (stationarymatern::*kappa_vec) ();
    Eigen::MatrixXd (stationarymatern::*dkappa_mat) ();
    Eigen::VectorXd kappa_no_transform()  {return(Bkappa*kappa);};
    Eigen::MatrixXd dkappa_no_transform() {return(Bkappa);};
    Eigen::VectorXd kappa_exp_transform()  {Eigen::VectorXd temp =  Bkappa*kappa; temp.array() = temp.array().exp(); return(temp);};
    Eigen::MatrixXd dkappa_exp_transform();


    void set_fixed(const Eigen::VectorXd& fixed_in)   {fixed = fixed_in; noiseClass->fixed = fixed.tail(noiseClass->npars);};
     
    void initSolver(Rcpp::List const &);
    stationarymatern() {npars = 0;};
    ~stationarymatern() {delete Ksolver;};
    void init(Eigen::SparseMatrix<double,0,int>* , Eigen::SparseMatrix<double,0,int>* , GHnoise*);
    void setX(Eigen::VectorXd *Xin){ X = Xin;};
    void setK(Eigen::SparseMatrix<double,0,int> *Kin){ K = *Kin;};
    GHnoise *noiseClass;
    void setNoise(GHnoise *noise){noiseClass = noise; npars += noiseClass->npars; fixed = noiseClass->fixed;};
    Eigen::MatrixXd Bkappa;
    Eigen::VectorXd kappa;
    void updateNoise();
    void update();
    void update_param();
    void initFromList(Rcpp::List const &);
    int use_chol;
    void simulateNoise() {noiseClass->simulateV();};
    void vec_to_theta(const Eigen::VectorXd&);
    double f(const Eigen::VectorXd& );
    Eigen::VectorXd df();
    Eigen::MatrixXd calcH();
    Eigen::VectorXd exp_param(); 
    /*
    TODO::
    */
    virtual void set_solver(solver *) {};
    virtual void init_chol(std::string) {};
    virtual void init_iter(std::string,int,int,double) {};
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    virtual Eigen::VectorXd direction() {return *X;};
    virtual void show_results();

  
};
class genericKGH :  public virtual latent {
  private:
          
      Qmatrix* Kobj;
  public:
    void set_fixed(const Eigen::VectorXd& );
     void set_K(Qmatrix*);
    genericKGH() {npars = 0;};
    void init(Eigen::SparseMatrix<double,0,int>* , Eigen::SparseMatrix<double,0,int>* , GHnoise*);
    void setX(Eigen::VectorXd *Xin){ X = Xin;};
    //void setK(Eigen::SparseMatrix<double,0,int> *Kin){ K = *Kin;};
    GHnoise *noiseClass;
    void setNoise(GHnoise *);
    void updateNoise();
    void update();
    void update_param();
    void initFromList(Rcpp::List const &);
    void simulateNoise() {noiseClass->simulateV();};
    void vec_to_theta(const Eigen::VectorXd&);
    double f(const Eigen::VectorXd& );
    Eigen::VectorXd df();
    Eigen::VectorXd exp_param(); 
    /*
    TODO::
    */
    virtual void set_solver(solver *) {};
    virtual void init_chol(std::string) {};
    virtual void init_iter(std::string,int,int,double) {};
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    virtual Eigen::VectorXd direction() {return *X;};
    virtual void show_results();

  
};
#endif