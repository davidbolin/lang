//
//  optim.h
//  mixture_library
//
//  Created by Jonas Wallin on 30/05/14.
//  Copyright (c) 2014 Jonas Wallin. All rights reserved.
//

#ifndef __mixture_library__optim__
#define __mixture_library__optim__

#include <iostream>
#include <stdio.h>      /* printf */
#include <Eigen/Dense>
#include <math.h>

class optim_func{
public:
	Eigen::VectorXd theta; // current parameter
	virtual double f(Eigen::VectorXd& theta)=0; //log-likelihood
	virtual Eigen::VectorXd df(Eigen::VectorXd& theta)=0; //gradient
	virtual Eigen::VectorXd direction(Eigen::VectorXd& theta)=0; //direction
	virtual Eigen::VectorXd valid_step(const Eigen::VectorXd& theta, const Eigen::VectorXd& dtheta)
	{
		return dtheta/2.;
	}
};



class optim {
	/*
	 optimsing class using either linesearch + method
	 or in case no function evalution is avialble gradient descent.

	 note however that optim_func f must allways have f,
	 in the case on does not have the actual function just check if coniditons are statisifed
	 if no condiiton always return 0. If condition viloted return inf or nan

	*/
private:

	// computes the descent direction at theta
	void gradient_descent(Eigen::VectorXd& theta);
	void precond_gradient_descent(Eigen::VectorXd& theta);

	//Broyden Quasi Newton
	void QuasiNewton(Eigen::VectorXd& theta);
	// computes descent direction for CG;
	void CG(Eigen::VectorXd& theta);
	// computes the direction for CG (internal)
	void CG_beta();
	// linesearch method or steplength
	void  linesearch();
	// method satistifying Armijo condition
	void  linesearch_Armijo();
	// no linesearch just step by alpha
	void linesearch_no();
	// method for interpolating alpha
	void  linesearch_interpolation(double fs);
	// selecting the inital value for steplength
	void select_alpha();
	void Poly_interpolation(double);
	double linesearch_i; //iteration number
	double gtd;
	Eigen::VectorXd alphas;
	Eigen::VectorXd fs;
	Eigen::VectorXd  theta_old;


public:
	optim_func *func;
	optim(optim_func *f, int type_ = 2); // init

	int type; // which optimiser to use:
				// 0 - steepest descent
				// 1 - Precond steepest descent
				// 2 - Conjugate gradient
				// 3- Broyden Quasi Newton
	int CG_type; // 0 - FR

	// linesearch value
	double alpha;
	double alpha0;
	int alpha_type;  // -1 - do nothing
						// 1 - use alpha=1 (unless iter = 1
					    // 2-  use alpha= alpha0
						// 3- use Brazilai Ben
	int stochastic_alpha; // decrese with
  // with stochastic alpa we decrease alpha using:
  // alpha=alpha/(stochastic_c0 + stochastic_c1 * n)^stochastic_c2
  double stochastic_c0;
  double stochastic_c1;
  double stochastic_c2;
	// line search parameters
	double c1;
	double c2;
	int line_search_iter;
	int line_search_condition; // 0 - Armijo
	int line_search_interp; // 0 - half step size, 1 - cubic interpolation (function vals only)
	double tau_alpha; // how fast to decrease alpha
	Eigen::VectorXd& step(); // takes a step in the descent direction
	void descent_direction(Eigen::VectorXd& theta); //computes the descent direction
													// at theta

	double           f; // value of f(theta), if using non evalution just check if in valid parameter space
	Eigen::VectorXd df; // gradient
	Eigen::VectorXd direction; // descent direction
	Eigen::VectorXd theta_current; // the current value of the parameters


	Eigen::VectorXd df_old;
	Eigen::VectorXd direction_old;
	Eigen::VectorXd direction0;
	Eigen::VectorXd direction0_old;
	Eigen::MatrixXd QN_matrix;



	int iter_count;
	};

#endif /* defined(__mixture_library__optim__) */
