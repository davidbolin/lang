#ifndef __SPATIAL__LATENT__
#define __SPATIAL__LATENT__
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "localprint.h"
#include "noiseGH.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>
#include <Eigen/SparseCore>
#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#include "Qmatrix.h"
#ifdef TIME_IT
	#include "TimeIt.h"
#endif

#ifdef _OPENMP
	#include<omp.h>
#endif
//#include "optim.h"
#include "MatrixAlgebra.h"
#include "solver.h"
#include <Rcpp.h>

class latent {
  private:

  public:
    latent(){npars=0;};
    virtual ~latent(){};
    int npars; //<  number of parameter for the latent component
    int n; //size of field
    int estimateOperator;
    Eigen::VectorXd * X;
    double logdetQ;
    Eigen::SparseMatrix<double,0,int> Q_post;
    Eigen::VectorXd                   mu_post;
    Eigen::VectorXd                   mu_prior;
    virtual void initFromList(Rcpp::List const &)=0;
    virtual void set_solver(solver *) = 0;
    virtual double f(const Eigen::VectorXd&) = 0;//{return(0);};
    virtual Eigen::VectorXd df()=0;
    virtual Eigen::VectorXd direction()=0;
    virtual Eigen::VectorXd get_theta()=0;
    virtual void vec_to_theta(const Eigen::VectorXd&)=0;
    virtual Rcpp::List output_list() =0;
    virtual void set_matrices(Eigen::VectorXd *)  {std::cout << "in show_results latent base class\n";};
    virtual void show_results()  {std::cout << "in show_results latent base class\n";};
    virtual void update_param()  {std::cout << "in update_param latent base class\n";};
    virtual void update()        {std::cout << "in update latent base class\n";};
    virtual void updateNoise()   {std::cout << "in upadeNoise latent base class\n";};
    virtual void simulateNoise() {std::cout << "in simulateNoise latent base class\n";};
    virtual Eigen::MatrixXd calcH() {Eigen::MatrixXd H; -H.setIdentity(npars,npars); return(H);};    
    virtual Eigen::VectorXd  get_fixed()        { return(fixed);};
    virtual void set_fixed(const Eigen::VectorXd& fixed_in)   {fixed = fixed_in;};
     
    Eigen::VectorXd fixed;
    virtual Eigen::VectorXd exp_param() { std::cout << "not implimented exp_param\n";return(fixed);};; // expontial paramatrization
    virtual void initSolver(Rcpp::List const &) {std::cout << "not implimented initsolver\n";};
    virtual void setNoise(GHnoise *noise) {};
    virtual void set_K(Qmatrix *) {};
};

#endif