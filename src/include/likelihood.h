#ifndef __SPATIAL__LIKELIHOOD__
#define __SPATIAL__LIKELIHOOD__
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "localprint.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>
#include <Eigen/SparseCore>
#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#include <Rcpp.h>
#ifdef TIME_IT
	#include "TimeIt.h"
#endif

#ifdef _OPENMP
	#include<omp.h>
#endif
#include "optim.h"
#include "MatrixAlgebra.h"
#include "solver.h"
#include "Qmatrix.h"
#include <random>
#include <chrono>
using namespace Eigen;
class likelihood {
  protected:
    solver * Qsolver;
    Qmatrix * Kobj;
    int estimateOperator;
    
  public:
  likelihood()  {Kobj = NULL; estimateOperator = 0; npars = 0; Qsolver=NULL;};
  virtual ~likelihood() {delete Qsolver;};
    Eigen::VectorXd * X;
    int nd; //number of datapoints
    int d; //dimension of data
    int npars; //number of parameters
    Eigen::VectorXd mu_post;
    Eigen::SparseMatrix<double,0,int> Q_post;
    double resid; //(Y-B*b)'*Q_e*(Y-B*b)
    double logdetQ; //log|Q_e|
    Eigen::VectorXd fixed;
    virtual void set_estimateOperator(int estOin){ npars -= estimateOperator - estOin; estimateOperator=estOin;};
    int get_estimateOperator(){return(estimateOperator);};
    void set_matrices(Eigen::VectorXd *Xin){  X = Xin;};
    void set_solver(solver *Qin){ Qsolver = Qin;};
    Qmatrix* get_Kobj(){return(Kobj);};
    virtual void initFromList(Rcpp::List const &)=0;
    virtual Eigen::VectorXd df()=0;
    virtual Eigen::VectorXd dfgX() {Eigen::VectorXd temp; return(temp);}; // direction with X known
    virtual Eigen::VectorXd direction()=0;
    virtual void vec_to_theta(const Eigen::VectorXd&)=0;
    virtual void show_results() = 0;
    virtual void update() = 0;
    virtual void update_param() = 0;
    virtual void add_mean( Eigen::VectorXd& X) {};
    virtual Eigen::VectorXd get_theta() = 0;
    virtual Rcpp::List output_list() = 0;
    virtual double f(const Eigen::VectorXd& theta) {return 0;};
    virtual Eigen::MatrixXd calcH() {Eigen::MatrixXd H; -H.setIdentity(npars,npars); return(H);};
    virtual Eigen::MatrixXd calcHgX() {Eigen::MatrixXd H; -H.setIdentity(npars,npars); return(H);};
    virtual Eigen::VectorXd exp_param() {std::cout << "likelihood:exp_param() not implimented\n";Eigen::VectorXd temp; return(temp);}; // expontial paramatrization
    virtual void set_Kmatrix(Qmatrix &K_in) {Kobj = &K_in;};
};

class likelihood_optim_function : public optim_func {
  
  private:
  
    likelihood * lik;
    solver     * Qsolver;
    Eigen::VectorXd                   theta_vec;
    Eigen::VectorXd                   alpha_vec; // remove temporary!!!!
    std::mt19937 random_engine;
    std::normal_distribution<double> normal;
    int n,df_updated;
    Eigen::VectorXd df_vec;

    int estimateH; // if 0 doesn't estimate,
                    // if 1 estimating it through the covariance of df, Stochastic Approximation
    double c0_H,c1_H,c2_H; // constant used in estimation of H
    
  public:
    Eigen::VectorXd X;
    Eigen::VectorXd theta; // current parameter
    double f(Eigen::VectorXd& ); //log-likelihood
    Eigen::VectorXd df(Eigen::VectorXd& ); //gradient
	  Eigen::VectorXd direction(Eigen::VectorXd& ); //direction
    void set_likelihood( likelihood &lik_in);
    Eigen::VectorXd get_theta();
    void vec_to_theta(Eigen::VectorXd& );
    void show_results();
};
#endif