#ifndef __SPAITAL_LIKGAUSS__
#define __SPAITAL_LIKGAUSS__
#include "likelihood.h"
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <RcppEigen.h>
#include <Rcpp.h>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <unsupported/Eigen/KroneckerProduct>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#include "Qmatrix.h"
#include "transformationFunctions.h"
#include "error_check.h"

#ifdef _OPENMP
	#include<omp.h>
#endif

#include "MatrixAlgebra.h"
#include "solver.h"



/*
	The likelihood for the standard measurement equation
		Y = B*beta + A*X + e
*/
class standard1dim : public virtual likelihood{
  protected:
		Eigen::MatrixXd B,AtB;
		Eigen::VectorXd beta,Y,AtY;
		double sigma2,dsigma,d2sigma;
		Eigen::MatrixXd H;
		Eigen::SparseMatrix<double,0,int> A,AtA;
		Eigen::VectorXd  dsigmadbeta,g,p;
		Eigen::MatrixXd d2beta;
		int n_beta;
    double d2dt;
    Eigen::VectorXd diffk;

	public:
	  standard1dim(){};
	  ~standard1dim(){};
	 	void init(std::string);
    void initFromList(Rcpp::List const &);
		double f(Eigen::VectorXd&);
		Eigen::VectorXd df();
    Eigen::VectorXd dfgX();
		Eigen::VectorXd direction();
		Eigen::VectorXd get_theta();
    Eigen::VectorXd dbeta;
		void vec_to_theta(const Eigen::VectorXd&);
		void show_results();
    void update();
    void update_param();
    void add_mean( Eigen::VectorXd& X);
    Eigen::MatrixXd calcHgX();
    Rcpp::List output_list();

    Eigen::VectorXd exp_param();
};


/*


  Standard Gaussian measurement error.
  Y = B*beta + A*X + e
  e \sim N(0,\sigma^2I)
  where one can put covaraites on \sigma_v using that
  \sigma_v = exp(B_{\sigma}\sigma)
  Futher it is possible to estimate the parameter of the operator (K) in the model:
  KX = E,
   Y = B*beta + A*X + e
   
   by reformulating as:  
   X = E,
   Y = B*beta + A*K^{-1}X + e.
   covariates on the mean is through:
   B * beta
   covariates on the standrad deviation is through:
   Bsigma * sigma 
   For usage of df, or direction uses exp(Bsigma * sigma)
   
   
   if Bsigma is empty
   then sigma = exp( param)
   
   
*/
class std1DimGaussianWOperator : public virtual likelihood{
  protected:
  
  	Eigen::MatrixXd  B, Bsigma;
    Eigen::VectorXd Y;
		Eigen::VectorXd beta;
    Eigen::VectorXd det_;
    Eigen::VectorXd sigma2_vec;
		Eigen::VectorXd sigma ,dsigma;
    Eigen::MatrixXd d2sigma;
		Eigen::MatrixXd H;
    Eigen::MatrixXd  dsigmadbeta;
		Eigen::SparseMatrix<double,0,int> A,AtA;
		Eigen::VectorXd  g,p;
		Eigen::MatrixXd d2beta;
    Eigen::MatrixXd df_Kvec_guess;
		int n_beta, n_sigma;
    double d2dt;
    Eigen::VectorXd diffk;    
    Eigen::VectorXd (std1DimGaussianWOperator::*sigma_vec) ();
    Eigen::VectorXd (std1DimGaussianWOperator::*dsigma_vec) (const Eigen::VectorXd&);
    Eigen::VectorXd sigma_exp_transform(){Eigen::VectorXd temp =  Bsigma*sigma; temp.array() = temp.array().exp(); return(temp);};
    Eigen::VectorXd dsigma_exp_transform(const Eigen::VectorXd  &);
    void update_Kparam();
    double trace_Qeps(int); // calcs E[A dQ_e Q_e A^T xx^T] under assumption Q_e = diag(e^{Bsigma})
    double trace_Qeps2(int, int); // calcs E[A dQ_e_i dQ_e_j Q_e A^T xx^T] under assumption Q_e = diag(e^{Bsigma})
    SparseMatrix<double,0,int> * dQ_e;
	public:
	  std1DimGaussianWOperator() : likelihood() {};
	  ~std1DimGaussianWOperator(){};
	 	void init(std::string);
    void initFromList(Rcpp::List const &);
		double f(Eigen::VectorXd&);
    void set_estimateOperator(int );
		Eigen::VectorXd df();
    Eigen::VectorXd dfgX();
		Eigen::VectorXd direction();
		Eigen::VectorXd get_theta();
    Eigen::VectorXd dbeta;
		void vec_to_theta(const Eigen::VectorXd&);
		void show_results();
    void update();
    void update_param();
    Eigen::MatrixXd calcHgX();
    Rcpp::List output_list();
    Eigen::VectorXd exp_param();
    void set_Kmatrix(Qmatrix &);
};


/*
  The likelihood for the nested SPDE model
    Y = B*beta + A*X + e
    X = (1 + B'*nabla)*Z
*/
class nestedspde : public standard1dim{
  private:
    VectorXd thetavec, dthetavec;
    MatrixXd d2thetavec, dbetadthetavec, dsigmadthetavec;
    SparseMatrix<double,0,int> * Bvec;
    SparseMatrix<double,0,int> I,Hvec;
    int n,n_vec;
  public:
    void init(std::string);
    void initFromList(Rcpp::List const &);
    void update_param();
    double f(VectorXd&);
    VectorXd df();
    VectorXd direction();
    VectorXd get_theta();
    void vec_to_theta(const VectorXd&);
    void show_results();
};

/*
	Likelihood for d-dimensional data with separate measurement noise variances
	for each dimension and constant mean for each dimension
*/
class simpleDdim : public virtual likelihood{
	private:
		MatrixXd * U;
		MatrixXd * QU;
		MatrixXd * Xk;
		MatrixXd Yk, B;
		VectorXd Y, beta_mu, sigma2;
		MatrixXd H;
		SparseMatrix<double,0,int> A,Qsig,AtA,I;
		VectorXd dmu, dsigma,d2mu, d2sigma,dsigmadmu,g,p;
		int N;
	public:
	 	void init(std::string);
		double f(VectorXd&);
		VectorXd df();
		VectorXd direction();
		VectorXd get_theta();
		void vec_to_theta(const VectorXd&);
		void set_matrices(MatrixXd *, MatrixXd *,MatrixXd *);
		void show_results();
};


#endif
