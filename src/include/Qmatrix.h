#ifndef __Q__MATRIX__
#define __Q__MATRIX__
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "localprint.h"
#include "transformationFunctions.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>
#include <Eigen/SparseCore>
#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#include <Rcpp.h>
#include <RcppEigen.h>
#include "MatrixAlgebra.h"
#ifdef TIME_IT
	#include "TimeIt.h"
#endif

#ifdef _OPENMP
	#include<omp.h>
#endif
__Q__MATRIX__
#include "solver.h"

class Qmatrix {
  protected:
    solver * Qsolver;
  public:
    Qmatrix() {Qsolver = NULL;};
    virtual ~Qmatrix(){delete Qsolver;};
    int d; //dimension
    int npars; //number of parameters
    Eigen::SparseMatrix<double,0,int> Q; // the generic matrix object
    Eigen::MatrixXd K;                   // the generic matrix object if Q is full!
    virtual void initFromList(Rcpp::List const &)=0;
    virtual void initFromList(Rcpp::List const &, Rcpp::List const &) {Rcpp::Rcout << "initFromList(list1,list2) not implimented in Qmatrix\n";};
    virtual Eigen::SparseMatrix<double,0,int> df(int)=0;
    virtual Eigen::SparseMatrix<double,0,int> d2f(int,int)=0;
    virtual double trace(int)=0;
    virtual double trace2(int,int)=0;

    virtual void vec_to_theta(const Eigen::VectorXd&)=0;
    virtual Eigen::VectorXd get_theta() = 0;
    virtual Rcpp::List output_list() = 0;
    virtual double f(const Eigen::VectorXd &)=0;
    virtual void show_results()=0;
    virtual double logdet()=0;
    // x  = A^-1b
    virtual Eigen::VectorXd solve(Eigen::VectorXd &b, Eigen::VectorXd &Guess) {return(Qsolver->solve(b, Guess));};
    virtual void update(){};
    virtual void update_param(){};
    virtual Eigen::VectorXd exp_param() { throw("Qmatrix::not implimented exp_param\n");}; // expontial paramatrization
    Eigen::VectorXd fixed;

};

class ARmatrix : public Qmatrix{
  protected:
    Eigen::SparseMatrix<double,0,int> I,I0,I11,I12;
    Eigen::VectorXd phiv;
    Eigen::MatrixXd Bphi;
    Eigen::DiagonalMatrix<double,Dynamic> Phi;
    SparseMatrix<double,0,int> * Phik;
    VectorXd phi,phi2;
  public:
    void initFromList(Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int){return 0.0;};
    double trace2(int,int){return 0.0;};
    double f(const Eigen::VectorXd&);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet(){return 1.0;};

};

class MaternMatrix : public Qmatrix{
  protected:
    double ldet;
    Eigen::VectorXd g,p;
    SparseMatrix<double,0,int> KCK;
    Eigen::SparseMatrix<double,0,int> G, C, Ci, K;
    Eigen::VectorXd kpv, phiv, dkappa, dphi,kappa,phi, theta;
    Eigen::MatrixXd d2kappa, d2phi, dkappadphi, Bkp, Bphi;
    int nkp, nphi;
    Eigen::DiagonalMatrix<double,Dynamic> Kappa, Phi;
    Eigen::MatrixXd H;
    bool use_chol;
    SparseMatrix<double,0,int> * Phik, * Mk;
    VectorXd phi2;
  public:
    void initFromList(Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int);
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet(){return ldet;};
};


class MultivariateMaternMatrix : public Qmatrix{
  protected:
    int dim, nop, n;
    Eigen::MatrixXd nop_ij;
    double ldet;
    Eigen::VectorXd g,p;
    SparseMatrix<double,0,int> *Klist, *Clist, *Glist, *Cilist;
    Eigen::SparseMatrix<double,0,int> G, C, Ci, K, Kphi;
    //Eigen::SparseMatrix<double,0,int> *Klist;
    Eigen::VectorXd *kpv, *phiv, *kappa, *phi;
    Eigen::VectorXd *dkappa, *dphi, theta;
    Eigen::MatrixXd *d2kappa, *d2phi, *dkappadphi, *Bkp, *Bphi, S;
    Eigen::VectorXi nkp, nphi;
    Eigen::DiagonalMatrix<double,Dynamic> *Kappa, *Phi;
    Eigen::MatrixXd H;
    bool use_chol;
    SparseMatrix<double,0,int> * Phik, * Mk;
    VectorXd phi2;
  public:
    void initFromList(Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int);
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet(){return ldet;};
};



class MaternMatrixOperator : public Qmatrix{
  protected:
    double ldet;
    Eigen::VectorXd g,p;
    Eigen::SparseMatrix<double,0,int> G, C;
    Eigen::VectorXd kpv, phiv, dkappa, kappa, theta;
    Eigen::MatrixXd d2kappa,  Bkp;
    int nkp, nphi;
    Eigen::DiagonalMatrix<double,Dynamic> Kappa;
    Eigen::MatrixXd H;
    bool use_chol;
    int calc_det;
    SparseMatrix<double,0,int> * Phik, * Mk;
    VectorXd phi2;

    Eigen::VectorXd (*kappa_vec) (Eigen::MatrixXd &, Eigen::VectorXd &);
    Eigen::MatrixXd (*dkappa_mat) (Eigen::MatrixXd &, Eigen::VectorXd &);


  public:
    int n;
    MaternMatrixOperator(){};
    void initFromList(Rcpp::List const &);
    void initFromList(Rcpp::List const &, Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int){ Rcpp::Rcout << "MaternMatrixOperator trace2 not defined\n";return(std::numeric_limits<double>::infinity());};
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet();
    Eigen::VectorXd exp_param();
};


class MultivariateMaternMatrixOperator : public Qmatrix{
   protected:
    int dim, nop, n;
    Eigen::MatrixXd nop_ij;
    double ldet;
    Eigen::VectorXd g,p;
    SparseMatrix<double,0,int> *Klist, *Clist, *Glist, *Cilist;
    Eigen::VectorXd (*kappa_vec) (Eigen::MatrixXd &, Eigen::VectorXd &);
    Eigen::MatrixXd (*dkappa_mat) (Eigen::MatrixXd &, Eigen::VectorXd &);
    Eigen::MatrixXd  dkappa, dphi;
    Eigen::VectorXd kappa;
    Eigen::SparseMatrix<double,0,int> G, C, Ci, Kphi;
    Eigen::VectorXd *kpv, *phiv;
    Eigen::VectorXd   theta, phi;
    Eigen::MatrixXd *d2kappa, *d2phi, *dkappadphi, *Bkp, *Bphi, S;
    Eigen::VectorXi nkp, nphi;
    Eigen::DiagonalMatrix<double,Dynamic> *Kappa, *Phi;
    Eigen::MatrixXd H;
    bool use_chol;
    SparseMatrix<double,0,int> * Phik, * Mk;
    VectorXd phi2;
  public:
    ~MultivariateMaternMatrixOperator();
    void initFromList(Rcpp::List const &);
    void initFromList(Rcpp::List const &, Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int);
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet(){return ldet;};
    Eigen::VectorXd exp_param();
};

class constMatrix : public Qmatrix{
  protected:
    double ldet;
    Eigen::VectorXd v;
    Eigen::SparseMatrix<double,0,int> m;
  public:
    void initFromList(Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int){return m;};
    Eigen::SparseMatrix<double,0,int> d2f(int,int){return m;};
    double trace(int){return 0.0;};
    double trace2(int,int){return 0.0;};
    double f(const Eigen::VectorXd & theta_in){return 0.0;};

    void vec_to_theta(const Eigen::VectorXd&){};
    Eigen::VectorXd get_theta(){return v;};
    Rcpp::List output_list();
    void show_results(){};
    double logdet(){return 0;};
};


class generic0Matrix : public Qmatrix{
  protected:
    Eigen::VectorXd tau;
    Eigen::SparseMatrix<double,0,int> M,O;
    int n_scale;
  public:
    void initFromList(Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int);
    double f(const Eigen::VectorXd & theta_in);
    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta(){return tau;};
    Rcpp::List output_list();
    void show_results(){Rcpp::Rcout << "tau = " << tau(0);};
    double logdet();
};


class nestedOperator : public Qmatrix{
  protected:
    Eigen::VectorXd theta;
    SparseMatrix<double,0,int> * Bvec;
    SparseMatrix<double,0,int> I;
  public:
    int n;
    nestedOperator(){};
    void initFromList(Rcpp::List const &);
    void initFromList(Rcpp::List const &, Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int){ Rcpp::Rcout << "nestedOperator trace not implemented\n";return(std::numeric_limits<double>::infinity());};
    double trace2(int,int){ Rcpp::Rcout << "nestedOperator trace2 not implemented\n";return(std::numeric_limits<double>::infinity());};
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet(){return 0.0;};
};

class GeneralMatrixOperator : public Qmatrix{
  protected:
    double ldet;
    Eigen::VectorXd g,p;
    Eigen::MatrixXd H;
    bool use_chol;
    int calc_det;


  public:
    int n;
    GeneralMatrixOperator(){};
    void initFromList(Rcpp::List const &);
    void initFromList(Rcpp::List const &, Rcpp::List const &);
    Eigen::SparseMatrix<double,0,int> df(int);
    Eigen::SparseMatrix<double,0,int> d2f(int,int);
    double trace(int);
    double trace2(int,int){ Rcpp::Rcout << "GeneralMatrixOperator trace2 not defined\n";return(std::numeric_limits<double>::infinity());};
    double f(const Eigen::VectorXd &);

    void vec_to_theta(const Eigen::VectorXd &);
    Eigen::VectorXd get_theta();
    Rcpp::List output_list();
    void show_results();
    double logdet();
    Eigen::VectorXd exp_param();
};


#endif