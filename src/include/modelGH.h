#ifndef __SPATIAL__MODELGH__
#define __SPATIAL__MODELGH__

#include <Rcpp.h>
#include <RcppEigen.h>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <random>
#include <chrono>
#include "optim.h"
#include "likelihood.h"
#include "latent.h"
#include "solver.h"
#include "optim.h"


typedef std::shared_ptr<likelihood> likelihood_ptr;
typedef std::shared_ptr<latent> latent_ptr;

class modelGH : public optim_func {
  private:
    solver     * Qsolver;
    Eigen::VectorXd                   theta_vec;
    Eigen::VectorXd                   alpha_vec; // remove temporary!!!!
    std::mt19937 random_engine;
    std::normal_distribution<double> normal;
    int n,df_updated;
    Eigen::VectorXd df_vec;

    int estimateH; // if 0 doesn't estimate,
                    // if 1 estimating it through the covariance of df, Stochastic Approximation
    double c0_H,c1_H,c2_H; // constant used in estimation of H
    

  public:


    likelihood * like;
    latent     * lat;
    Eigen::SparseMatrix<double,0,int>  Q;
    Eigen::VectorXd                   mu;
    Eigen::VectorXd                   X;
    Eigen::MatrixXd   Xs;
    Eigen::VectorXd df_var;
    Eigen::MatrixXd  dfs;

   modelGH();
   ~modelGH();
   void init_Hestimation(Rcpp::List const &init_list);
   void set_likelihood( likelihood &lik_in);
   void set_latent( latent &);
   void initSolver(Rcpp::List const &init_list);
   void set_solver( solver &solver_in){delete Qsolver; Qsolver = &solver_in; };
   void update_param();
   void update0();
   void simulateX();
   void simulateNoise();
   void simulate();
   void show_results();
   Eigen::VectorXd return_mu(){return((*Qsolver).solve(mu,mu));};
   void set_fixed(const Eigen::VectorXd& );
   Eigen::VectorXd get_fixed();
   Eigen::VectorXd kriging(Eigen::SparseMatrix<double,0,int> &,const int ,const int);
   int nGibbsIter;
   int prenGibbsIter;

  double f(Eigen::VectorXd& ); //log-likelihood
	Eigen::VectorXd df(Eigen::VectorXd& ); //gradient
	Eigen::VectorXd direction(Eigen::VectorXd& ); //direction
  Eigen::MatrixXd H;

  Eigen::MatrixXd H0; // H0 used in estimation of H
  int iter_H; // iteration for estimation H
  void set_H0(); // initalising H0

  void estimate_H();
  VectorXd get_theta();
  Rcpp::List output_list();
   int sim;
   int thin;

   void vec_to_theta(const Eigen::VectorXd&);
};


class modelGH_mult {
  public:
    std::vector<likelihood_ptr> lik_vec;
    std::vector<latent_ptr> lat_vec;
    
    std::vector<Eigen::VectorXd> X;
    std::vector<Eigen::SparseMatrix<double,0,int>>  Q;
    std::vector<Eigen::VectorXd>                   mu;
    
    void set_nreplicat(int );
    void set_latent(int    , latent_ptr );
    void set_likelihood(int, likelihood_ptr  );
   
};

#endif