#ifndef __SPATIAL__LATENTGAUSS__
#define __SPATIAL__LATENTGAUSS__
#include "latent.h"
#include "solver.h"
#include "MatrixAlgebra.h"
#include "Qmatrix.h"

class latentGaussian : public virtual latent {
  private:
    Qmatrix * Qs;
    solver * Qhsolver;
    Eigen::VectorXd g,p;
    Eigen::SparseMatrix<double,0,int> Mk;
    Eigen::MatrixXd H;
  public:
    void set_solver(solver *);
    double f(const Eigen::VectorXd&);
    VectorXd df();
    VectorXd direction();
    VectorXd get_theta();
    void vec_to_theta(const Eigen::VectorXd&);
    void set_matrices(Eigen::VectorXd *);
    void show_results();
    void initFromList(Rcpp::List const &);
    Rcpp::List output_list();
};

class proportionalGaussian : public virtual latent {
  private:
    Qmatrix * Qs, * Qd;
    solver * Qhsolver;
    Eigen::VectorXd dpsi, dtheta;
    Eigen::MatrixXd d2psi, d2theta, dpsidtheta;
    Eigen::VectorXd g,p;
    Eigen::SparseMatrix<double,0,int> Mk;
    Eigen::MatrixXd H;
  public:
    void set_solver(solver *);
    double f(const Eigen::VectorXd&);
    VectorXd df();
    VectorXd direction();
    VectorXd get_theta();
    void vec_to_theta(const Eigen::VectorXd&);
    void set_matrices(Eigen::VectorXd *);
    void show_results();
    void initFromList(Rcpp::List const &);
    Rcpp::List output_list();
};

#endif