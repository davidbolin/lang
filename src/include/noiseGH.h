#ifndef __SPATIAL__NOISEGH__
#define __SPATIAL__NOISEGH__
#include <Rcpp.h>
#include <RcppEigen.h>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include "rgig.h"
#include <Rmath.h>
#include <chrono>
/*

  The class is thought of for linear covariates for all parameter,
  to generalise more one should set:
  setpGIG, setaGIG, setbGIG
  which is the only placed where for simulation of the posterior the object is used
  grad object is an other thing for later
*/
class GHnoise{
  protected:
  gig rgig; // < generater of genralised inverse Gaussian random number

  
  public:  
    GHnoise();
    double V_adj;
    double b_adj;
    
    Eigen::VectorXd      resid;
   Eigen::VectorXd  pGIG; //< coeffients vectors for simulating V
  Eigen::VectorXd  aGIG; //< coeffients vectors for simulating V
  Eigen::VectorXd  bGIG; //< coeffients vectors for simulating V     
    Eigen::VectorXd fixed; // which parameters should be estimated 
    int n;               // number of noise components
    int npars;
    Eigen::VectorXd  E; //< the  noise
    Eigen::VectorXd  V;  //< the noise Variance
    Eigen::VectorXd  iVsigma;  // the noise precision used by latent
    Eigen::VectorXd  muE;      // the mean parameter used by latent
    void setE(Eigen::VectorXd *Ein){ E.resize((*Ein).size()) ;E = (*Ein);};
    void setV(Eigen::VectorXd *Vin){ V.resize((*Vin).size()) ; V = (*Vin); };
    void simulateV();
    virtual void vec_to_theta(const double*) = 0;
    virtual void vec_to_theta(const Eigen::VectorXd &) = 0;
    virtual void update()           = 0;
    virtual void updateV()          = 0; // bad name need, updates parameters needed by latent 
    virtual void update_param()     = 0;
    virtual void show_results()     = 0;
    virtual Rcpp::List toRcppList() = 0;
    virtual Eigen::VectorXd df()    = 0;
    virtual double f(const Eigen::VectorXd&)    = 0;
    virtual Eigen::VectorXd get_theta() = 0;
    virtual Eigen::MatrixXd calcH()   { return(Eigen::MatrixXd::Identity(npars, npars));};
    virtual Eigen::VectorXd estimate_exp() = 0;
};


class GALnoise: public GHnoise{
  private:
    Eigen::VectorXd      sigma;     // < the variance parameter of the latent noise  
                                    // (not equal to variancce of the latent noise)
    Eigen::VectorXd      delta;     // < the location parameter of the latent noise 
    Eigen::VectorXd      mu;        // < the asymmetric parameter of the latent noise
    Eigen::VectorXd      lambda;    // < the shape parameter of the latent noise
    Eigen::VectorXd     resid_square;
    void dmu(Eigen::VectorXd&);
    void ddelta(Eigen::VectorXd&);
    void dsigma(Eigen::VectorXd& );
    void dgamma(Eigen::VectorXd& );
    void show_results();
    int  count_df; //internal variable used in setting the gradient!
    int sigma_transformed; // 0 - no transform, 1- expontial form
    int lambda_transformed; // 0 - no transform, 1- expontial form
    
    //************************************************************************************************************************
    //************************************************************************************************************************
    // lambda transformation
    void initLambda();
    Eigen::VectorXd (GALnoise::*lambda_vec) ();
    Eigen::VectorXd (GALnoise::*dlambda_vec) (const Eigen::VectorXd&);

    Eigen::VectorXd lambda_no_transform(){return(Blambda*lambda);};
    Eigen::VectorXd dlambda_no_transform(const Eigen::VectorXd &df_pointwise){return(Blambda.transpose() * df_pointwise);};

    Eigen::VectorXd lambda_exp_transform(){Eigen::VectorXd temp =  Blambda*lambda; temp.array() = temp.array().exp(); return(temp);};
    Eigen::VectorXd dlambda_exp_transform(const Eigen::VectorXd  &);
  
    Eigen::VectorXd lambda_expi_transform(){ Eigen::VectorXd lambda_exp = lambda; lambda_exp.array() = lambda_exp.array().exp(); return(Blambda*lambda_exp);};
    Eigen::VectorXd dlambda_expi_transform(const Eigen::VectorXd  &);
    //************************************************************************************************************************
    //************************************************************************************************************************
    // Sigma transformation
    void initSigma();
    Eigen::VectorXd (GALnoise::*sigma_vec) ();
    Eigen::VectorXd (GALnoise::*dsigma_vec) (const Eigen::VectorXd&);
    
    Eigen::VectorXd sigma_no_transform(){return(Bsigma*sigma);};
    Eigen::VectorXd dsigma_no_transform(const Eigen::VectorXd &df_pointwise){return(Bsigma.transpose() * df_pointwise);};
     
    Eigen::VectorXd sigma_exp_transform(){Eigen::VectorXd temp =  Bsigma*sigma; temp.array() = temp.array().exp(); return(temp);};
    Eigen::VectorXd dsigma_exp_transform(const Eigen::VectorXd  &);
    //************************************************************************************************************************
    //************************************************************************************************************************
    //************************************************************************************************************************
  public:    
  // setting covariates vectors
    Eigen::MatrixXd Bmu;
    Eigen::MatrixXd Bdelta;
    Eigen::MatrixXd Bsigma;
    Eigen::MatrixXd Blambda;
    
    int GAL; //if 1 GAL else NIG
    
    void update(); // updates but assumes parameters fixed
    void update_param(); // updates the parameteres
    /*
      for vec_to_theta the following order is used:
      delta, sigma, mu, lambda
    */
    void vec_to_theta(const double* );
    void vec_to_theta(const Eigen::VectorXd &);
    void setB(const Eigen::MatrixXd*, const Eigen::MatrixXd*, const Eigen::MatrixXd*,const Eigen::MatrixXd*);
    
    /*
      transforming the GALnoise object to an R list
    
    */
    Rcpp::List toRcppList();
    /*
      initialising the object from an list
    */    
    void  initFromList(Rcpp::List const &);
    
    void updateV();
    Eigen::VectorXd df();
    Eigen::VectorXd estimate_exp();
    Eigen::MatrixXd calcH();
    double f(const Eigen::VectorXd&);
    Eigen::VectorXd get_theta();
    
};
#endif