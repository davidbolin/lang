#ifndef __TRANSFORM__
#define __TRANSFORM__

#include <Eigen/Dense>


Eigen::VectorXd g_exp_transform(Eigen::MatrixXd  &  , Eigen::VectorXd & );
Eigen::MatrixXd dg_exp_transform(Eigen::MatrixXd &   , Eigen::VectorXd & ); 
Eigen::VectorXd g_no_transform(Eigen::MatrixXd   &   , Eigen::VectorXd & );
Eigen::MatrixXd dg_no_transform(Eigen::MatrixXd  &   , Eigen::VectorXd & );






#endif