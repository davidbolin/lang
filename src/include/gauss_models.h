#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "localprint.h"
#include <Eigen/Dense>
#include <Eigen/LU>
#include <unsupported/Eigen/KroneckerProduct>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/OrderingMethods>
#ifdef TIME_IT
  #include "TimeIt.h"
#endif

#ifdef _OPENMP
  #include<omp.h>
#endif
#include "optim.h"
#include "MatrixAlgebra.h"
#include "likelihoodGaussian.h"
#include "latent.h"
#include "latentGaussian.h"
#include "solver.h"
using namespace Eigen;


class gaussianmodel : public optim_func {
  private:
    likelihood * like;
    latent * late;
    solver * Qsolver;

    VectorXd X, theta_vec;
    SparseMatrix<double,0,int> Qh_p;

    int N; //iterations in trace estimators.
    int d; //dimension of data
    int n; //size of field
    double f_val; // the log likelihood
    bool df_called, f_called;
    bool use_f, use_chol;

		VectorXd g,p;
		double tol;
	public:
		void init(std::string, likelihood*,latent*,bool);
		void initfromlist(likelihood*, latent*, Rcpp::List const &);
		double f(VectorXd&);
		VectorXd df(VectorXd&);
		VectorXd direction(VectorXd&);
		VectorXd get_theta();
		VectorXd get_X();
		VectorXd get_vars();
		void vec_to_theta(const VectorXd&);
		void print_results(std::string);
		void show_results();
		Rcpp::List output_list();
};
