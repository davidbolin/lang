#include "MatrixAlgebra.h"
#include "gauss_models.h"
#include "solver.h"
#include "TimeIt.h"
#include <ctime>

using namespace std;

int main (int argc, char * const argv[])
{
  string path;
  cout << "start test" << endl;
  if (argc > 1) {
    path = argv[1];
  } else {
    path =  "/tmp/";
  }
  SparseMatrix<double,0,int> M1, M2;
  MatrixXd v;
  VectorXd X1, X2;

  cout << "read data" << endl;
  read_SparseMatrix(M1,path,"M1");
  read_SparseMatrix(M2,path,"M2");
  read_MatrixXd(v,path,"v");

  timer_start(0);
  SimplicialLLT<SparseMatrix<double,0,int> > R;
  R.compute(M1);
  timer_end(0,"chol");
  SparseMatrix<double,0,int> Q1, Q2;
  SparseMatrix<double,0,int> Rq = R.matrixL();

  timer_start(1);
  Q1 = Qinv(Rq);
  timer_end(1,"Qinv");
  //cout << "Q1:\n " << Q1 << endl;

  //timer_start(2);
  //Q2 = Qinv2(Rq);
  //timer_end(2,"Qinv2");
  //cout << "Q2:\n " << Q2 << endl;

  //SparseMatrix<double,0,int> diff = Q1 - Q2;
  //cout <<  "Difference= " << diff.norm();
}