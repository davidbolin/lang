#include <Rcpp.h>
#include <RcppEigen.h>
#include "noiseGH.h"
#include "latentGH.h"
#include "modelGH.h"
#include "likelihoodGaussian.h"
#include "optim.h"
#include <cmath>
#include <Eigen/Dense>
#include "eigen_add_on.h"

/*
  Function for estimating 
  
  
*/  
// [[Rcpp::export]]
Rcpp::List likelihood_est(Rcpp::List spdeObj_in){
  
  Rcpp::List spdeObj      = clone(spdeObj_in);
  
  int verbose = Rcpp::as<int>(spdeObj["verbose"]);
  
  Rcpp::List latent_list  = spdeObj["latent"]; 
  Rcpp::List likelihood_list  = spdeObj["likelihood"]; 
  Rcpp::List optim_list   = spdeObj["optim"]; 
  Rcpp::List solver_list   = spdeObj["solver"];
  likelihood_optim_function model;
  
  if(latent_list.containsElementNamed("X") == 1){
    Eigen::VectorXd Xtemp = Rcpp::as<Eigen::VectorXd>(latent_list["X"]);
    model.X.resize(Xtemp.rows(), Xtemp.cols());
    model.X = Xtemp;
  }
  else
  {
    Rcpp::Rcout << "likelihood_est::must supply X in latent_list\n";
    throw;
  }
  Eigen::VectorXd E; 
  if(latent_list.containsElementNamed("E") == 1){
    E = Rcpp::as<Eigen::VectorXd>(latent_list["E"]);
  }
  else
  {
    Rcpp::Rcout << "likelihood_est::must supply E in latent_list\n";
    throw;
  }
  likelihood* likGaussian;

  
  
  Qmatrix* Kobj;   
  if(Rcpp::as<int>(latent_list["type"]) == 4){
    Kobj     = new MultivariateMaternMatrixOperator;
  }else{
    Kobj     = new MaternMatrixOperator;
  }
  Kobj->initFromList(latent_list, spdeObj["solver"]);
  
  likGaussian = new std1DimGaussianWOperator;
  likGaussian->initFromList(spdeObj["likelihood"]);
  
  
  likGaussian->set_Kmatrix(*Kobj);
  model.set_likelihood(*likGaussian);
  likGaussian->set_estimateOperator(1);
  
  
  
  int max_iter  = Rcpp::as<int>(optim_list["max.iter"]);
  int optimtype = Rcpp::as<int>(optim_list["type"]); 
  optim Optim(&model, optimtype);

  Optim.stochastic_alpha      = Rcpp::as<int>(optim_list["stochastic.alpha"]);
  Optim.alpha_type            = Rcpp::as<int>(optim_list["alpha.type"]);
  Optim.line_search_condition = Rcpp::as<int>(optim_list["line.search.condition"]);
  Optim.alpha0                = Rcpp::as<double>(optim_list["alpha0"]);

  double tol= Rcpp::as<double>(optim_list["tol"]);
  int use_chol = Rcpp::as<int>(optim_list["use.chol"]);
  int iterations = 0;
  
  
  
  Eigen::VectorXd theta = model.get_theta();
  
  if(spdeObj.containsElementNamed("theta")){
    
    Eigen::VectorXd theta_in = Rcpp::as<Eigen::VectorXd>(spdeObj["theta"]);
    if(theta.size() != theta_in.size())
    {
      Rcpp::Rcout << " length of theta should be :" << theta.size()<< "\n";
      throw("theta is wrong length\n");
    }
    theta = theta_in;
  }
  model.vec_to_theta(theta);
  Eigen::VectorXd nothing;
  model.X = Kobj->solve(E,nothing);
  Rcpp::List result; 
  if(Rcpp::as<int>(spdeObj["calcdf"]))
  {
    result["df"] = model.df(theta);
    result["direction"] = model.direction(theta); 
    return(result);
  }
  
  MatrixXd Thetas;
  Thetas.setZero(max_iter, theta.size());
  
  int iter_count = 0;
   for(int i = 0; i < max_iter; i++){
     
    Rcpp::Rcout << "df    =" << Optim.func->df(theta).transpose() << " \n";
    Rcpp::Rcout << "theta =" << theta.transpose() << " \n";
    if(((theta - theta).array() != (theta - theta).array()).all())
      break;
    model.X = Kobj->solve(E,nothing);    
    Thetas.row(iter_count) = theta;
    Optim.descent_direction(theta);
    
    Optim.step();
    theta = Optim.theta_current;
    Eigen::VectorXd df = Optim.func->df(theta);
    
    iter_count++;
    if(verbose){
      Rcpp::Rcout << "max iter: " << i << "/"<< max_iter << "\n";
      model.show_results();
    }
   }
  
  
  //result['theta'] = lik->get_theta();
  return(result);
}